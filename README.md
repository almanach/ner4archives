# NER4Archives

# :warning: This repository is now only maintained on GitHub, here: https://github.com/NER4Archives-project (new realeases, updated documentation etc.)

## :mag_right: Overview / Présentation : 

- :gb: Developments  made in collaboration with the French National Archives on named entity recognition methods for XML-EAD finding aids.

- :fr: Développements réalisés en commun avec les Archives Nationales de France sur les méthodes de reconnaissance d'entités nommées dans les instruments de recherche XML-EAD.

## For project participants / Pour les participants du projet 

<img src="./Documentation/mybox_inria.png">

* [INRIA-MyBox (*Guidelines*, gestion des documents, rapports, présentations etc.) [permission requise]](https://mybox.inria.fr/library/cca1a417-7e6a-4f0d-ba1f-17715a5eda5a/NER4Archives/)


<!--
## :question: Welcome to this project, start by the wiki [here](https://gitlab.inria.fr/almanach/ner4archives/-/wikis/home)
-->

## :e-mail: :question: Contact & Maintainers :

[Laurent Romary](laurent.romary@inria.fr)

[Lucas Terriel](lucas.terriel@inria.fr)
