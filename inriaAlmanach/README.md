# Sources pour NER4Archives

## Structure 

- `Evaluation_annotation/` : evaluation de la campagne d'annotation NER4Archives
- `N4ACorpus/` : datasets du projet
- `N4A_NER/` : expérimentations en extraction et reconnaissance d'entités nommées (NER) sur le corpus NER4Archives

- `Inception-config/` : fichiers spécifiques l'utilisation de la plateforme INCEpTION

## Remarques

### ![jupyter](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/44px-Jupyter_logo.svg.png) Notes pour l'utilisation des notebooks Jupyter

Procédure pour lancer les notebooks dans les différents répertoires : 

- Pré-requis

    - [Anaconda](https://www.anaconda.com/products/individual)
    - Python >= 3.7

- Pour lancer le(s) notebook(s) 

    1. Créer un environnement conda depuis le fichier `environment.yml` disponible dans le répertoire 

        ```bash
        $ conda env create -f environment.yml
        ```

    2. Activer l'environnement (`$ conda env list` pour retrouver le nom de l'environnement)

        ```bash
        $ conda activate <name_env>
        ```

    3. Lancer jupyter notebook depuis le dossier

        ```bash
        $ (<name_env>) jupyter notebook 
        ```