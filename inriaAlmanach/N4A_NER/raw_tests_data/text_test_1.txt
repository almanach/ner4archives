Lyon Lettres de rémission pour Étienne Brocade, « homme de labeur », au bailliage de Berry, pour meurtre commis sur la personne d'Antoine Juillet 1501 Lyon Lettres de rémission pour Martin de Béthencourt, demeurant à Rebreuve-sur-Canche pour meurtre commis de complicité avec Jean de Boffles, bâtard, sur la personne de Jean Le Moicte que ledit de Boffles voulait déloger d'une maison qu'il occupait Juillet 1501 Rebreuve-sur-Canche, Pas-de-Calais, arrondissement de Saint-Pol-sur-Ternoise, canton d'Avesnes-le-Comte.

Lyon Lettres de rémission accordées à Pierre Courtillier, « homme de labeur », demeurant à Garchizy au pays de Nivernais, bailliage de Saint-Pierre-le-Moûtier, pour meurtre commis sur la personne de Guillaume Thibauldes, à la suite d'une querelle survenue à propos de réquisition militaire exigée par la ville de la Charité-sur-Loire Juillet 1501 Garchizy, Nièvre, arrondissement de Nevers, canton de Pougues La Charité-sur-Loire, Nièvre, arrondissement de Cosnes.

POULAIN, architecte, 8 - 18 août 1883.

POULAIN, 12 - 22 février 1897.

POULAINE, abbé, 19 janvier - 2 février 1907.

POUPELET, Mme Jeanne, 10 - 24 janvier 1929.

POUPIN, 20 juillet - 10 août 1893.

Estimation des ouvrages de charpenterie faits par Pierre Mourault pour Nicolas Rambouillet, en son hôtel formant plusieurs corps de logis, rue du Rempart, rue du Reposoir, rue Mail et rue du Fossé aujourd'hui, rue d'Aboukir 18 mars 1642.

Estimation des travaux de couverture tant ardoise que thuille neufve faits par François Michelarme pour Nicolas Rambouillet en plusieurs corps de logis rue du Rempart, rue du Reposoir et rue du Mail 14 mai 1642.

Vosges Rapport sur l'épuration d'un surveillant de la maison d'arrêt de Remiremont, 1946. Rapport de tournée sur l'institution publique d'éducation surveillée de Neufchâteau, 1947-1948. Rapport de tournée sur le Centre pénitentiaire de la Vierge à Epinal, 1948-1949. Rapport de tournée sur les maisons d'arrêt d'Epinal et Remiremont, 1949. Rapport de tournée sur la mission d'information générale sur la personnalité, l'action administrative et politique du préfet nommé provisoirement par le Comité de Libération, 1945. Rapport de tournée sur la préfecture et les sous-préfectures de Neufchâteau et Saint-Dié, 1949.

A Willaume de Paris Pétitions en faveur des intérêts français dans les mers de Chine et réclamation de pièces 1854-1855.

Dr Wismuller, de Nuremberg Correspondance relative à un nouveau système de fortifications de sa conception 1852-1854.

Procuration de Jean-Baptiste Jules Caizac, propriétaire, demeurant 63, rue du Bac, à Claudine Anna Manson, sa femme, sans profession 7 janvier 1856.

Dépôt judiciaire du testament olographe de Guillaume Galland, propriétaire, demeurant 13, rue des Batignolles 8 janvier 1856.

Arrondissement Saint-Quentin; canton Le Catelet « Plan Et Figure De Ce Qui Constitue Le Marché Et Dimage D'etricourt Et Des Environs » relevant pour un tiers de l'archevêque de Sens, comme abbé de Mont-Saint-Martin, pour un tiers du prieuré de Coincy, pour un tiers du seigneur de Joncourt Parcelles Moulins Fermes Notice 187.

Boulogne-sur-Mer (Pas-de-Calais) D'or à trois tourteaux de gueules, et en abyme un écusson du même chargé d'un cygne d'argent.

Ateliers A Couachi Arcachon Moteur O4s Dessin n° 1215 8 mai 1920 Dix coupes sur une feuille.

Plan de la proue d'un navire Elévation d'une sonde Plan du milieu du pont supérieur Dressés par Joseph CIOLINA Avril 1912 BALISES 1889, 1908 2 pièces.

planche 130 Portraits d'acteurs ou d'artistes : Capoul, Castelmary, Crosti sans date 9 photographies.

planche 131 Portraits des Coquelin, acteurs au Théâtre-Français sans date 7 photographies.

Document concernant Louis Herduin 1 photographie en uniforme.

Document concernant Gabriel Bardon, 91e RIT Livret militaire.

Contribution PI030 Presse Périodiques de la guerre 14-18.

28/09/06 Roumanie : Bucarest : Interview du Président, accordée à Tv5 monde et Rfi lors du XIème sommet des chefs d'État.

29/09/2006 Roumanie: Bucarest : Conférence de presse conjointe, à l'issue du Xieme Sommet des Chefs d'État et de gouvernement de la Francophonie. 29/09/06 Arménie : Erevan : Toast prononcé par le Président de la République, à l'occasion du dîner d'État offert en son honneur par le President de la République d'Arménie. 30/09/06 Arménie : Erevan : Allocution prononcée par le Président de la République française, à l'occasion de l'inauguration de la place de France.

Saint-Étienne-de-Cuines à Serrières-en-Chautagne.

Abbaye et col de Tamié à Yenne.

Haute-Savoie.

Pont de l'Abîme à Bernex.

Engel Lucien, né le 14 février 1903.

Engels Jacques Joseph, pseudonyme « Rétine », né le novembre 1924.

Monerie, Jean Auguste : vente par lui d’un terrain à Noisy-le-Grand, à Roger Léon Thomas, cuisinier, et Joséphine Jeanne Marie Leblay, son épouse (25 mars 1938). 

Ollier, Suzanne Hélène : vente à elle faite d’un terrain à Paray-Vieille-Poste, par Robert Othon Ladislas Hudry et Jeanne Suzanne Marcelle Pretet, son épouse (4 octobre 1941). 

Cliché n°379 torrent des Usses. Les Usses à Châtel.

Série de Meytet Meythet 1902.

Montereau-Fault-Yonne (suite) à Neufmoutiers-en-Brie Montereau-Fault-Yonne, Centre d'hémodialyse, 2009-2010 Nemours, Centre hospitalier, 2007-2009 Neufmoutiers-en-Brie, Centre médical et pédagogique pour adolescents, 2006-2008.

Noisy-le-Sec à Tremblay-en-France Noisy-le-Sec, Clinique Korian Roger Salengro, 2012 Saint-Denis, Clinique du Grand Stade, 2011-2013 Tremblay-en-France, Maison de santé médicale Joseph Sauvy, 2010-2012.

Déclaration de privilège de second ordre de Eugène Hippolyte Rascal demeurant 4 rue Vide-Gousset 95300 PONTOISE, au profit de Charles Adolphe Schneider demeurant au 12 rue Gaillon 8 mars 1838.

Mariage entre Joseph Antoine Compa, poëlier fumiste, demeurant rue Meslay n°24, et Agathe Alexandrine Tabernat, femme de confiance au service de madame Devéria, demeurant rue Saint-Honoré n°290 12 mars 1838.


