import os
from zipfile import ZipFile
from gensim.models.keyedvectors import KeyedVectors

vec_source = './data/wiki.fr.vec'
vec_target = './data/wiki.fr.vec.txt'
vec_zip = 'wiki.fr.vec.txt.zip'

model = KeyedVectors.load_word2vec_format(vec_source, binary=False)
model.save_word2vec_format(vec_target, binary=False)

with ZipFile(vec_zip,'w') as zip:
    zip.write(vec_target)

os.remove(vec_target)