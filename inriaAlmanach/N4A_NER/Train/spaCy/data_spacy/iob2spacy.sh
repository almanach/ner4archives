#!/bin/bash

##################################
#
# Script to convert samples in iob format to  
# binary spacy format				  
#
# WARNING : use conda env with spacy 3.X installed 
# and fr_core_news_sm downloaded							  
###################################

# Add path to data here
PATH_TO_TRAIN=../../../../N4ACorpus/EAD_release/v1_n4a/sample/n2/train.iob
PATH_TO_TEST=../../../../N4ACorpus/EAD_release/v1_n4a/sample/n2/test.iob
PATH_TO_EVAL=../../../Evaluation/baseline-evaluation/evaluation_file/gt_ner4archives.conll
OUTPUT_DIR=./v1_n4a/large/
#######################
echo "* SPACY VERSION : "
pip show spacy

echo -e "* TRAIN : \n"
python -m spacy convert -c iob -s -n 1 -b fr_core_news_sm $PATH_TO_TRAIN $OUTPUT_DIR
echo -e "* TEST : \n"
python -m spacy convert -c iob -s -n 1 -b fr_core_news_sm $PATH_TO_TEST $OUTPUT_DIR
echo -e "* EVAL : \n"
python -m spacy convert -c iob -s -n 1 -b fr_core_news_sm $PATH_TO_EVAL $OUTPUT_DIR
