## Éxpérimentaions en reconnaissance d'entités nommées dans le corpus N4A

Dernière actualisation : 27/10/2021

### Pipeline & composants NER SpaCy

* CNN (sur la base des modèles pré-entrainés)
* *finetune* Transformer (*camembert-base*)

Extra *features* :
    - [fasttext wiki_fr *embeddings*](https://fasttext.cc/docs/en/pretrained-vectors.html)
    - [fr_core_news_lg *embeddings*](https://spacy.io/models/fr)
    - [règles (*rules-based matching*)](https://spacy.io/usage/rule-based-matching)

### Pipeline NER Delft 

* BidLSTM-CRF 

----

- `Train/`
- `Models/`
- `Evaluation/`
- `Rules_based_build/`
- `raw_tests_data/`
----