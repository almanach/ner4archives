## Evaluation inter-annotateurs NER4Archives

Dernière actualisation : 01/09/2021

`Evaluation_annotation/` regroupe les outils pour calculer les scores inter-annotateurs du projet  et les résultats.

- `IAA_evaluation.ods` : Résultats des différentes évaluations (par feuilles).

- `Statistiques_IAA.ipynb` : Notebook pour calculer les scores inter-annotateurs et réaliser les graphs pour une évaluation.

- `General_stats.ipynb` : Notebook pour produire un graphique afin d'évaluer la progression sur plusieurs évaluations inter-annotateurs (fichier source : `./in/general_stats.csv`).
