﻿
# *Guidelines* d'annotations pour le projet NER4Archives

- dernière mise à jour : 10/06/2021
- Auteur : Lucas Terriel (ingénieur R&D, Inria, ALMAnaCH lab)
- Contact : <lucas.terriel@inria.fr>

<p>
<img src="https://upload.wikimedia.org/wikipedia/commons/9/9e/Inr_logo_fr_rouge.png" width="130" />
 <img src="https://www.wikimedia.fr/wp-content/uploads/2019/02/ob_b40c9f_logo-archives-nationales-gt.png" width="180" />

   <img src="https://lh3.googleusercontent.com/proxy/PSR3wSlLlYxZX6DOK3upic8FfuQPyI4AUXJRgBCFXDnNwI3xU2XZv57D7EmsoTg29P89skQErmZJczynHpxhf7m-ClS64FOJ8dRF-yo" width="65" />
</p>

## Sommaire

* Méthodologie

* Débuter avec INCEpTION

* Le schéma d'annotation des entités nommées
	* Considérations générales
	* Tableau général des types d'entités nommées

* L'annotation en bref
	* Cas d'usage des types d'entités nommées
		* LOCATION
		* PERSON
		* LEGAL
		* TITLE
		* PERIOD
		* INSTITUTION
		* INSTALLATION
		* UNKNOWN

* Le *workflow* d'annotation

* Hésitations ?


## Méthodologie :point_right:

Ces *guidelines* évoluent selon la méthodologie suivante :

- Sélection des types d'entités nommées parmi le jeu d'étiquettes de *Grobid-ner*;

- Sélection d'une série-référence d'inventaires d'archives représentatifs des services/missions scientifiques des Archives nationales ;

- Annotation de la série-référence pour tester et valider les *guidelines* durant les *workshops*
[annotation => collection des problèmes => discussion et arbitrage => adaptation des *guidelines* => annotation]

- Correction de la série-référence, sélection de cas problématiques;

- Validation des *guidelines*


## Débuter avec [INCEpTION](https://inception-project.github.io/)  :computer:

- Allez sur le lien : https://inception.dhlab.epfl.ch/
- Connectez-vous avec l'identifiant et le mot de passe qui vous a été fourni
- Sélectionnez le projet :file_folder: `NER4Archives-template` et cliquez sur :pencil2: `Annotation`
- Pour une introduction à l'interface d'INCEpTION, rendez-vous sur le [guide utilisateur](https://inception-project.github.io/releases/0.19.6/docs/user-guide.html) (*en anglais*)

## Le schéma d'annotation :label:

### Considérations générales

Ces *guidelines* se basent sur un sous-ensemble simplifié du jeu de 27 étiquettes de [Grobid-ner](https://grobid-ner.readthedocs.io/en/latest/class-and-senses/); Le jeu d'étiquettes de Grobid-ner est basé sur ceux utilisés par les outils *open-source* NER comme pour les corpus annotés Reuters/CoNLL 2003 ou MUC.

### Tableau général des types d'entités nommées

On distingue deux niveaux d'annotations :

- Priorité I
	- `LOCATION` (lieu)
	- `PERSON` (personne)
	- `LEGAL` (type d'acte)
- Priorité II
	- `TITLE` (métier)
	- `PERIOD` (date)
	- `INSTALLATION` (structure construite par des humains)
	- `INSTITUTION` (organisation de personnes et lieux ou structures qui partage le même nom)
- Autres (Priorité I & II)
	- `UNKNOWN`

## L'annotation en bref

### Cas d'usage des types d'entités

==============================================================
`LOCATION`

 Tous types de lieux comme les noms de pays, les communes, les entités administratives, les départements, les régions, les types de voies et les espaces naturels.

* :arrow_forward: Exemples

> France; France de l'occupation

> Pontoise; Paris; com. d'Amiens; baillage des montages d'Auvergne; diocèse de Rennes; district rural du Vexin; paroisse de Croizy

> Saint-Martin, rue; Paris, 32, rue de l'Université; 77, rue Neuve-des-Petits-Champs


* :heavy_exclamation_mark: Attention

	* On évitera d'annoter les codes postaux
		> `<LOCATION>`59 Rue Guynemer`</LOCATION>`, 93383 `<LOCATION>`Pierrefitte-sur-Seine`</LOCATION>`

	* On évitera d'annoter les lieux avec des élisions "d'Amiens" sauf si elle fait partie d'une phrase plus large
		> `<LOCATION>`arr. d'Amiens`</LOCATION>`
		> Mr. X est né à `<LOCATION>`Amiens`</LOCATION>`

	* En cas de double référence, annoter la dernière
		>  cant. et `<LOCATION>`arr. de Gaillon-Campagne`</LOCATION>`


==============================================================

 `PERSON`

prénom(s), noms, pseudonymes, personnages de fiction.


* :arrow_forward: Exemples

> Jean Lardeur; Louis XVI, Napoléon Ier


* :heavy_exclamation_mark: Attention

	* On évitera d'annoter la titulature (Cf. TITLE pour les titulatures et métiers)
		> roi `<PERSON>`Louis XIV`</PERSON>`

	* On peut annoter les prénoms, nom et pseudo séparés par une virgule si cela fait sens
		> `<PERSON>`Jean Le Douch`</PERSON>`, dit `<PERSON>`Mesgret`</PERSON>`

	* Dans le cas d'une co-reference on annote le dernier terme
		> Monsieur et `<PERSON>`Madame Chirac`</PERSON>`
		> Monsieur `<PERSON>`Jacques Chirac`</PERSON>`

==============================================================

`LEGAL`

Tous types d'actes administratifs, lois, conventions, jugements, traités.

* :arrow_forward: Exemples

	> Lettres de rémission; Confirmation de privilèges; Contrat de mariage; Constitution du 4 octobre 1958;  Charte constitutionnelle du 4 juin 1814

==============================================================

`TITLE`

métiers, fonctions et titulatures.

* :arrow_forward: Exemples

	> député; peintre; cardinal; bibliothécaire; marin; machiniste; sous-officier; notaire
	> roi; duchesse; vicomte; baron

* :heavy_exclamation_mark: Attention

	* On annote un `TITLE` s'il fait référence à une personne
		> Le `<TITLE>`Maire de Paris`</TITLE>` `<PERSON>`Bertrand Delanoë`</PERSON>`
		> Le projet a été proposé par le maire de `<LOCATION>`Paris`</LOCATION>` durant la session...
		> Le chercheur du `<INSTITUTION>`CNRS`</INSTITUTION>` à fait une grande découverte

==============================================================

`PERIOD`

dates absolues et périodes précises.

* :arrow_forward: Exemples

	> 1er janvier 2019; 01/12/1997; XIXe siècle
	> les trente glorieuses

* :heavy_exclamation_mark: Attention

	* On n'annote pas les heures, ni les dates relatives (mois, jour seul)
		> 00:12:23; janvier; lundi


==============================================================

`INSTALLATION`

Structures construites par les humains.

* :arrow_forward: Exemples

	> Notre-Dame de Paris; Pont du Gard; Viaduc de Millau; camp d'Auschwitz

==============================================================

`INSTITUTION`

Organisation de personnes et lieu ou structure partageant le même nom.

* :arrow_forward: Exemples

	> Université de la Sorbonne; gouvernement français; ONU; Archives nationales

==============================================================

`UNKNOWN`

En cas de doute sur la catégorie, label à utiliser.

==============================================================

## Le *workflow* d'annotation :dart:

*Ce plan est susceptible de changer*

### Session 1

Pour la première session d'annotation, on portera une attention particulière à des types d'entités selon le type d'inventaires :

- **FRAN_IR_054605** : personnes / lieux
- **FRAN_IR_053754** : personnes
- **FRAN_IR_054129** : personnes / lieux
- **FRAN_IR_058341** : lieux
- **FRAN_IR_50516** : lieux
- **FRAN_IR_000242** : personnes
- **FRAN_IR_058836** : lieux
- **FRAN_IR_001631** : personnes, lieux
- **FRAN_IR_057246** : personnes
- **FRAN_IR_001488** : lieux
- **FRAN_IR_050370** : lieux
- **FRAN_IR_000061** : personnes / lieux
- **FRAN_IR_001454** : lieux (voies de Paris et communes île-de-France)
- **FRAN_IR_041253** : personnes / lieux / types d'actes
- **FRAN_IR_058292** : personnes / lieux / types d'actes
- **FRAN_IR_050185** : lieux / personnes
- **FRAN_IR_055325** : lieux (département et communes)

### Étape 1

- A ce stade les inventaires d'archives sont pré-annotés en entités nommées (automatiquement) pour les personnes, les lieux, et les actes (parfois).

- Le but de cette étape est de corriger toutes les erreurs de l'annotation automatique, c'est-à-dire :
	- supprimer les entités mal annotées;
	- changer le type des entités qui n'ont pas été bien reconnues;
	- annoter les entités qui manquent (**seulement celles de priorité I**).


:heavy_check_mark: Une fois l'Étape 1 terminée et l'inventaire d'archive achevé vous pouvez fermer le document (bouton "Finish document" :lock_with_ink_pen:) et demander à renseigner  le tableur ici : https://docs.google.com/spreadsheets/d/1kBjF12CBX-wRhzCj8wtK925KsfFhAQn1w2eN3nz3LsE/edit#gid=0 (demander l'accès); vous pouvez passer à un autre document.

Entre l'Étape 1 et 2, une période correction est réalisée.

Les Étapes 2 et 3 peuvent être réalisées simultanément.

### Étape 2

- Dans cette étape, il s'agit de confronter les mentions à Wikidata (QID) et de relever l'identifiant Wikipage;

- Corriger les identifiants pré-annotés;

- Dans le cas où l'entité n'est **pas présente dans Wikidata**, ne rien inscrire;

- Pour les entités historiques il peut être difficile de déterminer la nature exacte du référent Wikidata (QID) . En effet, il peut exister un référent contemporain dans Wikidata pour une entité historique. On prendra le référent contemporain en cas de besoin si le référent historique n'existe pas (par exemple, "Serbie XIXe siècle" n'a pas de référent spécifique dans Wikidata. Dans ce cas on annotera "Serbie" avec l'identifiant <https://www.wikidata.org/wiki/Q403> )

- Un outil indispensable pour vérifier les bons identifiants Wikidata est le service "Term look up" (recherche du QID et de l'identifiant Wikipage avec le terme) et "Concept lookup" (recherche du terme via le QID) disponible via *Entity-fishing* (<http://nerd.huma-num.fr/nerd/">). Cependant effectuez une double vérification sur Wikidata/Wikipédia avec ces services

### Étape 3

- Dans cette étape, il s'agit de relier les entités aux identifiants des vocabulaires et référentiels des Archives nationales.

:heavy_check_mark: Une fois les Étapes 2 et 3 terminées, l'inventaire d'archives achevé, vous pouvez fermer le document (bouton "Finish document" :lock_with_ink_pen:) et demander à renseigner le tableur ici : https://docs.google.com/spreadsheets/d/1kBjF12CBX-wRhzCj8wtK925KsfFhAQn1w2eN3nz3LsE/edit#gid=0 (demander l'accès); vous pouvez passer à un autre document.

### Session 2

On recommence les étapes 1 à 3 pour les entités nommées de priorité II

## Hésitations ? :information_source:

### FAQ 

* Comment étiqueter une entité nommée dans Inception ?

1) Dans la fenêtre d'annotation, sélectionnez dans la liste déroulante `Named entity`
2) Annotez le token avec la souris
3) Dans le champ `value` , cliquez sur la flêche pour dérouler la liste et choisissez le label correspondant

* Comment trouver le lien Wikidata ?
1) Dans la fenêtre d'annotation, sélectionnez dans la liste déroulante `Entity linking`
2) Annotez le token avec la souris et saisissez en plein texte dans `uri_wikidata` la valeur du token
3) Des candidats apparaissent, cliquez sur la bonne valeur (effectuez les vérifications)


* Comment trouver l'identifiant de la page Wikipédia ?
1) Dans la fenêtre d'annotation, sélectionnez dans la liste déroulante `Entity linking`
2) Annotez le token avec la souris et saisissez en plein texte l'identifiant de la page Wikipédia dans le champ `uri_wikipedia`
3) Bien vérifier que l'identifiant de la page Wikipédia renvoi au concept wikidata (QID)

### Vérifier

Si vous souhaitez confirmer, utilisez les ressources suivantes :

- [Trésor de la langue française](http://atilf.atilf.fr/)
- [Wikipédia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal)
- [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)
- [Entity-fishing](http://nerd.huma-num.fr/nerd/)

Dans le cas où les vérifications avec les ressources ci-dessus/Wikipédia ne donnent pas d'informations, passez l'annotation.

### Reporter les cas problématiques

Dans les cas de doutes, envoyez un mail ou postez une issue sur le  GitLab du projet [ici](https://gitlab.inria.fr/almanach/ner4archives/-/issues), avec vos questions, commentaires, captures d'écran.
