# INCEPTION CLI

Cet outil permet d'intéragir avec une instance de l'outil d'annotation [INCEpTION](https://inception-project.github.io/) afin de réaliser les tâches suivantes : 

- Visualiser l'ensemble des documents disponibles dans le projet ; 
- Exporter l'ensemble du projet et les données associées dans un dossier compréssé (zip) ;
- Exporter seulement les documents corrigées (suivant un niveau `progress` ou `complete`) depuis INCEpTION dans le format souhaité (xmi / text / tei / conll) ;
- Exporter les annotations d'un document spécifique présent dans le projet ;

## Pré-requis 

- Python  >= 3.7
- Accès à une instance d'INCEpTION et disposer des droits `manager` sur les projets.

## Dépendances

- click==8.0.1 : création de CLI
- [pycaprio==0.2.0](https://pycaprio.readthedocs.io/en/latest/) : client Python (API) pour accéder à l'outil d'annotation INCEpTION

## Installation

### A. Côté Python

1. Cloner le dépôt 

```bash
$ git clone https://gitlab.inria.fr/almanach/ner4archives
```
2. Se rendre dans le répertoire de la CLI

```bash
$ cd inriaAlmanach/N4ACorpus/InceptionCLI/
```
3. Créer un environnement virtuel

```bash
$ virtualenv -p python3.7 inception_cli
```

4. Puis

```bash
$ source inception_cli/bin/activate
```
5. Installer les dépendances via le gestionnaire pip

```bash
$ pip install -r requirements.txt
```

### B. Côté environnement OS  

6. Initialiser vos variables d'environnement dans le terminal 

```bash
$ (inception_cli) export INCEPTION_HOST=<http://your-inception-host.com>
$ (inception_cli) export INCEPTION_USERNAME=<remote-user>
$ (inception_cli) export INCEPTION_PROJECT=<project-name>
$ (inception_cli) export INCEPTION_PASSWORD=<password>
```

7. rendre exécutable le script bash
```bash 
$ (inception_cli) chmod +x commandsInception.sh
```

## Utilisation

```bash 
$ (inception_cli) ./commandsInception.sh
```
et suivre les indications du terminal.

pour changer de projet, réinitialiser la variable d'environnment `INCEPTION_PROJECT`

