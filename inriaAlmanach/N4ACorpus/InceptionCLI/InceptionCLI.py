#!/usr/bin/env python
# coding: utf-8

"""InceptionCLI (stable)

This CLI require an account with specific privileges on Inception 

Tasks :
    * Export all project from INCEpTION tool
    * Export only curated documents (in progress or complete) from INCEpTION tool
    * Export annotations in specific document from INCEpTION tool

TODO(@Lucas) : Add helpers in CLI commands manager
TODO(@Lucas) : Better manage errors with shell script

----------------------------------------------------------------------
-- use InceptionCLI through ./commandsInception.sh is more easier ! --
----------------------------------------------------------------------

~~ Inspired and adapted from scripts in Impresso lib and EpibauCorpus (Matteo Romanello, DH Lab, EPFL)
for NER4ARchives project. ~~

"""

from io import BytesIO
from os import environ, path
import logging

import click
from pycaprio import Pycaprio
from pycaprio.core.mappings import AnnotationState, DocumentState
from pycaprio.mappings import InceptionFormat
from tqdm import tqdm
import zipfile

__author__ = "Lucas Terriel"
__email__ = "lucas.terriel@inria.fr"
__organisation__ = "ALMANACH Lab, Inria"
__status__ = "development"


# Customise your own export format here (see https://github.com/Savanamed/pycaprio/blob/master/pycaprio/core/mappings.py and Export
# format in INCEpTION platform)
EXPORT_FORMAT = {
    'xmi' : InceptionFormat.XMI,
    'text': InceptionFormat.TEXT,
    'tei': InceptionFormat.TEI,
    'conll' : 'conll2002',
}

EXTENSION = {
    'xmi' : 'xmi',
    'text': 'txt',
    'tei': 'xml',
    'conll' : 'conll',
}

LOGGER = logging.getLogger(__name__)

def get_key(dict_per, val):
    for key, value in dict_per.items():
         if val == value:
             return key

def _find_projectid_by_name(inception_client: Pycaprio, project_name: str) -> int:
    """Finds a project id in INCEpTION by its name.
    """
    matching_projects = [project for project in inception_client.api.projects() if project.project_name == project_name]
    assert len(matching_projects) == 1
    return matching_projects[0].project_id

class ClientInception:
    def __init__(self, 
                api_endpoint, 
                username, 
                password,
                project_name='',
                curation_state='',
                filter_name='',
                output_dir='',
                export_format='',
                document_name=''):
        self.api_endpoint = api_endpoint
        self.username = username
        self.password = password
        self.project_name = project_name
        self.curation_state = curation_state
        self.filter_name = filter_name
        self.output_dir = output_dir
        self.export_format = export_format
        self.document_name = document_name
        try:
            self.client = Pycaprio(api_endpoint, authentication=(username, password))
            LOGGER.warning(
        (
            '[INFO] * Using following INCEpTION connection parameters (from environment) => \n'
            f"Host: {self.api_endpoint}; user: {self.username}; password: {'set (hidden for security)' if len(self.password) != 0 else '⚠️ not set'}"
        )
    )
        except:
            LOGGER.error(('⚠️ Cannot establish connexion, sorry.'))

        try:
            self.project_id = _find_projectid_by_name(self.client, project_name=project_name)
            print(self.project_id)
            LOGGER.warning(
        (
            f"[INFO] * Project {self.project_name} has ID {self.project_id}\n"
        )
    )
        except:
            LOGGER.error(('⚠️ Cannot find ID of project.'))


    def fetch_documents(self):
        documents = self.client.api.documents(self.project_id)
        return documents

    def download_annotations_document(self):
        try:
            documentsID_to_export = [doc for doc in self.fetch_documents() if doc.document_name.startswith(self.document_name)]
            print(documentsID_to_export)
            for document in tqdm(documentsID_to_export):
                annotation_content = self.client.api.document(self.project_id, document.document_id, document_format=self.export_format)
                extension = EXTENSION[get_key(EXPORT_FORMAT, self.export_format)]
                with open(f"{self.output_dir}{path.splitext(document.document_name)[0]}.{extension}", 'wb') as annotation_file:
                    annotation_file.write(annotation_content)
            LOGGER.warning((f'\n✔️ All documents export with success in {self.output_dir}\n'))
            return True
        except Exception as e:
            LOGGER.error((f'⚠️ Cannot Export documents : {e}'))
            return False
   

    def download_curated_documents(self):
        try:
            curations = []
            documents = self.fetch_documents()
            for document in tqdm(documents):
                if document.document_state == self.curation_state:
                    if not document.document_name.startswith(self.filter_name):
                        LOGGER.warning((f"✔️ Document {document.document_id} {document.document_name} is {document.document_state} and will be downloaded"))
                        curated_content = self.client.api.curation(self.project_id, document, curation_format=self.export_format)
                        if self.export_format == InceptionFormat.XMI:
                            curations.append(curated_content)
                            for curation in curations:
                                z = zipfile.ZipFile(BytesIO(curation))
                                z.extractall(self.output_dir)
                        else:
                            extension = EXTENSION[get_key(EXPORT_FORMAT, self.export_format)]
                            with open(f"{self.output_dir}{path.splitext(document.document_name)[0]}.{extension}", 'wb') as annotation_file:
                                annotation_file.write(curated_content)
            return True
        except Exception as e:
            LOGGER.error((f'⚠️ Cannot Export documents curated : {e}'))
            return False

    def export_all_project(self):
        LOGGER.warning((f'\nExport in progress [Take a coffee/tea break 🍵] ...\n'))
        try:
            content = self.client.api.export_project(self.project_id, project_format=self.export_format) 
            with open(f"{self.output_dir}_{self.project_name}.zip", 'wb') as zip_file:
                zip_file.write(content)
            LOGGER.warning((f'\n✔️ All project {self.project_name} export with success in {self.output_dir}\n'))
            return True
        except Exception as e:
            LOGGER.error((f'⚠️ Cannot Export all project : {e}'))
            return False

@click.command()
@click.option('--username', '-user', type=str)
@click.option('--password', '-pwd', type=str)
@click.option('--api_endpoint', '-host', type=str)
@click.option('--project_name', '-pname', type=str)
@click.option('--output_dir', '-od', type=str, default='/')
@click.option('--filter_name', '-fn', type=str)
@click.option('--curation_state', '-cs', type=str)
@click.option('--is_doc_export', '-doc', is_flag=True)
@click.option('--is_batch_export', '-batch', is_flag=True)
@click.option('--show_docs', '-show', is_flag=True)
@click.option('--is_all', '-all', is_flag=True)
@click.option('--export_format', '-exp', type=str, default='xmi')
@click.option('--document_name', '-docn', type=str)
def cli(username, 
        password, 
        api_endpoint,
        project_name,
        output_dir,
        filter_name,
        curation_state,
        is_doc_export,
        is_batch_export,
        show_docs,
        is_all,
        export_format,
        document_name):
    # Check Inception wrapper values
    username = username if username else environ['INCEPTION_USERNAME']
    password = password if password else environ['INCEPTION_PASSWORD']
    api_endpoint = api_endpoint if api_endpoint else environ['INCEPTION_HOST']

    # Inspect output directory
    if not output_dir.endswith('/'):
        output_dir = output_dir + '/'

    # Check curation state if export curated documents
    if curation_state:
        if curation_state.lower() == "progress":
            #curation_state = 'CURATION-IN-PROGRESS'
            curation_state = DocumentState.CURATION_IN_PROGRESS
        if curation_state.lower() == "complete":
            #curation_state = 'CURATION-COMPLETE'
            curation_state = DocumentState.CURATION_COMPLETE

    
    # Check export format
    if export_format:
        export_format = EXPORT_FORMAT[export_format.lower()]

    # Check filter name not None
    if filter_name is None:
        filter_name = 'XXXX'

    # Create a client
    inception_client = ClientInception(api_endpoint=api_endpoint, 
    username=username, 
    password=password, 
    project_name=project_name, 
    curation_state=curation_state, 
    filter_name=filter_name,
    output_dir=output_dir,
    export_format=export_format,
    document_name=document_name)

    # Display selected list
    if show_docs:
        if curation_state == DocumentState.CURATION_IN_PROGRESS or curation_state == DocumentState.CURATION_COMPLETE:
            print(f"Documents are available with curation state {curation_state} : \n")
            for doc in inception_client.fetch_documents():
                if curation_state == doc.document_state:
                    if not doc.document_name.startswith(filter_name):
                        print(doc.document_id, doc.document_name, doc.document_state)
        else:
            print("Documents are available : \n")
            for doc in inception_client.fetch_documents():
                if not doc.document_name.startswith(filter_name):
                    print(doc.document_id, doc.document_name, doc.document_state)

    if is_doc_export:
        inception_client.download_annotations_document()

    if is_batch_export:
        inception_client.download_curated_documents()

    if is_all:
        inception_client.export_all_project()
                    
if __name__ == '__main__':
    cli()
