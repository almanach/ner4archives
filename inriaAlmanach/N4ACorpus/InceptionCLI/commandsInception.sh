#!/bin/bash

##################################
#
# ~~ Script Inception CLI  ~~
#
# Date : 31/08/2021
# Auteur : Lucas Terriel				  
#								  
###################################

# Set Python CLI with root privilege
chmod +x ./InceptionCLI.py

# Display title
echo -e '\n************************'
echo 'WELCOME TO INCEPTION CLI'
echo -e '************************\n'

######################################################################################
# Utils functions 

check_env_vars () {
    for name; do
    : ${!name:?$name must not be empty}
    done
}

list_documents () {
    echo -e "\n *** List documents in a project *** \n"
    read -p "[OPTIONNAL] Give a curation state for documents (enter 'progress' or 'complete' or empty as '' to pass) : " curation_state
    ./InceptionCLI.py -pname $INCEPTION_PROJECT -cs $curation_state -show
}

export_all_project () {
    echo -e "\n *** Export all project locally *** \n"
    read -p '[REQUIRED] Give your output directory path : ' out_dir
    ./InceptionCLI.py -pname $INCEPTION_PROJECT -od $out_dir -all
}

export_curated_documents () {
    echo -e "\n *** Export curated documents locally *** \n"
    read -p "[REQUIRED] Give a curation state for documents (enter 'progress' or 'complete') : " curation_state
    read -p "[REQUIRED] Format of documents ('xmi' / 'text' / 'tei' / 'conll') : " export_format
    read -p '[REQUIRED] Give your output directory path : ' out_dir
    ./InceptionCLI.py -pname $INCEPTION_PROJECT -od $out_dir -cs $curation_state --export_format $export_format --is_batch_export
    sleep 5

    # check extension var
    if [[ $export_format == "text" ]]
    then
        export_format="txt"
    elif [[ $export_format == "tei" ]]
    then 
        export_format="xml"
    else
        export_format=$export_format
    fi

    cd $out_dir

    # add prefix "curated_" for export file
    for FILENAME in *.$export_format; do 
            mv $FILENAME "curated_"$FILENAME
        done
    cd ..
}

export_specific_document () {
    echo -e "\n *** Export a specific documents locally *** \n"
    echo -e "N.B : Only annotations mark as COMPLETE / FINISHED in INCEpTION are download"
    read -p "[REQUIRED] Format of documents ('text' / 'tei' / 'conll') : " export_format
    read -p '[REQUIRED] Give your document name (prefix) : ' doc_name
    read -p '[REQUIRED] Give your output directory path : ' out_dir
    ./InceptionCLI.py -pname $INCEPTION_PROJECT --document_name $doc_name -od $out_dir --export_format $export_format --is_doc_export

}

exit_gate () {
    echo "Bye !" ; exit 1
}

######################################################################################

# MAIN PART 

# 1) Check variables env and Connexion to HOST INCEpTION if env variable not exist
if ! check_env_vars "INCEPTION_HOST" "INCEPTION_USERNAME" "INCEPTION_PASSWORD" "INCEPTION_PROJECT"; then
    read -p 'Host API : ' hostvar
    read -p 'Username : ' uservar
    read -p 'Name annotation project : ' projectvar
    read -sp 'Password : ' passvar

    export INCEPTION_HOST=$hostvar
    export INCEPTION_USERNAME=$uservar
    export INCEPTION_PROJECT=$projectvar
    export INCEPTION_PASSWORD=$passvar   
fi



# 2) Display menu in switchcase statement
while true; do
        # Can add more options here
        options=("List all documents in your annotation project" "Export all project in INCEpTION (zip file)" "Export only curated documents in INCEpTION" "Export specific documents in INCEpTION" "Exit program")
        # 3) Choose action
        echo -e "Choose an action : \n"
        select opt in "${options[@]}"; do
            case $REPLY in 
                1) list_documents; break ;;
                2) export_all_project; break ;;
                3) export_curated_documents; break ;;
                4) export_specific_document; break ;;
                5) exit_gate; break;;
                *) echo "Sorry cannot understand" >&2
            esac
        done
done
# 4) And repeat ...







