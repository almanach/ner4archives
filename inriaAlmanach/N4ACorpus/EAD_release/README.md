### Description EAD_release/

Ce répertoire continent les données annotés et corrigés en bout de chaîne, chaque corpus est versionné selon le schéma suivant : 

```
vX_n4a (numéro de la version + nom projet)
|
|- conll
|   |
|   |- annotations (ensemble des annotations en CONLL 2002 corigées  
|   |               et exportées depuis INCEpTION)
|   |- un fichier CONLL "all" qui merge l'ensemble des fichiers CONLL
|   
|- xmi_curated (annotations corrigées en xmi exportées depuis INCEpTION + TypeSystem.xml)
|
|- xmi_retokenized (annotations corrigées et retokénisées en XMI pour les importer dans INCEpTION
|                   pour prérarer la sortie CONLL) 
|
|-sample (fichiers sérialisés pour l'entraînement des 
|         modèles NER) : n2/ (train/test) et n3/ (train/test/validation) + fichier 
|         non sérialisé avec traitements 
|
```

#### Informations et statistiques sur l'état de chacun des datasets (Cf. `../Statistiques_dataset.ipynb`) :

- `v_0/` | `v_beta/` : corpus pour les tests de *pipeline* (ne pas utiliser)

- `v_1_n4a/` (10/2021) :

    * **Source** : 8 instruments recherche en XML-EAD des AN
    * **Classes** : LOCATION, PERSON, ORGANISATION, TITLE, EVENT
    * **Phrases annotées** : 872 / 106 513
    * **Total entités** : 2 241 
    * **Répartition des entités** : 

    | LABEL        |      TOTAL    |
    |--------------|:-------------:|
    | LOCATION     |  797          |
    | PERSON       |  495          |
    | ORGANISATION |  490          |
    | TITLE        |  431          |
    | EVENT        |  28           |

