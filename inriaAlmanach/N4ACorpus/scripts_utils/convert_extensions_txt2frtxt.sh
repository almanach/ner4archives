#!/bin/bash
# Script pour renommer tout les *.txt en *.fr.txt


# change the path
for f in ./*.txt;
	do mv -- "$f" "${f%.txt}.fr.txt"; 
done

echo '=== * Tous les fichiers sont renommés * ==='