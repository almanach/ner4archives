<?xml version="1.0" encoding="UTF-8"?>

<!-- 
* Conversion EAD vers TEI 
* 
* Input : Inventaire d'archives en XML conforme DTD EAD 2002
* Output : Format pivot TEI 
*
* Auteur : Lucas Terriel
* Date : 26/05/2021

~~ CROSSWALK SCHEMA ~~

    |
    |
    |- <text> : contient le corpus textuel dans la strutucture générale TEI
    |    |
    |    |- <body> : contient l'ensemble du texte à annoter
    |    |    |
    |         |- <div> : sub-division de l'inventaire contenant une description archivistique (<c> [EAD])
    |         |    |
    |         |    |- <p> : regroupement des éléments de description archivistique contenu dans le <did> [EAD] et le <scopecontent> [EAD]
    |                  |
    |                  |- <s> (s-unit) : contient la valeur textuelle dans une division/segmentation de type phrase de l'élément de description (<unitid>, <unittitle>, <unitdate>, <p> [EAD])
    |
    |
    |
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs" version="2.0">
    <xsl:output indent="yes" method="xml" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    
   

    <xsl:template match="/">
        <TEI>
            <teiHeader>
                <fileDesc>
                    <titleStmt>
                        <title>
                            <xsl:value-of select="substring-before(tokenize(base-uri(.), '/')[last()], '.xml')"/>
                        </title>
                    </titleStmt>
                    <publicationStmt>
                        <authority>Archives nationales</authority>
                        <date>
                            <xsl:value-of
                                select="format-dateTime(current-dateTime(), '[D]/[M]/[Y]-[H]:[m]')"
                            />
                        </date>
                    </publicationStmt>
                    <sourceDesc>
                        <ab>Format pivot TEI pour le transfert des fichiers inventaires en XML
                            conforme à la DTD EAD 2002 dans le cadre du projet NER4ARchives
                            (Archives nationales/Inria).</ab>
                    </sourceDesc>
                </fileDesc>
            </teiHeader>

            <!-- Récupération du niveau <dsc> dans le <text> -->

            <xsl:element name="text">
                <xsl:element name="body">
                    <xsl:apply-templates select="//dsc"/>
                </xsl:element>
            </xsl:element>

        </TEI>
    </xsl:template>


    <!-- Récupération du <c> général et des <c> inférieurs-->

    <xsl:template match="c">
        <xsl:element name="div">
            <xsl:apply-templates />
            <!--
            <xsl:for-each select="c">
                <xsl:element name="div">
                        <xsl:apply-templates select="did | scopecontent" />
                </xsl:element>
            </xsl:for-each>-->
        </xsl:element>
    </xsl:template>



    <!-- Traitements pour les <did> -->

    <xsl:template match="did">

        <xsl:variable name="unitid" select="unitid"/>
        <xsl:variable name="unittitle" select="unittitle"/>
        <xsl:variable name="unitdate" select="unitdate"/>
        
        <xsl:element name="p">
            
        <xsl:if test="$unitid">
            <xsl:element name="s">
                <xsl:value-of select="$unitid"/>
            </xsl:element>
        </xsl:if>

        <xsl:if test="$unittitle">
            <xsl:for-each select="$unittitle">
                <xsl:element name="s">
                    <xsl:value-of select="normalize-space($unittitle)"/>
                </xsl:element>
            </xsl:for-each>
        </xsl:if>

        <xsl:if test="$unitdate">
            <xsl:element name="s">
                <xsl:value-of select="$unitdate"/>
            </xsl:element>
        </xsl:if>
        </xsl:element>
            
        
    </xsl:template>

    <!-- Traitements pour les <scopecontent> -->

    <xsl:template match="scopecontent">
        <xsl:element name="p">
            <xsl:for-each select="./* |./list/*">
            <xsl:if test="./text() | ./list/text()">
            <xsl:element name="s">
                <xsl:value-of select="normalize-space(.)"/>
            </xsl:element>
            </xsl:if>
        </xsl:for-each>
        </xsl:element>
    </xsl:template>
    
    <!-- Tags ignore -->
    <xsl:template match="altformavail"></xsl:template>
    <xsl:template match="acqinfo"></xsl:template>
    <xsl:template match="physdesc"></xsl:template>
    <xsl:template match="langmaterial"></xsl:template>
    <xsl:template match="repository"></xsl:template>
    <xsl:template match="physloc"></xsl:template>
    <xsl:template match="arrangement"></xsl:template>
    <xsl:template match="controlaccess"></xsl:template>
    <xsl:template match="daogrp"></xsl:template>
    <xsl:template match="userestrict"></xsl:template>
    <xsl:template match="accessrestrict"></xsl:template>
    <xsl:template match="relatedmaterial"></xsl:template>
    <xsl:template match="separatedmaterial"></xsl:template>
    <xsl:template match="bioghist"></xsl:template>
    <xsl:template match="custodhist"></xsl:template>
    <xsl:template match="appraisal"></xsl:template>
</xsl:stylesheet>
