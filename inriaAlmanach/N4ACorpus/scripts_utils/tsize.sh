#!/bin/bash

##################################
#
# ~~ Script statistiques minimales documents ~~
#
# * Total de documents
# * Taille total des fichiers d'un dossier (Ko)
# * Nombre de lignes moyennes pour l'ensemble des fichiers
# * Nombre de mots moyen pour l'ensemble des fichiers
#
# Date : 27/04/2021
# Auteur : Lucas Terriel				  
#								  
###################################

TotalSize=0
NumberDocuments=0
AvgLines=0
AvgWords=0
TotalLines=0
TotalWords=0

if [[ $1 == "-h" ]]; then
	echo "Usage : path_to_files_with_*_extensions"; exit 1
elif [[ $# == 0 ]]; then 
	echo "Sorry, nothing to compute"; exit 0
else
	for file in "$@" ; do
		# Step control file
		[[ -f $file ]] || continue 

		# Step to size, lines and words compute
		sizefile=$(ls -s -k $file | cut -d " " -f1)
		countlines=$(wc -l $file | cut -d " " -f1)
		countwords=$(wc -w $file | cut -d " " -f1)

		# Step to create totals
		TotalSize=$(( $TotalSize + $sizefile ))
		TotalLines=$(( $TotalLines + $countlines ))
		TotalWords=$(( $TotalWords + $countwords ))
		NumberDocuments=$(( $NumberDocuments + 1 ))
	done

	# Compute Average with totals
	AvgLines=$(( $TotalLines / $NumberDocuments ))
	AvgWords=$(( $TotalWords / $NumberDocuments ))

	# Show results
	echo -e "\n"
	echo -e "|--------------------------------------------------------------------------------------------------------|"
	echo -e "|                                   RESULTS                                                              |"
	echo -e "|--------------------------------------------------------------------------------------------------------|"
    echo -e "|\tNb Docs         |\tTotal Size (Ko)\t|\tAvg. lines\t|\tAvg. tokens (words)\t |"
    echo -e "|-----------------------|-----------------------|-----------------------|--------------------------------|"
    echo -e "|\t$NumberDocuments files\t|\t$TotalSize Ko         |\t~ $AvgLines lines    |\t~ $AvgWords words \t         |\n"; exit 0
 	echp -e "\n"
fi
