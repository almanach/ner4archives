#!/bin/bash

###############################
# SCRIPT TO CONVERT XML TO TEXT
###############################

# Initialize directory 
PATH_XML = data/corpus/corpus-long/aida/RawText/*.xml

# Initialize Xpath to cath the content of XML
XPATH_VALUE = "//newsitem//text()"

for file in $PATH_XML; 
	do xmllint --xpath $XPATH_VALUE "$file" > "${file/%xml/txt}"; 
done