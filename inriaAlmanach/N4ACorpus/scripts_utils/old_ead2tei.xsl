<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    
=====================================================================================
*
*
* XSLT stylesheet for an EAD to TEI conversion as part 
* of the NER4archives project.
* 
* Input : Finding aids in XML conforming to DTD EAD 2002.
* Output : TEI pivot format.
*
* Author : Lucas Terriel <lucas.terriel@inria.fr>
* Date : 26/05/2021
*
* Note : For batch process, use collection() XPath function. 
*
* This script is free to reuse according to the terms
* of the Creative Commons Attribution license.
*
=====================================================================================


~~ CROSSWALK SCHEMA ~~

    |- <teiHeader> : "Light" metadata like current date, file name and project description.
    |
    |- <text> : Textual corpus in the general TEI structure.
    |    |
    |    |- <body> : All the text to be annotated.
    |    |    |
    |         |- <div> : Division of the finding aid containing an archival description of a documentary unit (<c> [EAD]) : @xml:id (identifier in EAD), @type (level of documentary unit).
    |         |    |
    |         |    |- <div> : Subdivision corresponding either to the identification and description (<did> [EAD]) or to the presentation of the content (<scopecontent> [EAD]) : @type (tag in EAD).
    |                  |
    |                  |- <ab> (anonymous block) : Textual value in different context (<unitid>, <unittitle>, <unitdate>, <p>, <list>/<item> [EAD]).
 
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs" version="3.0" expand-text="yes">

    <xsl:output indent="yes" method="xml" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>



    <xsl:template match="/">

        <xsl:variable name="filename"
            select="substring-before(tokenize(base-uri(.), '/')[last()], '.xml')"/>
        <xsl:variable name="datenow"
            select="format-dateTime(current-dateTime(), '[D]/[M]/[Y]-[H]:[m]')"/>

        <!-- Main canvas -->
        <TEI>
            <teiHeader>
                <fileDesc>
                    <titleStmt>
                        <title>
                            <xsl:value-of select="$filename"/>
                        </title>
                    </titleStmt>
                    <publicationStmt>
                        <authority>Archives nationales</authority>
                        <date>
                            <xsl:value-of select="$datenow"/>
                        </date>
                    </publicationStmt>
                    <sourceDesc>
                        <ab>Format pivot TEI pour le transfert des fichiers inventaires en XML
                            conforme à la DTD EAD 2002 dans le cadre du projet NER4ARchives
                            (Archives nationales/Inria).</ab>
                    </sourceDesc>
                </fileDesc>
            </teiHeader>
            <text>
                <body>
                    <xsl:apply-templates select="//dsc"/>
                </body>
            </text>
        </TEI>

    </xsl:template>


    <!-- Recovery of high-level <c> and sub-levels <c> -->

    <xsl:template match="c">
        <div xml:id="{@id}">
            <xsl:if test="@level">
                <xsl:attribute name="type" select="@level"/>
            </xsl:if>
            <xsl:apply-templates/>
        </div>
    </xsl:template>



    <!-- Specific actions for <did> -->

    <xsl:template match="did">

        <xsl:variable name="unitid" select="unitid[normalize-space() != '']"/>
        <xsl:variable name="unittitle" select="unittitle[normalize-space() != '']"/>
        <xsl:variable name="unitdate" select="unitdate[normalize-space() != '']"/>

        <div type="did">



            <xsl:if test="$unitid">
                <ab>{$unitid}</ab>
            </xsl:if>

            <xsl:if test="$unittitle">
                <xsl:for-each select="$unittitle">
                    <ab>{normalize-space(.)}</ab>
                </xsl:for-each>
            </xsl:if>


            <xsl:if test="$unitdate">
                <ab>{$unitdate}</ab>
            </xsl:if>

        </div>

    </xsl:template>

    <!-- Specific actions for <scopecontent> -->

    <xsl:template match="scopecontent[normalize-space() != '']">
        
        <xsl:variable name="p" select="p[normalize-space() != '']"/>
        <xsl:variable name="list_item" select="list/item[normalize-space() != '']"/>
        <xsl:variable name="note_p" select="note/p[normalize-space() != '']"/>

        <div type="scopecontent">
           <xsl:for-each select="$p | $list_item | $note_p ">
               <ab>{normalize-space(.)}</ab>
           </xsl:for-each>
            
        </div>

    </xsl:template>
    
 
    



    <!-- Tags to ignore -->
    <xsl:template match="altformavail"/>
    <xsl:template match="acqinfo"/>
    <xsl:template match="physdesc"/>
    <xsl:template match="langmaterial"/>
    <xsl:template match="repository"/>
    <xsl:template match="physloc"/>
    <xsl:template match="arrangement"/>
    <xsl:template match="controlaccess"/>
    <xsl:template match="daogrp"/>
    <xsl:template match="userestrict"/>
    <xsl:template match="accessrestrict"/>
    <xsl:template match="relatedmaterial"/>
    <xsl:template match="separatedmaterial"/>
    <xsl:template match="bioghist"/>
    <xsl:template match="custodhist"/>
    <xsl:template match="appraisal"/>
    <!-- ~~~~~~~~~~~ -->

</xsl:stylesheet>
