#!/usr/bin/perl

use 5.010;
use strict;
use warnings;

use XML::LibXML;

###############################
# SCRIPT TO CONVERT XML TO TEXT
###############################

# Initialize file 
my $filename = './dataset/FRAN_IR_000061.xml';

# Initialize destination
my $des = 'test_p.txt';

# Initialize xpath
my $xpath = '//unittitle | //unitdate | //scopecontent';

my $dom = XML::LibXML->load_xml(location => $filename);

open(fh, '>', $des) or die $!;

foreach my $title ($dom->findnodes($xpath)) {

	print fh $title->to_literal();

}

close(fh) or "Couldn't close the file."

# TODO : - resolve bad encoding / tabulation  
