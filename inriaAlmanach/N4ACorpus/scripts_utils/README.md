- `old_ead2tei.xslt` (schéma TEI défaillant/pas intéropérable avec Inception mais qui respecte le mieux la structure EAD) : feuille de style XSLT pour transformer les fichiers XML EAD en TEI pour l'intégration dans Inception. Consulter le schéma TEI directement dans la *docstring* du fichier.

- `ead2tei.xslt` (schéma TEI intéropérable avec Inception, utilisé pour `EAD/src_prepared/EAD_TEI_V2`) : feuille de style XSLT pour transformer les fichiers XML EAD en TEI pour l'intégration dans Inception. Consulter le schéma TEI directement dans la *docstring* du fichier.

- `tsize.sh` : calcul du nombre total de documents, taille (Ko), moyenne total de lignes, et de mots. exemple de commande : ```$ bash tsize.sh ../EAD/src_prepared/Rawtext_V2_allcontent/*.txt```

- `convert_extensions_txt2frtxt.sh` : script pour changer les extensions de fichiers .txt en .fr.txt pour *Entity-fishing* s'exécute directement dans le dossier cible.

- `xml2txt.pl`, `xml2txt.sh` : scripts permettant des récupérations rapides de fichiers textes à partir du contenu des fichiers XML (sortie pas forcément concluante suivant le XML fourni et son niveau d'arborescence).