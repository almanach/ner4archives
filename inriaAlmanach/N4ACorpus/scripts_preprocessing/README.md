### Scripts_preprocessing/

* `Retokenization_xmi` : script pour la retokenisation des fichiers XMI corrigés 
* `conll2samples.ipynb` : script pour réaliser l'ensemble des post-traitements sur les CONLL corrigés (utiliser le fichier `environment.yml` pour créer un env conda)
* `others/` : dossier comprenant d'autres scripts de traitements utiles.
