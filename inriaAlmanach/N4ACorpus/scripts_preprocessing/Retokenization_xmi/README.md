## Retokenisation des fichiers XMI 

### Description

Suivant la phase d'annotation et de correction des fichiers dans INCEpTION et avant la phase d'export du dataset en CONLL ; il est nécéssaire de résoudre la tokenisation des fichiers XMI en sortie d'INCEpTION qui porte sur les points suivants : 

* résolution des apostrophes.

    >  "d'Amiens" => ["d'",, "Amiens"]

    >  "l'Estuaire de l'Aa" => ["l'", "Estuaire", "de", "l'", "Aa"]

    Une exception cependant pour quelques formes d'entités composées ou non de tirets et qui comporte des apostrophes, qui ne sont pas prise en compte dans la retokenisation.

    > "Val-d'Oise", "aujourd'hui"

* Les caractères non Unicode sont remplacés par des *underscores*

### Mode opératoire du script de retokenisation

* Préalable : créer un environnement virtuel à partir du fichier `requirements.txt`

1. Placer les fichiers XMI annotés et corrigés dans le dossier `annotations/` (pour récupérer les fichiers depuis INCEpTION, utiliser la CLI `../InceptionCLI/` ou exporter manuellement depuis la plateforme). Ne pas oublier de récupérer le fichier XML `TypeSystem.xml` qui vient avec les XMI.

2. Depuis le dossier `scripts_preprocessing/` lancer la commande : 

```
$ python retokenize_xmi.py -i ./annotations/ -s ./TypeSystem.xml -o ./out_retokenize/
```

3. Suivre les indications dans le terminal, une fois le processus terminé les fichiers tokenisés sont disponible dans `out_retokenize/`. Un fichier `retokenization.log` est égallement généré et permet de contrôler les opérations réalisées.