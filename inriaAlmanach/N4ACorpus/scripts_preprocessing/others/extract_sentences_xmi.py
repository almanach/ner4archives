#!/usr/bin/env python 
#-*- coding: utf-8 -*-

"""
Script to extract only sentences that contains entities from XMI to text source document.

Author : Lucas Terriel <lucas.terriel@inria.fr>
date : 23/09/2021
"""


from os import walk
import sys

from cassis import *
from tqdm import tqdm

if __name__ == "__main__":

    #   -- Enter parameters --
    dir = "../v_0/xmi/annotations/"
    output_file = "../v_0/text/all_normalize_xmi.txt"
    #################################################

    for root, dirs, files in walk(dir):
        files = files

    typesystem_file = [file for file in files if file == "TypeSystem.xml"][0]
    xmi_files = sorted([file for file in files if file != "TypeSystem.xml"])

    with open(dir + typesystem_file, 'rb') as f:
        typesystem = load_typesystem(f)


    final_file = open(output_file, mode="w", encoding="utf-8")
    with tqdm(xmi_files) as xmi_files_pbar:
        for xmi in xmi_files_pbar:
            xmi_files_pbar.set_description(f'Process sentences from {xmi}')
            sentences = []
            with open(dir + xmi, 'rb') as f:
                cas = load_cas_from_xmi(f, typesystem=typesystem)
                for sentence in cas.select('de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence'):
                    sentence_string = sentence.get_covered_text()
                    if token in cas.select_covered('de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity', sentence):
                        for token in cas.select_covered('de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity', sentence):
                            if sentence_string not in sentences:
                                sentences.append(sentence_string)
                                final_file.write(f"{str(sentence_string)}\n")
                    


    final_file.close()