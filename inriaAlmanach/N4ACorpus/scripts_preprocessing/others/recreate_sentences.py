#!/usr/bin/env python 
#-*- coding: utf-8 -*-

"""
Script to preprocessing text output from XML to create a source document.

Author : Lucas Terriel <lucas.terriel@inria.fr>
date : 22/09/2021
"""

import re

def recreate_sentences(filepath: str) -> None:
    fo = open(file='../v_0/text/up/all_normalize_2.txt', mode='w', encoding='utf-8')
    with open(file=filepath, mode='r', encoding='utf-8') as fi:
        file = fi.read()
        # iterate over a line as list item
        for row in file.splitlines():
            # remove large spaces between sentences (cause of XML convertion)
            text = re.sub(r"\s{2,}", "", row)
            # test if text is not empty
            if text != "":
                # write the new normalize text without begin spaces
                fo.write(row.strip() + "\n")
    fo.close()

if __name__ == "__main__":
    recreate_sentences(filepath="../v_0/text/up/all_2.txt")