Lettres de : princesse Héléna de Sclesvig-Holstein. duc et duchesse d'Albany. duchesse d'Argyle. duc et duchesse de Cambridge. roi George et reine Mary. roi Édouard et reine Alexandra. duc et duchesse de Conaught. duc et duchesse d'Édimbourg.

Lettres de : roi Guillaume. Henri, prince de Reuss et son épouse.

Lettres reçues de : pape Pie IX ; roi Oscar de Suède ; reine Joséphine de Suède.

Dans le premier cas, il s’agit surtout de dépêches, de nombreux télégrammes provenant des municipalités italiennes.

Lettres adressées à Napoléon III et à l’Impératrice par divers. 1832-1918 Un carton.

Les lettres ne sont pas classées. On trouve entre autres correspondants : Bazaine ; Canrobert ; Chateaubriand (trois lettres, 1832 et 1842) ; Clémenceau (une lettre à l’Impératrice pour la remercier d’un document donné par elle et remis aux Archives nationales, 1918, une lettre de l’Impératrice à Clemenceau, 30 novembre 1918, avec une réponse dactylographiée et signée, 13 décembre 1918) ; Victor Cousin ; Mgr Darboy ; Gustave Doré ; Octave Feuillet ; Flahaut ; Achille Fould (certaines lettres concernent le budget et la situation financière de la France, 1861-1865) ; Mérimée ; Émile Ollivier (avec des minutes de réponses de l’Empereur) ; Pasteur ; Renan ; Sainte-Beuve (deux lettres, 1865) ; Sandeau (trois lettres, s. d. [1873 et 1879]) ; Sismondi ; Veuillot ; Vieillard ; Vigny (une lettre, 1854) ; comte Walewski (dont une sur le traité de Paris, 1856).

Lettres adressées à l’Empereur et à l’Impératrice par des membres de leur famille. 1825-1910 Un carton.

Correspondance et discours de Napoléon III ; testament. 1853-1873 Un carton.

Lettres adressées à l’Empereur et pièces diverses relatives aux attentats commis contre sa personne, 1853-1867. Rapports ayant trait aux relations de l’Empereur avec sa famille, en particulier aux allocations attribuées, 1853-1861. Discours de l’Empereur, minutes et exemplaires imprimés, 1848-1871. Notes dictées par lui, 1856-1872 et s. d. Lettre et statistique relatives au plébiscite de 1870. Liste des membres de l’Assemblée nationale ayant protesté contre la déchéance (avec signatures), 1er mars 1871. Testaments de l’Empereur, 1873 (expéditions ; Me Mocquard, notaire), et copie du procès-verbal relatif à son inhumation rédigé par Maret, 1873.

Campagne de Crimée.

Correspondance. Un carton.

Lettres du général de Béville et du général Bosquet.

Lettres du général Canrobert et lettres en rapport avec le Conseil de guerre.

Contient notamment des lettres de : Niel et Pélissier.

Contient notamment des lettres de Saint-Arnaud et de Vaillant.

Documents diplomatiques. Un carton.

Campagne d'Italie.

Dépêches : du ministre de France à Turin au ministre des Affaires étrangères à Paris ; diverses ; de Napoléon III à l’impératrice Eugénie.

Lettres de : Jomini ; Goyon. Minute d’une note du commandant Schmitz sur Magenta.

Correspondance diplomatique. 1859.

Lettres reçues, en particulier de : marquis d’Azeglio ; Binda ; Cavour.

Lettres reçues, en particulier de : Drouyn de Lhuys ; Fleury ; Garibaldi ; duc de Gramont.

Lettres reçues, en particulier de : La Valette ; Metternich.

Lettres reçues, en particulier de : comte Pepoli ; Thouvenel.

Régence. 1859-1870.

« Lettres patentes. 1859, 1865, 1870 ». Lettres patentes conférant à l’Impératrice le titre de régente (3 mai 1859, 26 avril 1865, 23 juillet 1870) et ordres généraux du service pendant l’absence de l’Empereur (7 mai 1859, 28 avril 1865, 23 juillet 1870). Registre demi chagrin vert. Procès-verbaux du conseil des ministres durant la régence. 12 mai-16 juillet 1859. 2 volumes plein chagrin vert. [Il s'agit de doubles avec seulement quelques variantes. Cependant, l'un est contresigné par Eugénie et Achille Fould, l'autre Fould uniquement]. 3 mai-9 juin 1865. 2 volumes plein chagrin vert. [Ce sont des doubles, tous deux contresignés par l'Impératrice et par Rouher].

Documents concernant l'Italie et l'Algérie. 1853-1870.

Adresse suivie de trois pages de signatures.

Correspondance, notes, protocoles.

Campagne du Mexique. 3 cartons.

Lettres et rapports reçus de quelques hommes politiques ou diplomates, mais surtout d’officiers.

Dossir 2 Bazaine (général).

Notamment : Castelnau et Drouyn de Lhuys.

Contient notamment les lettres de Frossard, Gallifet et Jurien.

Documents diplomatiques (notes sur le Mexique, analyse du courrier du Mexique). 1863-1866.

Minutes originales de la correspondance adressée par le baron de Malaret, ministre plénipotentiaire de France en Italie à Turin, puis à Florence, au ministre des Affaires étrangères. 1863-12 septembre 1870.

Campagne de 1870. 1870-1872.

Correspondance et notes sur les préliminaires de la guerre, en particulier sur la situation de l’armée. Un carton.

Lettres de : Bismarck ; lord Clarendon ; Drouyn de Lhuys ; général Fleury ; duc de Gramont ; La Valette ; Mels ; Metternich. Notes dictées par Napoléon III. Rapport à l’Empereur sur l’entrevue de Gastein, 18 mars 1872.

Dépêches. Juillet-septembre 1870 Un carton.

Lettres et souvenirs relatifs à la guerre de 1870. 1870-1872 Un carton.

[Article déclassé ; l'odre des documents ne correspond plus à celui indiqué dans l'inventaire]. Notes de Rossel sur le siège de Metz, lettres de Duvernois, de Leboeuf, du général Boyer...

Documents concernant notamment des pension accordées, les joyaux de la couronne et la naissance du prince impérial. 1853-1874.

Documents concernant notamment la mort de Napoléon III. 1853-1909.

Copies de documents des années 1812 et 1813 sur le cérémonial et l’étiquette réunies par le ministère de la Maison de l’Empereur, projet de cérémonial pour les réceptions du 1er janvier (s. d.), et règlements des fonctions et attributions des grands officiers de la Maison de l’Empereur (1853 et s. d.) rédigés par le ministère de la Maison de l’Empereur.

Bulletins indiquant les noms des tireurs et le nombre de pièces abattues, par tireur, à Rambouillet, Fontainebleau et Compiègne.

Dessin représentant Napoléon III le jour de sa mort, 9 janvier 1873. Emboîtage de toile verte. « Procès-verbal. Mise en bière de l’empereur Napoléon III. 14 janvier 1873 ». Document sur parchemin, signé, avec des sceaux noirs, en deux exemplaires, placés dans un coffret simulant un volume, demi-reliure à coins, maroquin vert. « Documents relatifs à la mort et aux funérailles de l’empereur Napoléon III ». Entre autres son testament et le procès-verbal descriptif de l’état du corps. Janvier 1873-août 1887. Registre, demi-reliure à coins, chagrin vert. « Mort de l’empereur Napoléon III. Journaux illustrés ». Journaux et revues relatifs à la mort de l’Empereur. 1873 et 1909. 10 pièces dans un portefeuille recouvert de toile verte.

Deux gravures de Traversier représentant les grandes armes du Second Empire, et diverses photographies des armes de Napoléon III et d’Eugénie.

Photographies diverses. XIXe siècle.

Album renfermant quatorze photographies de l’intérieur et de l’extérieur du palais.

