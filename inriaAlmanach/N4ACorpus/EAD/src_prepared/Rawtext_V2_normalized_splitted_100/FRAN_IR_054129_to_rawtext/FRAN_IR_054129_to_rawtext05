1 carte militaire d'un soldat donnant de nouvelles d'André Bellanger, blessé, à sa mère.

PA_109 Documents isolés 1914-1918.

100 plaques de verre stéréoscopiques  Aisne- Cadavres allemands au plateau de Craonne ; Crouy : cuisine roulante ; Laffaux : poste de tir ; canon de 400 sur voie ferrée à Mailly ; brancardiers route de Margival ; canon de 400 à Margival ; Massiges : tranchée, église en ruines ; charrettes à cheval route de Soissons ; carrières de Soissons ; boyau du bois des Buttes ; lancement de grenades en 1ère ligne ; prisonniers allemands Marne- Beauséjour : ravin, cimetière ; Chalons : maison détruite ; guetteur dans la tranchée de Maisons ; fort de La Pompelle : 1ère ligne de front ; Le Mesnil en ruines ; marais de Saint-Gond : corps de soldats allemands ; Mont Haut : cadavres dans une tranchée ; lancement de grenades devant Perthes ; abri de mitrailleuses ; 1ère ligne de front ; arrivée de renforts Meurthe-et-Moselle- Forêt de Bois-le-Prêtre sous la neige ; forêt de Parroy : chevaux morts après un bombardement Meuse- Fort de Douaumont ; Fleury : cadavres dans une tranchée ; Vaux : observatoire dans un arbre, étang ; Verdun : la ville en ruine, ravin de l'Hermitage, ravin des Vignes, côte du Poivre, avion allemand abattu, pièce de 140 ; bois d'Ailly (comm Han-sur-Meuse) : prisonniers allemands ; bois d'Avocourt (comm Varennes-en-Argonne) : batterie, tranchées ; cote 304 : ravin de La Caillette (comm Fleury-devant-Douaumont) Oise- Bois de Courcelles après l'attaque ; soldats en marche sur le plateau de Quennevières Pas-de-Calais- Notre-Dame de Lorette (comm Ablain-Saint-Nazaire) : soldats en 1ère ligne ; route d'Artois enneigée Somme- Becquincourt : église en ruines ; Bouchavesnes : ruines ; Bray-sur-Somme : camp de prisonniers, convoi de ravitaillement ; Combles : paysage désolé, ravin ; Feuillères : en ruines ; Lihons : tué devant sa cagna ; Maricourt : prisonniers, cimetière ; arbre camouflé ; Maurepas en ruines ; convoi d'autobus de transport de troupes ; écluses ; chargement d'un obus de 400 Soins aux blessés russes ; soldats avec mitrailleuse ; distribution de biscuits aux prisonniers ; prisonniers de guerre se rendant au travail en Champagne ; tombe du sergent Sparck ; lancement d'un pigeon voyageur ; remise de décorations par le général Pétain ; revue par le général Gouraud ; distribution de masques à gaz ; l'aviateur Jean Chaput devant un avion abattu (sa 11e victime) ; soldats à la gare d'embarquement vers le front (sans lieu) ; intérieur d'une cagna ; la soupe devant la cagna ; la chasse aux totos ; distribution de vivres.

PA_110/1 Documents concernant Jean Mauresmo (1889-1981) sans date.

1 photographie en uniforme de marin (Brest) ; fascicule de mobilisation.

PA_110/2 Documents concernant Henri Vernillet (1895-1969), 172e RI 1918.

1 photographie en uniforme (sans date) ; citation (1918).

PA_111 Documents concernant les frères Auguste et Prudent Lelaidier, 26e RT.

2 photographies : Auguste en uniforme, Prudent blessé à l'hôpital de Luçon (Vendée).

PA_112 Documents concernant les frères Gravier : Gustave Gravier (1881-1971), polytechnicien, capitaine du Génie, Jean Gravier († 14 juin 1916), caporal au 326e RI et Marcel Gravier, 6e Chasseurs 1914-1918.

Correspondance : 5 lettres envoyées par Gustave à ses parents (1914-1918), dont 1 lettre donnant la liste des tombes au cimetière de Froméréville près de Verdun (Meuse) (1916) ; 4 lettres envoyées par Jean à ses frères Gustave et Marcel (1916)  5 photographies : soldats en uniforme (Gustave, Jean), groupe de mariés (Gustave Gravier), soldats morts sur le front Menu (10 mai 1916), programme de théâtre aux armées (1917).

PA_113 Documents concernant Joseph Doniot († 16 avril 1917), de Cancale (Ille-et-Villaine), 5e RIC 1915-1916.

7 cartes militaires envoyées à son épouse.

PA_114 Documents concernant Alfred Loustalot, 103e RI 1912-1918.

Ordre d'appel sous les drapeaux 1912 53 photographies et cartes photographiques 1915-1918 Loustalot en uniforme (1915) ; groupe de soldats Meurthe-et-Moselle- Bois-le-Prêtre : soldats morts (1915), cimetière ; Fey-en-Haye : église et maisons en ruine ; Griscourt : groupe de soldats, pont sur la Seille, le tambour-major et la cartographe, village sous la neige (1916), maréchal-ferrand (1916) ; Limay : église en ruine ; Lironville sous la neige en 1915 ; fort de Manonviller ; Mont Sainte-Geneviève ; Reillon ; Villers-en-Haye sous la neige ; transport de soldats en train Meuse- Verdun (1916) ; Verdun, camp de La Falouse en 1916 : cuisines roulantes, boites à lettres et petits métiers (armuriers, cordonniers, tailleurs, charrons) ; convoi d'un officier tué (sans lieu) ; « saucisse » (dirigeable) à Verdun, en 1916 ; fusées éclairantes à Verdun en 1916 (photo prise de nuit) Cadavres, au dos note manuscrite : « A faire voir à ceux qui veulent la guerre » (sans date) ; lancement de grenades ; devant un avion (1917) ; trous d'obus (sans lieux) ; restes d'un avion allemand abattu ; Saint-Brélade (île de Jersey) : église et cimetière ; remise de décorations ; camarade blessé à la tête ; groupe de soldats devant un sapin de Noël (1916) Carnet de notes manuscrit Août-décembre 1914.

PA_116 Documents concernant Louis Vassalo († 13 septembre 1914), 332e RI 1914.

Carnet de notes manuscrit Août 1914.

PA_117 Documents concernant Max Forrer, Légion étrangère 1915-1919.

Gazette de Zürich, « Les lettres du soldat » (sur microfilm)  3 photographies : Forrer à la Légion étrangère 1915 et 1919 Correspondance : 3 cartes postales : portraits en médaillon des généraux Wille, Dufour († 1875) et Herzog († 1894) ; transport des blessés français à travers la Suisse ; 1 lettre (1919).

PA_118 Documents concernant Jules Drouet 1914-1916.

Carnet de notes manuscrit 25 octobre au 19 novembre 1914 50 cartes postales patriotiques et humoristiques, sans rapport avec Jules Drouet : le poilu et sa fiancée ; le poilu dans son abri : « ça sent le renfermé ici » ; le permissionnaire ; « la vie en cartes » ; la marraine de guerre ; le travail des femmes de France ; « vers le triomphe » ; « honneur au drapeau » ; les usines Schneider d'Albert (Somme) bombardées par les Allemands ; la Marseillaise en Alsace ; la baignade des poilus dans la Meuse ; champ de bataille de la Marne  1914-1916.

PA_119 Documents concernant Adrienne Bergès, infirmière à Versailles 1914-1915.

Carnet de notes manuscrit « Décès salle 4 » Août-octobre 1914 Correspondance : 3 lettres à ses parents (août-septembre 1914), 1 lettre reçue d'un soldat en convalescence, Élie Renou (1915) 1914-1915.

PA_120 Documents concernant Jean-Marie Chapalain, 48e RI 1914.

Correspondance : lettre de Jean-Marie Chapalain à ses parents ; lettre du maire de l’ile de Batz, père de Jean Marie Chapalain, demandant des nouvelles de son fils ; 3 lettres du secrétariat du roi d’Espagne informant que Jean Marie-Chapalain ne figure pas sur la liste de prisonniers de l’ambassade d’Espagne à Berlin Août-décembre 1914.

PA_121 Documents concernant Marc Simon, 1er régiment du Génie, fabricant de meubles au faubourg Saint-Antoine à Paris, prisonnier de guerre en Allemagne, au camp de Tingleff, dans le Schleswig-Hosltein (auj Tinglev, Danemark) 1915-1918.

3 photographies ; correspondance : 92 cartes postales, 176 lettres.

PA_122 Documents concernant Jacques Michaut 1916-1918.

Lettres et poèmes :  1916- 9 lettres et 2 poèmes : « Avant l'attaque » (Douaumont, 22 mai 1916), « Lorsque la paix sera signée » (mai-juin 1916) 1917- 1 lettre, 2 poèmes : « Au printemps » (Aisne, mars 1917), « Toussaint » (1er novembre 1917) 1918- 1 lettre.

PA_123 Documents concernant Albert Laborde, 160e RI 1914-1921.

Carnets manuscrits écrit en 1921 : « Souvenirs de guerre 1914-1919 » par Albert Laborde 1er volume : 1914-1916 ;  2ème volume : 1914-1919  Carte d'état-major (Artois, 1915).

PA_124 Document concernant Madeleine Carrelet (1889-1978), infirmière à Épinal 1915-1916.

Cahier manuscrit de 55 pages, comprenant les témoignages manuscrits de plusieurs poilus hospitalisés, dont un Alsacien : Attard (Louis), 9e Train d’équipages, d’Alger (Algérie) ; Baron (Eugène), 33e RI ; Blanc de La Cour, Chasseurs Alpins, de Moutiers (Savoie) ; Bouton (Henri), Chasseurs Alpins, originaire de Buenos-Aires (Argentine) ; Bragouse (Étienne), Chasseurs Alpins, de Vébron (Lozère) ; Brasset (Albert), Chasseurs Alpins, de Marlens par Faverges (Haute-Savoie) ; Dacheville (François), Chasseurs Alpins ; Ecrille, 6e groupe Artillerie à pied, de Belfort ; Fantauzzi (L), RAT, de Marseille (Bouches-du-Rhône) ; Ferrari (A), Chasseurs Alpins ; Gaulinfrat ; Granat (Charles), Chasseurs Alpins Territorial ; Griffe (J), 343e RI ; Hans, Chasseurs Alpins, de Soulans (Hautes-Pyrénées) ; Lamy (Marius), batterie de bombardiers de 58, de Paris ; Levesseire (A), 213e RI ; Maiffret (Marius), Chasseurs Alpins, de Beaulieu-sur-mer (Alpes-Maritimes) ; Manaurenti, Chasseurs Alpins, de Marseille (Bouches-du-Rhône) ; Marcel (Léon), Chasseurs Alpins, de L’Albéric (Isère) ; sergent Mathieu (Jean), 215e RI, de Pantin (Seine-Saint-Denis) ; Mazel (Jonanin), de Le Cailar (Gard) ; Pélissier (Léon), 343e RI, des Camazes (Tarn) ; Reignier (E), 152e RI ; Richard (Marcel), 2e Dragons ; Riotton (Edmond), Chasseurs Alpins, d’Annecy (Haute-Savoie) ; Salle, 23e RI ; Terrat (Paul), Chasseurs Alpins ; Turrel (Émile), 253e RI, de Narbonne (Aude) ; Veillard ; Vial (Noël), Chasseurs Alpins 14 juin 1915-8 février 1916.

PA_125 Documents concernant Jules Dezon († 21 octobre 1917), 201e RI 1917-1919.

2 lettres (août et octobre 1917) ; lettres de l'état civil français de Belgique (1919) ; note sur la sépulture du soldat Dezon.

PA_126 Documents concernant le sous-lieutenant Édouard René Péguy, ingénieur des Mines 1918.

32 photographies : groupes de soldats ; convois de camions ; tracteur avec charrue ; automitrailleuse ; déchargement d'obus d'un camion à Rarécourt (Meuse) ; prisonnier de guerre avec brouette et balai ; Clermont-en-Argonne (Meuse) en ruines ; Wormhout (Nord) ; maisons en ruines ; canon de marine de 274 sur un train ; René Péguy, en uniforme, avec un appareil photo ; cavalerie anglaise (armée des Indes) Sans date Citation ; brevet de légion d'honneur 1918.

PA_127 Documents concernant Jules Jacquard, 404e RI (?) sans date.

31 photographies : Jules Jacquard en uniforme ; soldats dans un abri ; civils sur les ruines d'une maison ; Tilloloy (Somme) : le château en ruines ; séance de tir de mitrailleuses ; les officiers mitrailleurs du 404 ; soldats à leur poste de tir dans les tranchées ; défilé de mulets dans la neige ; la chasse aux poux ; lance-torpilles ; chemin de claies pour monter aux lignes sans marcher dans la boue ; une mascotte : le chien Roudibet ; groupe de sapeurs ; groupe de mitrailleurs ; chambre de repos dans un abri en tôle.

PA_128 Documents concernant Hubert Victor Adrien Guillet († 1963), 151e RI 1914-1915.

Correspondance : cahier de copies de lettres 20 décembre 1914-4 mai 1915.

PA_129 Documents concernant René Favrelle, infirmier à l'hôpital complémentaire de Paris-Plage, puis 1er BCP, prisonnier de guerre à Bad-Blenhorst (Allemagne, Basse-Saxe) en 1918 1914-1919.

Correspondance envoyée et reçue : 42 lettres (1914-1919) ; plan d'un camp de prisonniers (Oflag XI A) ; 1er bataillon de chasseurs : programme de la séance récréative du 14 juillet.

PA_130 Documents concernant Louis Le Cunff , cuisinier, 241e RI 1914-1929.

1 photographie en uniforme ; carnet de notes manuscrit (1914-1919) ; certificat de médaille de Verdun [1929 ?].

PA_132 Documents concernant Alexandre Louis Baraudon sans date.

1 photographie de groupe (anciens combattants ?).

PA_133 Document concernant le sergent Léon Delemasure, originaire de Tourcoing (Nord), 1er R de cavalerie sans date.

1 photographie en uniforme.

PA_134 Documents concernant Auguste Pinon (1893-1990), Chasseur alpin, 231e puis 246e RI.

