15 photographies : en uniforme ; dans un groupe de blessés et d'infirmières à l'hôpital de Montauban (1915) ; groupe de mitrailleurs (1916) ; soldats du 231e RI (1916) ; groupe de soldats dans une ferme à Villiers-les-Pots (Côte d'Or), avant le départ en Orient (1917) 1915-1917 Livret militaire ; certificats médicaux et de visite (1915) ; citation (1916) ; billet de permission à l'en-tête de l'armée d'Orient (1918) 1915-1918 État signalétique et des services (1926) ; attestation du bureau de recrutement de Blois (1927) 1926-1927.

PA_135 Documents concernant Jean Nicole (1887-1968) 1915-1916.

4 photographies et cartes postales photographiques : soldats construisant des baraquements (1915) ; groupe de mitrailleurs ; « popote dans un petit bois » (1916).

PA_136/1 Documents concernant le maréchal des logis, François Buffard, 11e puis 83e RA 1915-1918.

Carnet d'instruction et de manipulation de munitions et artifices (sans date) ; 1 photographie en uniforme (1915) ; carnet de liste de vivres (1918).

PA_136/2 Documents concernant Laurent Alix, prisonnier de guerre à Mannheim (Allemagne, Bade-Wurtemberg), 97e RI 1915-1919.

1 album photographique : photographies de famille, de soldats, avec cartes postales.

PA_137 Documents concernant Jean-Pierre Simonnot († 27 octobre 1914), 227e RI 1914-1921.

1 photographie en uniforme (sans date) ; acte de justice de paix attestant la transcription de l'acte de décès sur les registres de la commune de Fontaine-la-Gaillarde (Yonne, 1919) ; certificat de remise de la médaille militaire (1921).

PA_138 Documents concernant Frédéric Silvestre, agriculteur dans la Drôme, mobilisé à 41 ans, 17e RIT 1915-1916.

2 lettres de son père (1915-1916) ; certificat du maire de Fourcinet (Drôme) concernant la situation de famille de Frédéric Silvestre (1916).

PA_139/1 Document concernant Laurent Lumalé, originaire de Bayonne (Pyrénées-Orientales), 34e RI sans date.

Livret militaire.

PA_139/2 Documents concernant Paul Couzy († 3 septembre 1915), originaire de Toulouse (Haute-Garonne), 9e Chasseurs 1910-1923.

Certificat de bonne conduite (1910), livret militaire Carte confédérale du syndicat national des PTT, extrait de naissance, extrait de casier judiciaire (1912) 9 photographies : en militaire (sans date), avec sa femme, Madeleine (sans date), avec un groupe de blessés et d'infirmières à l'hôpital auxiliaire de La Garenne-Colombes (Hauts-de-Seine) (1915) 55 lettres et cartes de Paul Couzy à sa femme Madeleine Décembre 1914-septembre 1915 23 autres lettres reçues par sa femme, notamment l'avis de décès de son mari et une lettre de l'instituteur d'Agny (Pas-de-Calais) lui adressant un croquis de la tombe de son mari (10 décembre 1915) 1914-1923 12 cartes postales : caserne du 9e chasseurs à Auch et à Villecomtal-sur-Arros (Gers), tombe de Paul Couzy à Carency (Pas-de-Calais), cimetière militaire de La Targette  (Pas-de-Calais, comm Neuville-Saint-Vaast) ; cartes patriotiques (« Tout pour la patrie », « Prière de Française », humoristiques (« Les épingles de Marianne », « Flegme »)  Journaux et imprimés : supplément du Petit Journal : calendrier 1915, « Le sang de la France », roman patriotique de Paul Bertnay ; La dernière heure, 13 octobre 1915.

PA_139/3 Documents concernant Léonarde Laurent 1916-1923.

Extrait de naissance, sauf-conduit (1916), extrait de casier judiciaire (1923).

PA_140 Documents concernant le capitaine Joseph Patureau Mirand, député maire de Châteauroux (Indre) 1914-1918.

Album photographique  Amiens (Somme) : évacuation de populations civiles (mai 1918), rues et maisons en ruines (mai 1918) ; Amblemy (Aisne) : le château ; Louvencourt (Somme) : le château Poste de commandement au chemin des Dames ; officiers de liaison (à cheval, à vélo) ; groupes d'officiers, de soldats, dans les tranchées, cagna, à cheval ; camps de prisonniers allemands ; Malgaches au front ; infirmiers américains au front.

PA_141 Documents concernant Tobias Hechter (1872-1916), d'origine russe 1914-1916.

1 photographie en uniforme (sans date) ; lLivret d'oraison funèbre par le rabbin Raphaël Lévy, imp (2 octobre 1916).

PA_142 Documents concernant Jean Goutines, 103e RI (?) 1912-1918.

Carte d'étudiant de la faculté de droit de Paris 1912-1913 1 photographie en uniforme Sans date 2 cahiers de souvenirs de guerre : tome 1 (octobre 1915-août 1917), tome 2 (juillet 1917-novembre 1918) 1915-1918.

PA_143 Documents concernant César Alayse, 266e RI 1918-1940.

Livret militaire ; fascicule de mobilisation ; 1 photographie en uniforme (sans date) ; citation (27 octobre 1918) Carte du combattant (1935) ; attribution de la médaille militaire (octobre 1938) ; convocation comme réserviste (1940).

PA_144 Documents concernant Désiré Tilou 1915-1918.

29 photographies : groupes de soldats ; intérieur d'un abri ; ravitaillement par des ânes ; photographie aérienne ; ferme détruite ; intérieur d'une église ; poste d'observation en forêt ; travail sur une douille d'obus ; repas à l'extérieur ; poste radio ; abris Sans date Carnet de notes manuscrit 1915-1918.

PA_145 Documents concernant Pierre Fort, 13e RI 1914-1919.

Dessin à la plume d'un poilu, signé : A D de Segonzac Sans date 9 photographies : en uniforme ; photographie de promotion (1913) ; en famille (1914) ; au front (1916) ; entrée des Français à Colmar (Haut-Rhin) (1918) 1913-1918 Livret militaire ; citation (1917) Correspondance : 14 lettres reçues de sa fiancée Denise (1917-1918) ; 1 de sa belle-mère (sans date) « Mère patrie », poème de François Fort Juin 1915 Carnet de poèmes : « Le départ des conscrits » ; « À Denise pour sa fête » ; « Sur la tombe d'un ami » ; « Le retour du poilu » ; « Amour sacré » 1918-1919.

700AP/6 Contributions PA_146 à PA_181.

PA_146 Documents concernant le sous-lieutenant Armand Samson (1882-1970), 12e Cuirassier, originaire de Grosrouvre (Yvelines) 1914-1943.

Carnet de notes manuscrit.

PA_148 Documents isolés 1914.

Six formulaires d'enquête sur les conditions de vie ouvrière et de la vie rurale (1914, photocopies récentes), carte postale, photographie d'un groupe de militaires.

PA_149 Documents concernant le sous-lieutenant Jean Chaput, 36e RIC 1914-1916.

Planche de 8 photographies : vues des tranchées et des abris, blessé Sans date 4 lettres, dont une sur Noël 1914 et une décrivant la tombe de Jean Chaput 30 décembre 1914-18 avril 1916 Citation 1915 Faire-part de décès militaire humoristique, imp.

PA_152 Documents concernant Jacques Gaston Boulez (1883-1930), 31e RI 1915-1929.

Livret militaire ; citation (1918) 3 photographies : en uniforme, le bras en écharpe ; dans un groupe de soldats Sans date 18 cartes postales : dessins d'un poilu faisant sa toilette au-dessus d'un seau (1916), du pansage d'un cheval dans une forêt en Argonne (1915) ; cartes militaires, certaine illustrées de scènes de vie de poilus dans les bois de l'Argonne en 1915 : « Ma guitoune à Vauquois, 1915 », locomotive à vapeur dans une gare (1915) 1915-1916 Billet de permissions ou congés (1917-1918) ; billets de réforme (1918-1926) ; carte du combattant (1929).

PA_153 Documents concernant l'adjudant-chef Toussaint Franchi, 3e régiment de chasseurs d’Afrique 2013.

Renseignements civils et militaires, transcription du journal de guerre, photographies 125 pages.

PA_154 Documents isolés 1914-1918.

84 plaques de verre stéréoscopiques  Aisne- Tanks Renault à Audignicourt ; ravitaillement par camion ou par voitures à cheval à la ferme de Cussy ; Sénagalais au bivouac de la ferme de Cussy ; Anglais à Fresnoy-le-Petit ; prise du moulin de Laffaux ; ferme de Mongarny ; la soupe près Craonne Belgique- Nieuport (Flandre occidentale) : « maison du peintre » sous la neige Marne- Fort de la Pompelle ; incendie de Reims (avril 1918) ; prisonniers allemands à Perthes ; hommes revenant du feu près de Perthes ; gare de Suippes en activité Meuse- Fort de Tavannes ; ravin de Vaux ; vue de la Meuse du haut du fort des Romains ; ravin de la Gaillette ; ravin de la mort vers l'étang de Vaux ; obusiers américains à Lérouville ; le Génie américain réparant la chaussée de Lérouville à Saint-Mihiel (soldats noirs) Pas-de-Calais- Carency en ruine ; Mont-Saint-Éloi : église en ruine ; prise d'une tranchée boche à Notre-Dame de Lorette Haut-Rhin- Gare de Thann (août 1916) Haute-Saône- Poste de secours de Froideterre : civières posées sur la boue Somme- Boyau desservant une batterie dans la Somme Prisonniers allemands portant leurs blessés ; poste de secours dans les tranchées ; chien secouriste ; fosse commune ; cuisine en plein air des Américains ; abris dans la forêt, sous la neige ; départ pour les tranchées sous la neige ; bénédiction d'un canon ; officiers à cheval ; sortie d'un cercueil d'une église ; soldats dans la neige : « En montant à l'Hartmanwillerskopf » (Vosges) ; inhumation au front Tanks ; saucisse (dirigeable) ; l'aviateur Guynemer ; un Farman prenant son vol ; hangar Bréguet-Michelin détruit par une bombe.

PA_155 Documents concernant le capitaine Henri Bony (1872-1918), professeur d'histoire, 31e RIT 1914-1918.

2 photographies : un officier avec son épée, Henri Bony en uniforme dans les bois Sans date Correspondance : 39 lettres, cartes militaires et cartes postale - Beaumont-sur-Oise (Val d'Oise) : le pont de péniches ; Bouffémont (Val d'Oise) ; Le Bourget (Seine-Saint-Denis) : l'église des « dernières cartouches » (guerre de 1870) ; Épinal (Vosges) ; Lorient (Morbihan) : la colonne de la place Bisson ; Nesles-la-Vallée (Val d'Oise) : l'église ; Paris : le Val-de-Grâce ; Pierrelaye (Val d'Oise) ; Pontoise (Val d'Oise) : la cathédrale Saint-Maclou (Val d'Oise) ; Verdun (Meuse) ; le col du Glandon (Savoie).

PA_156 Documents concernant Jean-Adrien Raynaud, peintre et musicien, père de trois enfants 1918.

2 photographies : « Mes camarades et élèves » (groupe de soldats jouant de la musique) ; « Souvenir de mon cruel séjour au poste des Bagnelles secteur du Bonhomme dans les Vosges » (sans date) 1 lettre du 20 avril avril 1918 à sa femme Marie-Louise, à l'en-tête du foyer du soldat union franco-américaine.

PA_157 Documents concernant Marcel Dianoux, originaire d’Alger, 1er Zouaves 1914-1920.

Livret militaire, carnet de pécule Carte du combattant (1935), certificat d'invalidité (sans date), attribution de la médaille de guerre (1920).

PA_158 Documents concernant Jules Panzani (1893-1925), originaire de Sainte-Lucie de Tallano (Corse), 117e RI 1914-1918.

10 photographies : famille, canon de batterie, photographies de groupes de soldats (avec un canon, blessés, en uniforme) Sans date Correspondance envoyée à ses parents : 290 cartes et lettres 1914-1918 2 cartes postales : canon de 155 Affiche spectacle des armées 13 juillet 1915 5 documents concernant son hospitalisation à Nuits-Saint-Georges (Côte d'Or) 1917.

