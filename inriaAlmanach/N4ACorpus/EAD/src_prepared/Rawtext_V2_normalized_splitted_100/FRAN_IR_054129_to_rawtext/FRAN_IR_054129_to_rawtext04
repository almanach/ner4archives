PA_088/1 Documents concernant Joseph Brugeat, 28e BCA 1914-1915.

1 photographie : en groupe avec des soldats et des infirmières Lyon, 28 novembre 1914 Attribution de la médaille militaire 1915.

PA_088/2 Documents concernant le sous-lieutenant Jean-Baptiste Brugeat, prêtre († 24 janvier 1916), 300e RI 1916-1919.

1 photographie (sans date) ; citation (1916) ; 3 lettres sur les circonstances de sa mort (4 février et 22 mars 1916, 26 mars 1919) ; 2 plans de l'emplacement de sa sépulture au cimetière du Mont-Saint-Éloi (1917 et sans date).

PA_088/3 Documents concernant Paul Brugeat, séminariste († 2 novembre 1918), 260e RI 1918-1922.

3 photographies (sans date) ; attestation de décès à Monastir (Serbie) et renseignements sur sa mort (janvier-mars 1919)  Le trait d'union, périodique ronéotypé du séminaire de Tulle, n°20, janvier 1922, contenant son éloge funèbre.

PA_089/1 Document concernant Auguste Savy, 3e RA, forgeron, originaire de Naucelle (Aveyron) 1914.

1 carte postale adressée à sa fille représentant Carcassonne 12 décembre 1914.

PA_089/2 Documents concernant Olivier Brot, de Villeurbanne 1918-1921.

Cartes et tickets de rationnement pain, sucre et charbon.

PA_090 « Souscription faite en l'honneur des artilleurs qui ont abattu la grosse Bertha » 1918.

1 page manuscrite.

PA_091 Document concernant Eugène Roussel 1917.

Photographie carte postale représentant un groupe de soldats avec infirmières Mai 1917.

PA_092 Documents concernant Pasquale Chirico (1894-1980), soldat italien, de la région de Calabre 1918-1919.

3 photographies (reproductions modernes) représentant le front de la bataille de la Piave (juin 1918) ; feuille de congé illimité (en italien, 1919).

PA_093 Documents concernant Marcel et Renée Martinet, militants socialistes 1914-1919.

5 lettres et brouillons de lettres contre la guerre 1916-1918 Tracts pour des conférences de Marcelle Capy sur Romain Rolland (21 novembre et 5 décembre 1915) ;  tract de « L’homme enchaîneur, annales du pacifisme et de la délation » donnant une liste de personnes arrêtées pour délits d’opinion (sans date) ; carte postale des éditions de « L’Avenir social » en soutien à Hélène Brion, arrêtée en novembre 1917 pour propagande pacifiste ; La voix de l’humanité, du 2-9 août 1916 ; résolution imprimée du Conseil National de la Section Socialiste (28 juillet 1918) ; lettre circulaire signée de Magdeleine Marx pour un projet « Clarté » (8 juin 1919) 1915-1919.

PA_094 Document concernant le caporal Alfred Cheron, 129e RI 1922.

Brevet de médaille militaire.

PA_095 Documents concernant Édouard Besse, chauffeur des officiers 1915-1916.

20 photographies : Édouard Besse, avec sa voiture Lorraine-Dietrich, à la caserne de Jardin-Fontaine (Meuse, comm Verdun, août 1915-juin 1916) ; prisonniers (25 septembre 1915) ; groupe de soldats avec un canon ; blessé sur un brancard (octobre 1915) ; église d'Arras (Pas-de-Calais, 1915) ; caserne de Jardin-Fontaine (Meuse, comm Verdun) ; château de Saint-Lumier-la-Populeuse (Marne, juillet 1916) ; Verdun (Meuse, juin 1916) ; tombe de Louis de Lastour († avril 1916) à la caserne de Jardin-Fontaine à Verdun ; cimetière de Glorieux (Meuse, comm Verdun, juin 1916) ; soldat dans une tranchée.

700AP/5 Contributions PA_096 à PA_145.

PA_096 Documents concernant Louis Émile Aubert (né en 1904), fils du général Aubert 1918.

Cahier manuscrit avec une chronique des bombardements à Paris.

PA_097 Documents concernant le lieutenant Hubert de Monbrison, 1er Cuirassés 1916-1918.

Carnet de dessins, 40 folios  « Le patron » : le colonel Gillois [ commandant du 1er Cuirassiers de février 1915 à mars 1917], avec bottes et cravache, vu de dos Aquarelle couleurs  Le général Ferraud [ commandant du 1er corps de cavalerie] et le général Robillot [ commandant du 2e corps de cavalerie] Aquarelle couleurs  Le général de Rascas de Châteauredon [ commandant de la 2e brigade de cuirassiers à partir du 22 janvier 1916], à cheval de dos et un officier de dos, avec bottes et canne, fumant la pipe avec la légende : « son chef d'E M » Aquarelle couleurs  « Murat », « Delage » fumant le cigare ; entr'eux, une table avec un encrier et une pile de papiers, avec le titre : « Notes Tome 300 » Aquarelle couleurs  À gauche, « L'intendance » : officier tirant un jouet (une charrette tirée par un cheval) ; à droite, le colonel Gillois Aquarelle couleurs  Le capitaine Gallon sur son cheval « Guerrier » Aquarelle couleurs  À gauche, « Roland » tenant un lièvre et une casserole de légumes ; à droite, d'Arexy, tenant un drapeau rouge et blanc (?) Aquarelle couleurs  « Un anglomane », « Un assyrien » : deux officiers en bottes, avec cravache Aquarelle couleurs  « Décision ! » : le colonel de Viry [ commandant du 1er Cuirassiers en 1917] et le capitaine Courtois (?) fumant la pipe Aquarelle couleurs  Officier fumant la pipe, en buste (capitaine Courtois ?), 1917 Aquarelle couleurs  « Gougou » : commandant Gatelet [ commandant du 9e Cuirassiers, † 6 mai 1917], tenant une canne à pêche Aquarelle couleurs  « La Roumanie Messieurs» : colonel Sautereau fumant la pipe Aquarelle couleurs  « Tout en jambe », croquis d'un officier Dessin noir et blanc « Carency [ Pas-de-Calais], fév 1915 » : 4 poilus de la 70e DI se protégeant du froid avec des couvertures, panneau « Tranchée Augier » Aquarelle couleurs  « Carency [ Pas-de-Calais] 1914-15-16 » ; à gauche, « la tournée du propriétaire Col Bonnet, Lt de La Tour du Pin », de dos, devant le Boyau Bonnet ; à droite, « la sape X à Carency 1915 » : poilu se chauffant les mains sur un brasero Aquarelle couleurs  « Sape X à Carency », soldats dans une tranchée Aquarelle couleurs  Soldat allemand avec casque Aquarelle couleurs  « Noyon [ Oise], novembre 1917 » : soldat allemand avec lunettes rondes Aquarelle couleurs  Église de Marquivillers (Somme), mai 1916 Aquarelle couleurs  « Tracy-le-Val [ Oise], janv 17 » : croquis d'un poilu dans une tranchée Dessin noir et blanc  « Tracy-le-Val, janv 17 » : 2 croquis de poilus Dessin noir et blanc Capitaine Récamier, 70e DI Lorraine Artois, 1914-15 Aquarelle couleurs  « 1915 » : lieutenant de Waldmer, de face, pensant à son enfant (berceau dans les nuages) Aquarelle couleurs  « 18141914 » : lieutenant Grenié de dos, montrant Napoléon dans les nuages Aquarelle couleurs  « Esculape » : officier de face, avec divers objets dans les marges (mors, étriers, seringue, tête de femme) Aquarelle couleurs  « Échassier » : officier de face, s'appuyant sur son épée Aquarelle couleurs  Lieutenant Raverdy Aquarelle couleurs  Lieutenant M Aquarelle couleurs.

PA_099 Documents concernant Daniel Godu (1896-1970), 37e RI 1914-1918.

Carnet de notes manuscrit (12 août 1914-1918) En dernière page, bon mot sur « L'élixir de Longwy ».

PA_100 Documents concernant Marius Petit, 341e RI 1916.

Citation Juillet 1916.

PA_101 Documents concernant l'adjudant Eugène Hosmalin, et sa fille Marguerite, originaires de Joudreville (Meurthe-et-Moselle), 99e RIT 1914-1917.

11 photographies : Marguerite, sa fille et sa mère Léonie (juillet 1914) ; groupe de soldats (1914) ; Marguerite en 1915 ; Marguerite en costume traditionnel (octobre 1916) ; Marguerite en communiante (mai 1917) ; le grand-père tenant le journal La Croix ; la famille Hosmalin ; Léonie (sans date) 1914-1917 Correspondance : 105 lettres ornées de dessins, de sa fille Marguerite, domiciliée à Joudreville (Meurthe-et-Moselle), réfugiée à Saint-Ouen ( auj Seine-Saint-Denis), Saint-Quentin en Auvergne et Riom (Puy-de-Dôme) 1914-1917 Billet d'honneur décerné à Marguerite en 1915 ; diplôme « Paroisse Saint-Jean-de-Montmartre Souvenir de la Grande guerre 1914-1915 » : « cette année-là, dans un élan de patriotisme les enfants de France renoncèrent à leurs livres de prix en faveur des soldats combattants ou blessés » ; certificat d'études primaires (juillet 1918) 1915-1918.

PA_102 Documents concernant Paul Gabriel Ottello, boucher, 36e RIT 1915.

3 photographies : Gabriel Ottello en uniforme ; la famille Otello 30 cartes postales, cartes militaires, cartes humoristiques et patriotiques, plusieurs dessinées par Gabriel Otello, certaines sur écorce de bouleau : « Cadeau de Guillaume Le Turc Quelle sale pipe » ; « Major von Durpif» ; « Boche retour de la Marne » ; « Un souffle patriotique irrésistible entraine nos soldats » ; « Honneur aux Alliés Vive la France » ; « rue de Venise ! » : tranchée inondée ; « Après l'attaque » ; « Et s'il gèle cette nuit - Ben mon vieux, on pourra s'asseoir » ; « Le dernier embusqué » ; « Après le crime » (torpillage du Lusitania) ; « Verdun » ; « Retour de permission » ; « Les vieux aux tranchées, les jeunes aux sillons » ; « Graine de poilu » ; « Après la bataille, dernier hommage ! » ; [voyage à reculons] ; « Digne épouse » ; « Le champ d'ail » ; « Souvenir à mon petit Roland, l'enfant de la guerre » ; « La Terre promise » ; « Un zeppelin naufragé »  3 lettres, dont 2 de sa fille Simone (1915), relatant leur retour en France depuis la Belgique, via l'Angleterre Menu de Noël 1914 ; procuration de G Ottello à sa femme Angèle (1915) ; laissez-passer du consul de France à Rotterdam pour Simone Ottello (1915) 20 épinglettes et médailles de guerre.

PA_103 Documents concernant des Poilus à Nouméa, Nouvelle-Calédonie sans date.

1 photographie d'écolières costumées en Alsaciennes-Lorraines, prise le jour du retour des Poilus à Noumea.

PA_104 Documents concernant Mme Debrouvet 1915.

Récépissé de versement d'or pour l'emprunt pour la Défense Nationale.

PA_105 Documents concernant François Poupon, originaire de Saint-Goazec (Finistère), 2e régiment de chasseurs 1914-1918.

Croix de guerre, plaque d'identité ; livret militaire ; deux citations (1918).

PA_106 Documents concernant le sous-lieutenant Gabriel Lorch († 22 avril 1915), 73e RIT 1914.

Carnet de notes manuscrit Août-décembre 1914.

PA_107 Documents concernant Raphaël Dérou, 6e RI 1914-1938.

Livret militaire ; citation (1918) ; carte de combattant (1933) ; titre de libération (1938).

PA_108/1 Documents concernant Jean Augustin Fontaine 1914-1918.

Cahier scolaire de Marguerite Fontaine, sa fille, école de Levallois-Perret ( auj Hauts-de-Seine), avec des leçons sur la guerre : « Causes », « Le front français d'août 1914 à avril 1917 », « La Russie », « La Serbie », « L'Italie », « La Roumanie », « L'Asie », « Guerre sur mer », « Colonies allemandes », « Avril 1917 à mai 1918 », « L'armée française », « En Italie », « La Révolution russe », « En Grèce », « Bagdad et la Palestine », « Paix imposée à la Roumanie », « Conditions de paix de l'Entente », « Aide américaine », « La bataille du Kaiser », « La bataille pour Paris » 1914-1918 1 lettre à son épouse Août 1918.

PA_108/2 Documents concernant le maréchal des logis Georges Hinderman 1914-1918.

Croquis représentant Hindermann (décembre 1914) ; lettre de 12 pages d'Hinderman à son épouse (juillet 1918).

PA_108/3 Documents concernant André Bellanger 1915.

