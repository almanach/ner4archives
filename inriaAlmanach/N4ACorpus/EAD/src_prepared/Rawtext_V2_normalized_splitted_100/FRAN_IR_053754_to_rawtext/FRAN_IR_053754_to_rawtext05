400AP/44 Correspondance échangée entre Napoléon III et divers membres de sa famille, d’une part, et Rouher, d’autre part 1860-1879.

38 lettres de Napoléon III à Rouher, 1864-1871  28 lettres de Rouher à Napoléon III, 1860-1870  Lettres du prince impérial à Rouher, 1873-1879  Lettres du prince Napoléon (Jérôme) au même, 1861-1873 Une lettre du prince concerne le conseil général de la Corse Quelques lettres du prince Napoléon à l’Impératrice, sous forme de copie, ont été classées ici.

400AP/45 Dossier 1 « Lettres du docteur A Mels à M Piétri » 1870-1879.

Dix-neuf lettres adressées par Mels, écrivain allemand, secrétaire de Napoléon III à Wilhelmshöhe, à François Piétri, secrétaire de l’Impératrice.

400AP/45-400AP/47 « Lettres de souverains et princes adressées à Napoléon III ».

400AP/45 Dossier 2 Autriche ; Bade ; Bavière.

400AP/45 Dossier 3 Belgique ; Brunswick.

400AP/45 Dossier 3 Danemark ; Égypte ; Espagne.

400AP/45 Dossier 5 Grande-Bretagne ; Grèce.

400AP/46 Dossier 1 Hanovre ; Hesse-Darmstadt ; hohenzollern.

400AP/46 Dossier 2 Italie ; Japon ; Mexique.

400AP/46 Dossier 3 Moldo-Valachie ; Monaco ; Naples ; Parme ; Pays-Bas ; Portugal ; Prusse.

400AP/47 Dossier 1 Roumanie ; Russie.

400AP/47 Dossier 2 Saint-Siège ; Saxe.

400AP/47 Dossier 3 Suède (et Norvège).

400AP/47 Dossier 4 Toscane ; Turquie ; Wurtemberg.

400AP/48 Copies de lettres de souverains à Napoléon III, avec quelques réponses 1852-1859.

400AP/49-400AP/51 Lettres de souverains et de princes adressées à l'impératrice Eugénie.

400AP/49 Dossier 1 Bade.

Lettres des Grandes duchesses de Bade : Stéphanie de Beauharnais et Louise de Prusse.

400AP/49 Dossier 2 Grande-Bretagne.

Lettres de :  princesse Héléna de Sclesvig-Holstein duc et duchesse d'Albany duchesse d'Argyle duc et duchesse de Cambridge roi George et reine Mary roi Édouard et reine Alexandra duc et duchesse de Conaught duc et duchesse d'Édimbourg.

400AP/49 Dossier 3 Metternich (prince et princesse de).

400AP/50 Dossier 1 Mexique (Charlotte, impératrice du).

400AP/50 Dossier 2 Pays-Bas (Sophie, reine des).

400AP/50 Dossier 3 Prusse.

Lettres de :  roi Guillaume Henri, prince de Reuss et son épouse.

400AP/51 Dossier 1 Saint-Siège ; Suède (et Norvège).

Lettres reçues de :  pape Pie IX ; roi Oscar de Suède ; reine Joséphine de Suède.

400AP/51 Dossier 2 Lettres de condoléances adressées à l’Impératrice lors du décès de Napoléon III, puis à l’époque du décès du prince impérial 1873, 1879.

Dans le premier cas, il s’agit surtout de dépêches, de nombreux télégrammes provenant des municipalités italiennes.

400AP/52 Lettres adressées à Napoléon III et à l’Impératrice par divers 1832-1918.

Les lettres ne sont pas classées  On trouve entre autres correspondants : Bazaine ; Canrobert ; Chateaubriand (trois lettres, 1832 et 1842) ; Clémenceau (une lettre à l’Impératrice pour la remercier d’un document donné par elle et remis aux Archives nationales, 1918, une lettre de l’Impératrice à Clemenceau, 30 novembre 1918, avec une réponse dactylographiée et signée, 13 décembre 1918) ; Victor Cousin ; Mgr Darboy ; Gustave Doré ; Octave Feuillet ; Flahaut ; Achille Fould (certaines lettres concernent le budget et la situation financière de la France, 1861-1865) ; Mérimée ; Émile Ollivier (avec des minutes de réponses de l’Empereur) ; Pasteur ; Renan ; Sainte-Beuve (deux lettres, 1865) ; Sandeau (trois lettres, s d [1873 et 1879]) ; Sismondi ; Veuillot ; Vieillard ; Vigny (une lettre, 1854) ; comte Walewski (dont une sur le traité de Paris, 1856).

400AP/53 Lettres adressées à l’Empereur et à l’Impératrice par des membres de leur famille 1825-1910.

400AP/53 Dossiers 1 et 2 Lettres à Napoléon III 1825-1872.

400AP/53 Dossier 1 Lettres du roi Jérôme et de la princesse Mathilde 1825-1872.

400AP/53 Dossier 2 Lettres du prince Napoléon 1836-1872.

400AP/53 Dossier 3 et 4 Lettres à l'impératrice Eugénie 1854-1910.

400AP/53 Dossier 3 Lettres du roi Jérôme (1856-1859), du prince Napoléon Jérôme ( 1854-1890) et du prince Louis Napoléon (1885-1905) 1854-1905.

400AP/53 Dossier 4 Lettres de la princesse Mathilde (1856-1901) et de la princesse Marie-Clotilde [Marie-Clotilde de Sardaigne, épouse du prince Napoléon] (1859-1910) 1856-1910.

400AP/54 Correspondance et discours de Napoléon III ; testament 1853-1873.

Lettres adressées à l’Empereur et pièces diverses relatives aux attentats commis contre sa personne, 1853-1867  Rapports ayant trait aux relations de l’Empereur avec sa famille, en particulier aux allocations attribuées, 1853-1861  Discours de l’Empereur, minutes et exemplaires imprimés, 1848-1871  Notes dictées par lui, 1856-1872 et s d  Lettre et statistique relatives au plébiscite de 1870  Liste des membres de l’Assemblée nationale ayant protesté contre la déchéance (avec signatures), 1er mars 1871  Testaments de l’Empereur, 1873 (expéditions ; Me Mocquard, notaire), et copie du procès-verbal relatif à son inhumation rédigé par Maret, 1873.

400AP/55-400AP/56 Campagne de Crimée.

400AP/55 Correspondance.

400AP/55 Dossier 1 B.

Lettres du général de Béville et du général Bosquet.

400AP/55 Dossier 2 C.

Lettres du général Canrobert et lettres en rapport avec le Conseil de guerre.

400AP/55 Dossier 3 E - P.

Contient notamment des lettres de : Niel et Pélissier.

