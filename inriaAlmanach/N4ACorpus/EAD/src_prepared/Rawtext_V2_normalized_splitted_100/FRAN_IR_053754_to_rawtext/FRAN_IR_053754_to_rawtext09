Réclamations contre le gouvernement hessois et le Trésor français présentées par Jérôme, qui estimait être créancier du gouvernement français à l’époque de la Monarchie de Juillet.

400AP/92 Regnault de Saint-Jean d'Angély (comtesse) - Royer 1831-1857.

A noter : Rovigo (duc de), 3 lettres, 1831-1835 ; Royer (de), lettre, 1857.

400AP/93 Saint-Arnaud - Wurtemberg 1816-1860.

A noter : Saint-Arnaud, lettres, 1852-1854 ; Sénat, papiers le concernant pendant la présidence de Jérôme, 1852 [Jérôme devint président du Sénat le 28 janvier 1852] ; Thiers, lettres, 1837-1860 ; Vaillant (maréchal), 1851-1858 ; Victor-Emmanuel II, lettre, 1859 ; Wurtemberg (Guillaume Ier, roi de), lettres, 1816-1857, et lettres provenant d’autres membres de la famille de Wurtemberg.

400AP/94 Westphalie 1812-1813.

Finances du royaume de Westphalie : rapports au roi, budgets.

400AP/95 Lettres et instructions de Napoléon Ier 1799-1813.

«Lettres de Napoléon Ier à son frère Jérôme 1799-1813 » 193 lettres de Napoléon Ier au roi Jérôme Un volume plein maroquin vert  Instructions données par l’Empereur à Bessières à propos du mariage du prince Jérôme avec Catherine de Wurtemberg, pièce signée, 5 août 1807 ; lettres par lesquelles Frédéric-Guillaume, prince royal de Wurtemberg, accepte la procuration que lui a donnée le prince Jérôme pour le représenter lors des cérémonies du dit mariage, 12 août 1807.

400AP/96 « Rapports de la liste civile avec les princes Jérôme et Napoléon » 1852-1870.

Minutes d’arrêtés, notes, lettres, émanant la plupart du temps du ministère de la Maison de l’Empereur, adressées au roi Jérôme et au prince Napoléon, son fils, à propos de leur maison, leurs frais de voyage et leurs résidences, avec des réponses provenant des secrétaires des commandements des dits princes.

400AP/97-400AP/104 Papiers de Catherine de Wurtemberg, reine de Westphalie (1783-1835) 1792-1835  Catherine (reine de Westphalie ; 1783-1835).

Correspondance et pièces diverses.

400AP/97 Alexandre Ier - Brusselle (général de) 1806-1835.

A noter: Alexandre Ier, empereur de Russie, 19 lettres, 1814-1825 ; autopsie de la princesse de Montfort [titre de Catherine de Wurtemberg], procès-verbaux, 30 novembre et 3 décembre 1835 ; biographies, fragments deMémoiresrédigés par la reine, début deMémoires, 1 cahier, notes sur la vie de la reine de 1806 à 1820, 1 cahier, principaux événements de 1813 à 1818, carnet, journal de la reine, 21 mai-6 août 1818, volume plein chagrin vert, journal d’un voyage de la reine à Laybach, avril-mai 1821, volume plein chagrin vert, journal de la reine, août-novembre 1835, volume plein chagrin vert (voir aussi 400AP/102) ; Brusselle (général de), correspondance avec M de Stölting, 1815 400AP/102.

400AP/98-400AP/100 Catherine 1808-1835.

Copies-lettres de la reine Catherine et quelques copies de lettres.

400AP/98  1808-1821.

400AP/99  1817-1835.

400AP/99  1817-1820.

[Cette partie a été en partie déclassée et les lettres ne sont pas dans un ordre strictement chonologique].

400AP/99  1821-1822.

400AP/99  23 novembre 1825-1830.

400AP/99  1834-1835.

400AP/100 Lettres de la reine Catherine à son père, Frédéric Ier, roi de Wurtemberg, 1811-1813 ; lettre de la reine Catherine à sa belle-mère, Madame Mère, 1816, 14 pages 1811-1816.

400AP/101 Décrets - Louise d’Este 1813-1835.

A noter: Ellwangen (affaire d’), lettres et documents relatifs au choix de la résidence et à la fortune du prince Jérôme et de son épouse, 1815-1817 ; Fesch (cardinal), 3 lettres, 1820 ; François Ier, empereur d’Autriche, 5 lettres, 1815-1822 ; Genlis (Mme de), 3 lettres, 1813-1814 ; Hélène, grande-duchesse de Russie, lettres, 1825-1835 et s d ; Hortense, 2 lettres, 1835 ; Joseph, 4 lettres, 1819-1821 ; Louis, 2 lettres, 1820-1821 ; Louise d’Este [Louise d’Este épousa le 6 juin 1808 l’empereur d’Autriche, François Ier, et mourut le 7 avril 1816], 2 lettres, 1812.

400AP/102 Marie - Wurtemberg 1792-1834.

A noter: Marie, impératrice mère de Russie, lettres, 1792-1826 ; Marie-Louise, impératrice, 39 lettres [ces lettres ont été publiées dans laRevue des deux mondes, mai-juin 1928, p 386 à 426, par E d’Hauterive], 1810-1814 ; Maubreuil (affaire), documents divers, 1815-1817 ; Metternich, 7 lettres 1815-1824 ; Napoléon Ier, 14 lettres signées, 1807-1814 ; Nicolas Ier, empereur de Russie, 8 lettres, 1826-1834 ; Pauline, lettre signée, s d ; Valence (marquise de), lettres, 1829-1831 et s d ; voyages, en 1810 en Hollande, carnet non relié, en 1811, « journal de ma vie », volume plein chagrin vert, en 1812, « journal de ma vie », volume plein chagrin vert (voir aussi 400AP/97) 400AP/97.

400AP/103 Wurtemberg (suite) : lettres de plusieurs membres de la famille de Wurtemberg 1794-1816.

A noter: lettres de Frédéric Ier, roi de Wurtemberg, à sa fille : lettres originales, 1794-1814, un volume pleine basane rouge, grain long ; 23 lettres non reliées, 1815-1816 ; copies de certaines de ces lettres, 1806-1807, 1810 et 1813-1814.

400AP/104 Wurtemberg (suite) - Zeppelin 1810-1835.

A noter: lettres de plusieurs membres de la famille de Wurtemberg, en particulier du frère de la reine, Guillaume Ier, 1810-1835.

400AP/105/A-400AP/105/B Papiers de la princesse Mathilde (1820-1904) XIXe siècle.

400AP/105/A Deux minutes de lettres de la princesse à Sainte-Beuve sans date.

Une de ces deux lettres concerne Renan.

400AP/105/A « Asile Mathilde ».

Trois photographies de l’asile Mathilde.

400AP/105/A « Livre d’adresses ».

400AP/105/B Diplômes divers décernés à la princesse Mathilde, notamment en raison des peintures présentées par elle lors de salons ou d’expositions 1861-1900.

400AP/105/B Photographies de la princesse, et d’un tableau d’Ary Scheffer la représentant.

400AP/106-400AP/169 Papiers du prince Napoléon (1822-1891) XVe-XIXe siècles  Bonaparte, Napoléon-Joseph-Charles-Paul (1822-1891).

400AP/106-400AP/167 Correspondance et pièces diverses classées par ordre alphabétique XVIe-XIXe siècles.

400AP/106 Aali Pacha - Alexandre-Jean, prince de Roumanie 1822-1878.

A noter: Abd-el-Kader, 2 lettres et 2 transcriptions, 1854, 1860 et s d ; About (Edmond), lettres, 1859-1867 ; actes d’état civil relatifs au prince, 1822-1860 ; adresses venant de Corse, des Etats-Unis, de France, d’Italie, 1848-1872 ; Agoult (comtesse d’), lettres, 1862-1874 ; Ajaccio, adresse du conseil municipal à l’occasion de la naissance du prince Louis, 1864, élections législatives de 1876 et 1877 dans l’arrondissement d’Ajaccio, 1876-1878, achat d’une terre à Canniccio, près d’Ajaccio.

400AP/107 Ambassade en Espagne - Avila (comte d’) 1840-1889.

A noter: ambassade en Espagne en 1849, 1849-1850 ; Amélie, impératrice douairière du Brésil [fille d’Eugène de Beauhamais et veuve, depuis 1834, de l’empereur Pedro Ier], lettre, 1843 ; Amigues (Jules), lettres, 1873-1879 ; Angleterre, notes sur la succession, 1840 ; Annam (ambassadeurs du roi d’), lettre, 1862 ; Antioche (patriarche d’), lettre, 1863 ; Arago (François), lettre, 1848 ; Arménie (Léon, prince d’), lettre, 1860 ; Augier (Émile), lettres, 1860-1889 ; Aurelle (général d’), 2 lettres, 1860 et 1861 ; Avila (comte d’), lettres, 1861-1876.

400AP/108 Babinet - Berton 1841-1889.

A noter: Bacciochi, documents sur le majorat, 1841-1862, dépôt du testament de la comtesse Camerata [née Napoléonne-Élisa Bacciochi], 1869 ; Bakounine, lettre, 1864 ; Barrot (Odilon), lettre, 1845 ; Baudin (amiral), lettre, 1846 ; Baudrillart (Mgr), lettre, 1867 ; Bazaine, lettres, 1871 et 1874 ; Beaufort d’Hautpoul (général de), lettres, 1860-1870, note sur la situation en Syrie, 1860 ; Béhic, ministre de l’Agriculture, du Commerce et des Travaux publics, lettres, 1863, 1865 ; Benedetti (comte), lettres, 1861-1889 et s d ; Bernard (Claude), lettre relative au don d’un buste de Bonaparte à l’Institut, 1869 ; Berton (Herman), colonel américain, lettres sur les relations franco-américaines, 1869-1870.

400AP/109 Bertrand : papiers du général remis par la vicomtesse Henri Bertrand au prince Napoléon en 1881.

