Pièces d’état civil relatives au mariage de la reine Hortense, minute et copie de son testament, pièces concernant son décès (survenu le 5 octobre 1837)  Lettre d’Élisa à Hortense [Vers 1802]  Huit lettres de Louis à Oudinot, duc de Reggio (15 avril-30 juin 1810) ; copie de la lettre par laquelle Louis annonçait au Corps législatif sa décision de renoncer à la couronne (1er juillet 1810) ; lettre de Van Hugenpoth, ministre de la Justice de Hollande, à Oudinot sur l’occupation de la Hollande et l’abdication du roi (10 juillet 1810)  Mémoire présenté au nom du comte de Saint-Leu, par Meyer, à l’empereur de Russie et au roi de Prusse, à propos des intérêts du comte en Hollande, Amsterdam  Copie de l’inventaire après décès du roi Louis (survenu le 25 juillet 1846) et pièces diverses  Documents concernant le décès (survenu le 17 mars 1831, à Forli) et l’enterrement du prince Napoléon-Louis Pièces ayant trait à la succession de l’impératrice Joséphine  Inventaire de ses biens à la Martinique (11, 12 et 13 avril 1815) par Durieu, notaire, notes et observations Copies de lettres Conventions additionnelles au partage (20 décembre 1816) Lettre sur le troupeau de moutons de Malmaison donné au baron de Vitrolles (10 avril 1823).

400AP/35 La reine Hortense 1806-1837.

400AP/35 Dossier 1 Minutes de lettres ou lettres 1806-1837.

Lettres à :  prince Napoléon-Louis (1828-1831) prince Louis-Napoléon (1819-1833) Mme de Broc (1806-1812) Mme Salvage de Faverolles (1820-1835) M et Mme Vieillard (1821-1837) divers (1807-1837 et s d).

400AP/35 Dossier 2 Projet de lettre d’Hortense à Jérôme à propos des fiançailles du prince Louis-Napoléon avec la princesse Mathilde (juillet 1836) ; projet de contrat de mariage.

400AP/35 Dossier 3 Notes d’Hortense, entre autres sur le divorce de Napoléon Ier.

400AP/35 Dossier 4 Récit par Mme Salvage du voyage en France de la reine Hortense après l’affaire de Strasbourg Octobre-novembre 1836.

400AP/35 Dossier 5 Inventaire des bijoux, tableaux, meubles, services d’argenterie et de vermeil de la reine Hortense (1817 et s d) ; pièces ayant trait à ses biens en Italie (1823) 1817-1823.

400AP/36 Notes, poème et mémoire.

400AP/36 Dossier 1 « Notes et réflexions par le roi Louis-Napoléon ».

Notes historiques sur les premières années, les campagnes d’Italie et d’Égypte, le 18 brumaire, l’Empire, la Hollande  Réflexions historiques sur des sujets divers : religion, gouvernement, mariage, guerre, peine de mort, moeurs.

400AP/36 Dossier 2 « Le retour Poème en dix chants par le comte de Saint-Leu, Louis-Napoléon Bonaparte, ancien roi de Hollande » Florence, 19 mai 1843 19 mai 1843.

Manuscrit d’un poème sur le retour de l’Empereur de l’île d’Elbe, signé par le comte de Saint-Leu.

400AP/36 Dossier 3 Mémoires de la reine Hortense Augsbourg, 1820 1820.

Manuscrit de 550 pages.

400AP/37 Mémoires de la reine Hortense.

400AP/37 Dossier 1 Fragment des « Mémoires » de la reine Hortense.

« Tome I » Les 209 pages écrites correspondent aux 177 premières pages du volume deMémoiresanalysé en 400AP/36 Un volume plein chagrin rouge, grain long  « Tome II » Volume relié plein chagrin rouge, grain long, sans aucun texte.

400AP/37 Dossier 3 Copie des « Mémoires », sans doute de la main de Mme Salvage.

400AP/38 Comptes, romances et partitions 1806-1837.

400AP/38 Dossier 1 Comptes et, en particulier, état du produit de la succession de l’impératrice Joséphine 1817-1837.

400AP/38 Dossier 2 Textes manuscrits de romances et partitions composées par la reine Hortense à Augsbourg, Arenenberg et Rome 1806-1832.

400AP/38 Dossier 3 Prince Napoléon-Louis (1804-1831) [vers 1815]-1827.

Cahiers d’études du prince (français, allemand, latin)  Plan de ses études, 1827  Échange de réflexions entre le roi Louis et M Vieillard sur l’éducation du prince  Notes prises par lui au cours d’un voyage à Volterra, 1827  Biographie imprimée du prince extraite de laBiographie des hommes du jour, rédigée par Belmontel (Une note manuscrite sur la couverture indique que cette feuille avait été distribuée à toute la garnison de Strasbourg).

400AP/39-400AP/79 Napoléon III (1808-1873), l’impératrice Eugénie (1826-1920), et le prince impérial (1856-1879) XIXe-XXe siècles  Bonaparte, Louis (1856-1879) Napoléon III (empereur des Français ; 1808-1873) Eugénie (impératrice des Français ; 1826-1920).

400AP/39 Dossier 1 Actes d'état civil 1808-1856.

Extrait baptistaire de Napoléon III, né le 20 avril 1808, ondoyé, puis baptisé le 4 novembre 1810, 8 mai 1827, non signé, pièce non reliée  Acte de mariage de l’Empereur et de l’impératrice Eugénie, 30 janvier 1853, signé par les époux, le roi Jérôme, l’archevêque de Paris, etc  Acte d’ondoiement du prince impérial, 16 mars 1856, signé par l’Empereur, le comte de Morny, Mgr Menjaud, évêque de Nancy, etc  Acte de baptême du même, 14 juin 1856, signé par ses parents, le cardinal Patrizi, etc.

400AP/39-400AP/41 Lettres du prince Louis-Napoléon 1813-1909.

400AP/39 « Lettres du prince Louis-Napoléon à la reine Hortense » 1813-1837.

400AP/39 Dossier 2 « I 1813-1831 » 1813-1831.

79 lettres autographes signées.

400AP/39 Dossier 3 « II 1831-1837 » 1831-1837.

71 lettres : 70 autographes signées et une autographe non signée.

400AP/40 Lettres au roi Louis, au prince Napoléon-Louis et divers 1825-1846.

« Lettres du prince Louis-Napoléon au roi Louis I 1825-1835 » 55 lettres autographes signées, une copie, une lettre autographe non signée  « Lettres du prince Louis-Napoléon au roi Louis, au prince Napoléon-Louis et divers II 1835-1846 » 56 pièces, dont 30 lettres autographes signées au roi Louis, dix lettres autographes signées à Napoléon-Louis, et d’autres à divers A noter : une protestation du prince Louis-Napoléon réclamant l’épée de Napoléon 1er et des notes intitulées : « Souvenirs de ma vie » ; « Rôle que doit jouer la France en Europe » ; « La captivité » ; « Impressions d’un prisonnier » ; « Le Credo » ; « Paraphrases sur la Cloche de Schiller » ; « Notes sur l’histoire d’Angleterre ».

400AP/41 Lettres du prince Louis-Napoléon puis de l'empereur Napoléon III 1830-1909.

400AP/41 Dossier 1 « Lettres du prince Louis-Napoléon à M Vieillard, à M Salvage et à M Desportes » 1830-1849.

74 pièces dont 50 lettres à M Vieillard, 12 lettres à Mme Salvage de Faverolles, 6 lettres à M Desportes.

400AP/41 Dossiers 2 Copies de lettres du prince, puis de l’Empereur, à Hortense Cornu [fille de Mme Lacroix, femme de chambre de la reine Hortense] 1823-1872.

400AP/41 Dossier 3 Minutes ou copies de lettres adressées par le prince à divers 1833-1847, 1851.

Lettres envoyées notamment à : colonel Vaudrey ; Odilon Barrot ; le président des États-Unis ; le président du petit conseil du canton de Thurgovie, 1833-1847  Lettre du prince-président à un général, 10 mars 1851  Pièces contenues dans le portefeuille de Louis-Napoléon saisi à Boulogne et estampillées : « Empire français Direction générale des Archives » (1840), et copies de documents ayant trait à cette affaire (1840-1842).

400AP/41 Dossier 4 Minutes de lettres de l’Impératrice à divers 1873-1909.

Minutes de lettres de l’Impératrice à divers, entre autres Paul de Cassagnac, Olivier, le prince Napoléon, prisonnier à la Conciergerie, 1873-1909 et s d Notes de l’Impératrice sur ses prétendusMémoires, [1909].

400AP/42 Correspondance active et passive de Napoléon III 1853-1870.

400AP/42 Dossier 1 Minutes de lettres et lettres de Napoléon III aux ministres, au duc de Morny, à l’Impératrice, à des officiers, classées par ordre chronologique 1853-1870.

400AP/42 Dossier 2 Lettres et rapports adressés à Napoléon III par les maréchaux Niel, Randon, Regnault de Saint-Jean d’Angély, et Vaillant 1857-1870.

400AP/42 Dossier 3 Note sur le traité de 1860 avec la Chine annotée par l’Empereur 1860.

400AP/42 Dossier 4 Lettre de l’ambassadeur de France à Berlin envoyant au ministre des Affaires étrangères une traduction de la Constitution fédérale de l’Allemagne du Nord (1867) ; articles de journaux sur la question romaine (1867) ; note sur un dîner du prince Napoléon chez Émile de Girardin (s d) 1867.

400AP/42 Dossier 5 Notes sur l’armée adressées à Napoléon III 1865.

400AP/43 « Lettres de l’empereur Napoléon III à l’impératrice Eugénie » 1854-1872.

