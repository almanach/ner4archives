312 Paris Lettres de rémission accordées à Gillet Baudoin, compagnon cartier, demeurant rue Saint-Denis, à Paris, pour meurtre commis sur la personne de Jean Bernollet, qu'il avait surpris chez lui en flagrant délit de vol avec effraction (Fol 146 v°, n° 312) Septembre 1498.

313 Paris Lettres de rémission accordée à Guillot, Jean et Antoine Guillemin, frères, Pierre et Claude Guillemin, cousins des précédents, Pierre et Jean Longre, Légier Ymbert, Nicolas et Jean Hardouin, habitant à Saint-Loup-de-Gonois 1, au bailliage de Sens, pour meurtre commis le 21 juillet 1498 sur la personne de Philibert Montaulx, qui en compagnie de son frère Antoine Montaulx, avait voulu s'emparer par la force d'une partie des récoltes appartenant à Guillot Guillemin (Fol 147, n° 313) Septembre 1498.

1 Saint-Loup-de-Gonois, Loiret, arr de Montargis.

314 Paris Lettres de rémission accordées à Jean Malnorry, natif de Sandillon 1, au bailliage d'Orléans « nommé franc-archer pour nous servir au pays de Bourgogne, sous la charge du capitaine Guillaume Bongars », qui étant en sa garnison à Auxonne 2, le 3 août 1498, a tué un certain Jean Desmard, aussi franc-archer, qui prétendait l'expulser par la violence de son logement (Fol 147 v°, n° 314) Septembre 1498.

1 Sandillon, Loiret, arr d'Orléans 2 Auxonne, Côte-d'Or.

315 Paris Lettres de rémission accordées à Pierre Le Saige, natif des environs de Lille en Flandre, pour meurtre commis en 1485 à Ypres 1, au comté de Flandres, sur la personne de Jean Platel, natif des environs de Douai, qui l'avait grossièrement injurié et faussement accusé d'avoir « séduit une femme bien famée de ladite d'Ypres » (Fol 148, n° 315) Septembre 1498.

1 Ypres, Belgique, prov de la Flandre occidentale.

316 Paris Lettres de rémission accordées à Philippe Hervée, femme Olivier, « marchande hostelière et tavernière publique » à Tours, accusée d'être la complice d'un vol commis par un nommé Fouquet, orfèvre, et un certain Jean Vannier, chez la veuve de Jean Jalon (Fol 148, n° 316) Septembre 1498.

317 Paris Lettres de rémission accordées à Thomas Regnault, habitant à Castillon 1, diocèse de Bayeux, bailliage de Caen, pour meurtre commis sur la personne de Robin Scelle qui, passant à Planquery 2, avec plusieurs compagnons de guerre au retour de la montre faite à Cerisy 3 des gens de pied sous la charge de Colin de Silly, seigneur de Dompierre, avait mis à sac la maison de Jean Regnault, père du suppliant, et avait déloyalement attaqué ce dernier et ses deux frères, Guillaume et Raoul Regnault (Fol 148 v°, n° 317) Septembre 1498.

1 Castillon, Calvados, arr de Bayeux, cant de Balleroy 2 Planquery, Calvados, arr de Bayeux, cant de Balleroy 3 Cerisy-la-Forêt en l'abbaye, Manche, arr de Saint-Lô.

318 Paris Lettres de rémission accordées à Petit-Pierre Fournier, habitant les Écures 1, paroisse de Reterre 2en Combraille, au bailliage de Montferrand, qui étant à la chasse aux bécasses avec Gilbert Le Goys, en novembre 1497, a tué Gabriel Ribière qui lui avait cherché une mauvaise querelle (Fol 149, n° 318) Septembre 1498.

1 Les Écures, Creuse, arr d'Aubusson, cant d'Evaux 2 Reterre, Creuse, arr d'Aubusson.

319 Paris Lettres de rémission accordées à Jean de Marcille, habitant le Harps 1, paroisse de Ribay, aux comté et sénéchaussée du Maine, pour meurtre commis le 5 novembre 1497 sur la personne de Antoine Bouvant, qui avait gravement blessé à l'auberge le serviteur du suppliant, François Boisbonnier (Fol 149 v°, n° 319) Septembre 1498.

1 Harps (Le), Mayenne.

320 Paris Lettres de rémission accordées à Collart Courcol, habitant le Tranlay 1, prévôté de Péronne, pour meurtre commis le 16 avril 1497 sur la personne de Charlot de Grincourt, qui avait pris sans raison parti dans une querelle survenue entre son cousin Jean Planchon et le suppliant (Fol 149 v°, n° 320) Septembre 1498.

1 Tranlay, Somme, arr d'Abbeville, cant de Gamaches.

321 Paris Lettres de rémission accordées à Pierre Picart, religieux de l'abbaye Saint-Maixent 1, de l'ordre de de Saint-Benoît, en Poitou, et sacristain de ladite abbaye, accusé du meurtre de Gilles Chaignon, lequel faisait partie d'une bande de mauvais garçons qui depuis plusieurs jours ne cessaient d'assaillir « l'hostel et sacristainerie » du suppliant (cf infra, JJ 230, n° 324, fol 151 v° (Fol 150, n° 321) Septembre 1498.

1 Saint-Maixent, Deux-Sèvres.

322 Paris Lettres de rémission accordées à Colinet Quiévery, habitant Bezinghem 1, au comté de Boullenois, pour meurtre commis le 24 juin 1498 sur la personne de Gannot Tristan, avec lequel le suppliant, Gillet Dacquin et Jacques Le Sergent revenaient de la foire de Wicquinghem 2 (Fol 150 v°, n° 322) Septembre 1498.

1 Bezinghem, Pas-de-Calais, arr de Montreuil-sur-Mer 2 Wicquinghem, Pas-de-Calais, arr de Montreuil-sur-Mer.

323 Paris Lettres de rémission accordées à Pasquier Reddoyn, homme de labour, demeurant à Chambray 1, au bailliage de Touraine, pour meurtre commis en août 1498 sur la personne de son beau-frère, Jean Chollet, qui lui avait dérobé la plus grosse partie d'une récolte de foin qu'ils auraient dû partager (Fol 151, n° 323) Septembre 1498.

1 Chambray, Indre-et-Loire, arr de Tours.

324 Paris Lettres de rémission accordées à Savinian Aymeret, religieux de l'abbaye de Nieul-sur-l'Autise 1, de l'ordre de Saint-Augustin, sénéchaussée de Poitou, prétendu complice de Pierre Picart, religieux de l'abbaye de Saint-Maixent, accusé du meurtre de Gilles Chaignon (cf supra, JJ 230, n° 321, fol 150) (Fol 151 v°, n° 324) Septembre 1498.

1 Nieul-sur-l'Autise, Vendée, arr de Fontenay-le-Comte.

325 Paris Lettres de rémission accordées à Petit-Jean Fouquet, marronnier, habitant à Boulogne-sur-Mer, sénéchaussée de Boullenois, pour meurtre commis le 15 mai 1498 sur la personne de Préminot Le Conte, charretier, lequel avait conçu une haine mortelle contre le suppliant et l'avait attaqué, à la suite d'une querelle qu'il avait eue avec un de ses parents, Perrot Fouquet (Fol 152, n° 325) Septembre 1498.

326 Paris Lettres de rémission accordées à Robinet des Bordes, demeurant à la Neuville-en-Hez 1, au bailliage de Senlis, pour prétendue violence faite par lui, le 10 août, à Rue-Saint-Pierre 2, à Jehanne, femme de Nicolas de Marsennes, habitant à Gicourt 3, laquelle est depuis longtemps connue pour des départements (Fol 152 v°, n° 326) Septembre 1498.

1 Neuville-en-Hez (La), Oise, arr et cant de Clermont 2 Rue-Saint-Pierre (La), Oise 3 Gicourt, Oise, arr et cant de Clermont.

327 Paris Lettres de rémission accordées à Jean Pollet, habitant du village de Hallivilliers, au bailliage d'Amiens, pour meurtre commis en septembre 1497 sur la personne de Jean Canée dont Jehannin Pallet, fils du suppliant, avait refusé d'épouser la fille Marguerite prétendument enceinte de ses oeuvres, et qui en avait conçu une haine mortelle contre le suppliant qu'il avait déloyalement attaqué (Fol 153, n° 327) Septembre 1498.

328 Paris Lettres de rémission accordées à Jehannot Collesson, tisserand de toile, demeurant à Landricourt 1, en Champagne, bailliage de Vitry 2, qui se trouvant à Ambrières 3, le 15 août 1498, à la fête du village, en compagnie de son neveu Thichenot Barbier, monstre des escales » de Landricourt, a tué à la suite d'une querelle de l'auberge un nommé Louis Groslot (Fol 153 v°, n° 328) Septembre 1498.

1 Landricourt, Marne, arr de Vitry-le-François 2 Vitry-le-François, Marne 3 Ambrières, Marne, arr de Vitry-le-François.

329 Paris Lettres de rémission accordées à Wallat Le Leu, fils de Pierrart Le Leu, pour meurtre commis en juillet 1498 à Marchiennes 1, au bailliage de Tournai sur la personne de Noël Le Prince, qui accompagné d'un certain [Noël] de Hucquegnies, dit de Baudegnies, serviteur du bailli de Marchiennes (cf supra, n° 302), avait assailli le suppliant, pour le venger du mariage de son frère, Grégolet Le Leu, avec la fille d'Honestaire Brébant, veuve de Jacques Le Seur (Fol 153 v°, n° 329) Septembre 1498.

1 Marchiennes, Nord, arr de Douai.

330 Paris Lettres de rémission accordées à Pierre et Herlin Boniface, « hommes de labour », habitant Frégnicourt 1, comté d'Artois, gouvernement de Péronne, Roye et Montdidier, pour meurtre commis le 16 août 1498, en revenant de la fête de la Bucquière 2, sur la personne d'un certain Jean de Mary (Fol 154 v°, n° 330) Septembre 1498.

1 Frégnicourt, Pas-de-Calais, arr d'Arras 2 Bucquière (La), Pas-de-Calais, arr d'Arras.

331 Paris Lettres de rémission accordées à Martin de Lorme, demeurant au Plessis-Franc 1, paroisse de Saint-Germain-le-Gaillard 2, pour meurtre commis le 24 septembre 1497, au Tartre 3, même paroisse, paroisse, à la suite d'une noce, sur la personne d'un nommé Jean Macé (Fol 155, n° 331) Septembre 1498.

1 Plessis-Franc (Le), com de Saint-Germain-le-Gaillard 2 Saint-Germain-le-Gaillard, Eure-et-Loir, arr de Chartres, cant de Courville-sur-Eure 3 Tartre (Le), arr de Chartres, cant de Courville.

332 Paris Lettres de rémission accordées à Michel Chassaing, demeurant à Brioude, au bailliage de Montferrand, censier du chapître Saint-Julien de Brioude, pour meurtre commis le 5 juillet 1498, sur le chemin de Cougeat 1 à Brioude, sur la personne d'Alary Penchenat, qui avait ravagé à plusieurs reprises les terres du suppliant (Fol 155 v°, n° 332) Septembre 1498.

1 Cougeat, Haute-Loire, arr et cant de Brioude.

333 Paris Lettres de rémission accordées à Pierre Bérengier, ancien compagnon de guerre, demeurant à Bois-Robert 1, au bailliage de Caux, pour meurtre commis à Etables 2 le 9 septembre « dernier passé » sur la personne de Guillaume Gobet, en défendant contre ce dernier trois de ses anciens compagnons d'armes, Jacques Bauberel et Gillot et Hector, frères, bâtard de Jean du Plessis (Fol 156, n° 333) Septembre 1498.

1 Bois-Robert, Seine-Martime, arr de Dieppe, cant de Longueville-sur-Scie 2 Aujourd'hui Saint-Germain-d'Etables, arr de Dieppe.

334 Paris Confirmation des privilèges de la Sainte-Chapelle de Notre-Dame-du-Vivier, en Brie, au diocèse de Meaux (Fol 156 v°, n° 334) Septembre 1498.

335 Paris Lettres de rémission accordées à Pierre Maillefeu, demeurant à Bouillancourt-sous-Miannay 1, au au bailliage d'Amiens, comme complice du meurtre commis en juillet 1498, le jour de la fête de Saint-Sanson, sur les personnes de Jacquet du Cauffours et de Jean Guillemant, dit Lebon (voir infra, JJ 230, n° 351 (Fol 157, n° 335) Mars 1499.

1 Bouillancourt, Somme, arr d'Abbeville, cant de Moyenneville.

336 Paris Lettres de rémission accordées à Jeannequin Julien, habitant à Marques 1, au comté d'Aumale et au bailliage de Caux, pour meurtre commis, le 16 décembre 1498, aux Landes 2, comté d'Eu, sur la personne de Huchon Collombes, qui avait injurié et frappé dans l'exercice de ses fonctions le frère du suppliant, Jean Julien, collecteur de tailles (Fol 157 v°, n° 336) Mars 1499.

1 Marques, Seine-Martime, arr de Dieppe, cant d'Aumale 2 Landes-Vieilles-et-Neuves, Seine-Martime, arr de Dieppe, cant d'Aumale.

337 Paris Lettres de rémission accordées à Jacques du Fay, serviteur de René de Surgères, seigneur de la Floullière 1, Belleville-en-Thouarçois 2, Cersay 3, demeurant au château dudit Cersau, en la sénéchaussée de Poitou, pour meurtre commis le 3 février 1499 sur la personne de Jean Buschet, palefrenier, qui l'avait injurié et frappé, en dépit de sa qualité de gentilhomme (Fol 157 v°, n° 337) Mars 1499.

1 Floullière (La), localité disparue de la commune de Sainte-Verge, Deux-Sèvres, arr de Bressuire, cant de Thouars 2 Belleville-en-Thouarçois, Deux-Sèvres, arr de Bressuire 3 Cersay, Deux-Sèvres, arr de Bressuire.

338 Paris Lettres de rémission accordées à Antoine Sayres, habitant Castaigne-en-Albigeois, pour meurtre commis le 24 avril 1498 sur la personne de sa maîtresse Fine Fontaine, à la suite d'une scène de jalousie (Fol 158, n° 338) Mars 1499.

339 Paris Lettres de rémission accordées à Guerardin Letasseur dit Gaudois, habitant Mautart 1, près Abbeville, sénéchaussée de Ponthieu, pour meurtre commis le 5 juin 1498, à la suite d'une querelle de feu sur la personne de Nyvel de Farsure (Fol 158 v°, n° 339) Mars 1499.

1 Mautart, Somme, arr, cant et com d'Abbeville.

