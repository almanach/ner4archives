257 Paris Lettres de naturalité pour James Carrbes, archer de la garde écossaise (Fol 118 v°, n° 257) Août 1498.

258 Paris Création à la requête d'une foire à Guise, en Picardie, le 3 mai de chaque année (Fol 119, n° 258) Août 1498.

259 Paris Confirmation de privilèges commerciaux pour Avignon et le comté Venaissin (Fol 119, n° 259) Août 1498.

260 Paris Lettres de Louis XII vidimant des lettres de Charles VIII du 7 octobre 1494 et confirmant, à la requête de Jean de la Barre, trésorier et chanoine de l'église d'Angers, les privilèges du trésorier de cette église (Fol 119 v°, n° 260) Août 1498.

261 Paris Confirmation de la transaction passée entre les Chartreux de Notre-Dame de Vauvert, près Paris, et Jean Budé, audiencier de la Chancellerie de France, à la suite du procès depuis longtemps entamé entre lesdits Chartreux, feu Dreux Budé, aussi audiencier en la chancellerie, et son fils Jean Budé, transaction par laquelle les Chartreux cèdent à Jean Budé les terres qu'ils ont à Yerres 1, en échange de deux fiefs situés à Mandres 2, près Corbeil (Fol 120 v°, n° 261) Août 1498.

1 Yerres, Essonne, arr d'Evry 2 Mandres-les-Roses, Val-de-Marne, arr de Créteil, cant de Villecresnes.

262 Paris Lettres de rémission accordées à Pierre Thomynet, homme de labour, collecteur de tailles, demeurant à Bazoches 1, au bailliage de Meaux, prévenu de faux en écritures publiques de complicité avec Jacques Artus, « maistre des écolles » de Chevru 2 (Fol 121, n° 262) Août 1498.

1 Bazoches-les-Bray, Seine-et-Marne, arr de Provins, cant de Bray-sur-Seine 2 Chevru, Seine-et-Marne, arr de Coulommiers.

263 Paris Lettres de rémission accordées à Jean de Lyon et Simonne sa femme, demeurant à Villebéon 1, au bailliage de Château-Landon 2, pour meurtre commis le 16 juillet 1498 sur la personne de Jean Tignon coupable d'injures et de mauvais propos tenus à ladite Simonne (Fol 121, n° 263) Août 1498.

1 Villebéon, Seine-et-Marne, arr de Fontainebleau 2 Château-Landon, Seine-et-Marne, arr de Fontainebleau.

264 Paris Lettres de rémission accordées à Jean Aubert, potier d'étain, demeurant à Paris, prévenu de plusieurs recels de vaisselle d'étain (Fol 121 v°, n° 264) Août 1498.

265 Paris Lettres de rémission accordées à Jean Guesdon, compagnon suivant les guerres, pour meurtre commis à Andely-sur-Seine 1, bailliage de Gisors en Normandie, sur la personne de Thomas de la Haulle, coupable de « batteries et outrages » faits à Simonnet Guesdon, frère du suppliant (Fol 122, n° 265) Août 1498.

1 Andelys (Les), Eure.

266 Paris Lettres de rémission accordées à Jannet Moriset, homme de labour, demeurant en la châtellenie de Civray 1, en Poitou, pour faux en écriture privée (Fol 122 v°, n° 266) Août 1498.

1 Civray, Vienne, cant de Montmorillon.

267 Paris Lettres de rémission accordées à Jean de Marcoville, habitant de la paroisse de Coulombs 1, au bailliage de Caen pour meurtre commis le 15 juillet 1498 sur la personne de Loys, bastard du curé de Bur-le-Roi 2, en aidant Michel Lancelin de Saint-Germain-de-la-Lieue à défendre une terre par lui lui acquise des entreprises d'un certain Thomas Chappes (Fol 123, n° 267) Août 1498.

1 Coulombs, Calvados, arr de Caen 2 Bur-le-Roi, com de Noron, Calvados.

268 Paris Lettres de rémission accordées à Jean de Vesque, « prêtre, vicaire de l'église paroissiale de la cure de Gamaches 1 , au pays de Vimeu, bailliage d'Amiens, prévenu de faux en écritures privées » (Fol 123 v°, n° 268) Août 1498.

1 Gamaches, Somme, arr d'Abbeville.

269 Paris Lettres de rémission accordées à Jean de Coulombières, écuyer, pour meurtre commis le 25 juillet 1498 à Turqueville 1, au bailliage de Cotentin, sur la personne de Guillaume Guéroull, maçon qui voulait s'opposer par la force à la recette des deniers du guet, que le suppléant faisait avec le sergent Jean Laloé au nom de son maître, le capitaine du château de Valognes (Fol 124, n° 269) Août 1498.

1 Turqueville, Manche, arr de Valogne, cant de Saint-Mère-Église.

270 Paris Lettres de rémission accordées à Perinet de Saint-Pierre, homme de labour, habitant Saint-Pierre-des-Jonquières 1, au bailliage de Caux, pour meurtre commis, le 1er juillet 1498, sur la personne de Richard Cartier qui avec son frère Jehan Cartier et Pierret Barre avaient plusieurs fois sans raison attaqué le suppliant et son frère Pierre de Saint-Pierre (Fol 124 v°, n° 270) Août 1498.

1 Saint-Pierre-des-Jonquières, Seine-Maritime, arr de Dieppe, cant de Londinières.

270/bis Paris Lettres de rémission accordées à Pierre Bourgaize, prêtre, pour meurtre commis en état de légitime défense, à la suite d'une querelle à l'auberge, le 7 avril 1497, à Montargis, sur la personne d'un certain Le Gascon, chapelier (Fol 125 v°, n° 270 bis) Août 1498.

270/ter Paris Lettres de rémission accordées à Antoine Briant, écolier en l'université de Poitiers, pour meurtre commis le 29 mai 1498 sur la personne de Guillot de Bourges, lequel de compagnie avec une certain Jean de Flottes avait plusieurs mois auparavant déloyalement attaqué et grièvement blessé le suppliant (Fol 126, n° 270 ter) Août 1498.

271 Paris Lettres de rémission accordées à Louis colin, pâtre du bétail de Moussy-le-Neuf 1, prévôté de Dammartin, pour meurtre du nommé Louis Loseroye qui avait cherché une mauvaise querelle au suppliant et à son camarade Perrinet Le Riche, porcher de Beaumarchais 2 (Fol 126 v°, n° 271) Août 1498.

1 Moussy-le-Neuf, Seine-et-Marne, arr de Meaux 2 Beaumarchais, Seine-et-Marne, arr de Meaux.

272 Paris Lettres de rémission accordées à Colas du Boys, charpentier à Neuilly-sur-Eure 1, près Longni, au au Perche, bailliage de Chartres, pour meurtre commis le 29 juillet 1498 sur la personne de Denis du Boys, son frère bâtard, à la suite d'une querelle de famille (Fol 126 v°, n° 272) Août 1498.

1 Neuilly-sur-Eure, Orne, arr de Mortagne-au-Perche, cant de Longny-au-Perche.

273 Paris Lettres de rémission accordées à Jean Parent, habitant à Donchery-sur-Meuse 1, au bailliage de Vermandois, pour meurtre commis le 8 juillet 1498 sur la personne de Jacquemin Perrot à la suite d'une querelle de jeu (Fol 127, n° 273) Août 1498.

1 Donchery, Ardennes, arr et cant de Sedan.

274 Paris Lettres de rémission accordées à Jean Baudon, vigneron demeurant à Brimont 1, au bailliage de Vermandois, pour prétendu viol, commis le 5 juin 1498, sur la route de Brimont à Pontgivart 2, sur la la personne de Jehannette, fille de Jehannon La Court, servante à Brimont (Fol 127 v°, n° 274) Août 1498.

1 Brimont, Marne, arr de Reims, cant de Bourgogne 2 Pontgivart, Marne, arr de Reims, cant de Bourgogne.

275 Paris Lettres de rémission accordées à Antoine Gilbert, boucher, et à son cousin Jean Guynard, laboureur à Rive-de-Gier 1, en la sénéchaussée de Lyon, pour meurtre commis le 22 juillet 1498 sur la personne de Claude Gillebert, qui avait volé un cheval audit Antoine Gilbert (Fol 128, n° 275) Août 1498.

1 Rive-de-Gier, Loire, arr de Saint-Étienne.

276 Paris Lettres de rémission accordées à Mathieu Le Maluyer, pauvre homme de labour, demeurant à Cantigny, prévôté de Montdidier, pour meurtre commis sur la personne de Guillaume Fredin, qui le trompait avec sa femme, comme Claude de Sains, seigneur de Cantigny en a été une fois le témoin On rappelle que le suppléant a fait depuis ce meurtre le voyage de Rome où il a obtenu du Pape absolution dudit cas (Fol 128 v°, n° 276) Août 1498.

277 Paris Lettres de rémission accordées à Pierre Jehan, demeurant à Castelnau-de-Guers 1, en la sénéchaussée de Carcassonne, pour meurtre commis sur la personne de Pierre Bauya, à la suite d'une querelle de jeu Août 1498.

1 Castelnau-de-Guers, Hérault.

278 Paris Lettres de rémission accordées à Étienne Le Bourguignon, pauvre gentilhomme, originaire de Saint-Martin 1, au diocèse de Bourges, serviteur de Charles de Rochechouart, seigneur de Montpipeau, de passage à Semur 2, au bailliage d'Autun, à la suite au roi Charles VIII, alors en résidence au château d'Arcy 3, pour meurtre commis au mois de juillet 1497 sur la personne d'un certain Pasquier, serviteur d'un Lombard, lequel Lombard ayant pris la chambre destinée par son hôtesse au suppliant, celui-ci avait protesté auprès d'elle disant « faut-il que ces vilains bougres de Lombard soient logez et que nous ne le soyons point Je fais voeu à Dieu que, si ce n'estait pour l'honneur de vous je le délogeraye bien mais je coucherai pour cette nuit en l'estable, veu que j'aimerays mieux coucher les dix nuits que nue avec un Lombard, car ils sont tous bougres » ; propos qui amenèrent entre le suppliant et le nommé Pasquier une rixe suivie de la mort de ce dernier (Fol 129, n° 278) Août 1498.

1 Saint-Martin-d'Auxigny, Cher, arr de Bourges 2 Semur-en-Brionnois, Saône-et-Loire, arr de Charolles 3 Arcy, com de Vindecy, Saône-et-Loire, arr de Charolles, cant de Marcigny.

279 Paris Lettres de rémission accordées à Geoffroy Frérot, manouvrier, demeurant à Rouillon 1, bailliage de Dourdan, pour meurtre commis en septembre 1495 sur la personne de Pierre Roger, fabricant de charbon de bois, avec lequel il était en procès et qui l'avait déloyalement attaqué (Fol 129 v°, n° 279) Août 1498.

1 Rouillon, com de Dourdan, Essonne, arr et cant de d'Étampes.

280 Paris Lettres de rémission accordées à Jean Le Mannier, dit Martin, demeurant à Boves 1, au bailliage d'Amiens, actuellement prisonnier à Reims, pour meurtre commis le 28 octobre 1497 sur la personne de Jehannin Le Roy qui prétendant faussement que ledit suppliant lui avait fait tort l'avait attaqué à coups de pique (Fol 130, n° 280) Août 1498.

1 Boves, Somme, arr d'Amiens, cant de Sains.

281 La Ferté-Alais Lettres de rémission accordées à Pierre Gaultier, tisserand, demeurant en la paroisse de Pontgouin 1, au bailliage de Chartres, pour meurtre commis le 1 er juillet 1498, sur la personne de Guillaume Poulain, à la suite d'une rixe à l'auberge (Fol 130 v°, n° 281) Août 1498.

1 Pontgouin, Eure-et-Loir, arr de Chartres, cant de Courville.

282 Paris Lettres de rémission accordées à Huet Galle, jeune compagnon, demeurant à Musy 1, au bailliage d'Évreux, pour avoir, en 1496, porté à un certain Michelet Louvart des coups et blessures ayant entraîné la mort, à la suite d'une querelle qu'avaient cherchée au suppliant les fils dudit Louvart (Fol 131, n° 282) Août 1498.

1 Musy, Eure, arr d'Évreux, cant de Nonancourt.

283 Paris Lettres de rémission accordées à Macé Raoullet, natif de Saint-Martin-d'Auxigny 1, au diocèse de de Bourges, demeurant au château de Marcoussis 2, pour meurtre commis le 23 juillet 1498 sur la personne d'un nommé Henry Bataille à la suite d'une querelle à la foire de Marcoussis (Fol 131 v°, n° 283) Août 1498.

1 Saint-Martin-Auxigny, Cher, arr de Bourges 2 Marcoussis, Essonne, arr de Palaiseau, cant de Montlhéry.

