173 Loches Lettres de rémission accordées à Jean Clause et Jean Prélat, l'un prieur, l'autre collégien du « Collège de Rodez et de Laval », à Cahors, en la sénéchaussée de Quercy, pour meurtre commis sur la personne de Jean Villars, collégien dudit collège, à la suite d'une rixe (Fol 100, n° 173) Février 1501.

173/bis Loches Lettres de rémission accordées à Héliot du Chièvre, pour meurtre commis sur la personne d'Olivier Valeroy, prêtre, à la suite d'une dispute, au sortir d'une auberge à Montbron 1, au pays d'Angoumois, sénéchaussée de Poitou (Fol 100 v°, n° 173 bis) Février 1501.

1 Montbron, Charente, arr d'Angoulême.

174 Blois Confirmation des privilèges des habitants de Fontenay-le-Comte 1, en la sénéchaussée de Poitou Poitou (Fol 100 v°, n° 174) Février 1501.

1 Fontenay-le-Comte, Vendée.

175 Blois Lettres de rémission accordées à Robert de Laplaigne, écuyer, seigneur de Lachalm, Guillaume de Laplaigne, seigneur du Peyron, son frère, pour meurtre commis près Chantemerle 1, au bailliage de Montferrand, sur la personne de Laurent de Baille, à la suite d'une dispute sur le droit de passage (Fol 101, n° 175) Février 1501.

1 Chantemerle, Puy-de-Dôme.

176 Loches Lettres confirmant les privilèges de la baronnie de Grimaud 1, au pays de Provence, au profit d'Étienne de Vex, chevalier, seigneur et baron de Grimaud, conseiller et chambellan du roi, sénéchal de Beaucaire, lequel a succédé dans la possession de cette terre à Jean Cossa de Naples, Gaspard Cossa, son fils, et Honorat de Berre, seigneur d'Entrevennes 2 (Fol 102, n° 176) Février 1501.

1 Grimaud, Var, arr de Draguignan 2 Entrevennes, Alpes-de-Haute-Provence, arr de Digne, cant de Les Mées.

177 Blois Confirmation des privilèges de la ville de Sens (Fol 102 v°, n° 177) Février 1501.

178 Loches Lettres de rémission accordées à Jean Malignas, « homme de labour », habitant à Chevigny 1, paroisse de Bèze 2, au bailliage de Sens, pour meurtre commis sur la personne d'Étienne de Gonge, de Dampierre-sur-Vingeanne 3, qui lui dérobait du bois (Fol 103, n° 178) Février 1501.

1 Chevigny, Côte-d'Or, arr de Dijon, cant de Mirebeau-sur-Bèze, com de Bèze 2 Bèze, Côte-d'Or, arr de Dijon, cant de Mirebeau-sur-Bèze 3 Dampierre-sur-Vingeanne, Côte-d'Or, arr de Dijon, cant de Fontaine-Française.

179 Blois Lettres de rémission accordées à Jean de la Bruyère, seigneur du lieu, et à Guillaume Guillot son serviteur, pour meurtre commis sur la personne de Jean Félion, laboureur, demeurant à Rahay 1, en la baronnie de Mondoubleau 2, au comté de Vendômois, qui à une réquisition faite par ledit seigneur seigneur de la Bruyère, au nom de Méry Doulceron, lieutenant du gouverneur dudit comté, avait répondu par des injures et des menaces de mort (Fol 103 v°, n° 179) Février 1501.

1 Rahay, Sarthe, arr et cant de Saint-Calais 2 Mondoubleau, Loir-et-Cher, arr de Vendôme.

180 Loches Lettres de rémission accordées à Estienne Habert, boulanger, demeurant faubourg Saint-Sauveur à Tours, pour meurtre commis sur la personne de Pierre des Rourières, qui surpris par le suppliant en train de maltraiter un enfant, l'avait injurié et menacé de son épée (Fol 104 v°, n° 180) Février 1501.

181 Blois Création de foires dans la terre et seigneurie de la Chapelle, près Maussac, en la sénéchaussée de Quercy, et dans celle de Pezènes 1, au pays de Languedoc, à la requête de Guynot de Lauzières, conseiller et chambellan du roi, maître de son artillerie, seigneur desdits lieux (Fol 105, n° 181) Janvier 1501.

1 Pezènes, Hérault, arr de Béziers, cant de Bédarieux.

182 Loches Confirmation des privilèges de l'hôpital Saint-André de Bordeaux (Fol 105 v°, n° 182) Février 1501.

183 Loches Lettres de rémission accordées à Pierre Maulverny, marchand, demeurant à La Souterraine 1, au pays de Limousin, sénéchaussée de Poitou, pour meurtre commis sur une certaine Annette Vernoille, concubine de Jean Monique, habitant en la paroisse de Morterolles 2, et qui de concert avec ladite Annette ravageait la métairie du suppliant audit lieu de Morterolles (Fol 105 v°, n° 183) Février 1501.

1 Souterraine (La), Creuse, arr de Bourganeuf, cant de Royère 2 Morterolles, Creuse, arr de Bourganeuf, cant de Royère.

184 Blois Confirmation des privilèges des habitants de la ville du Gavre 1, au diocèse de Nantes (Fol 106, n° 184) Février 1501.

1 Gavre (Le), Loire-Atlantique, arr de Saint-Nazaire, cant de Blain.

185 Loches Lettres de rémission accordées à Hector Farnicq, écuyer, seigneur de Pouilly 1, au bailliage de Senlis, pour meurtre commis sur les personnes de Jean et Robinet de Hauteville, frères, à la suite d'une dispute survenue entre eux et le nommé Bontemps, serviteur dudit seigneur de Pouilly, à Fresneaux 2 (Fol 106 v°, n° 185) Février 1501.

1 Pouilly, Oise, arr de Beauvais, cant de Méru 2 Fresneaux-Mont-Chevreuil, Oise, arr de Beauvais, cant de Méru.

186 Blois Lettres de naturalité pour Louis de Maton, seigneur de Lornay 1, conseiller et chambellan du roi, grand écuyer de la reine, capitaine des Cent-Suisses de la garde du roi, gouverneur du comté de Montfort-l'Amaury, originaire de Savoie (Fol 107, n° 186) Décembre 1500.

1 Lornay, Haute-Savoie, arr d'Annecy, cant de Rumilly.

187 Loches Lettres de rémission accordées à Jean Honnoré, Jeanne Drouarde, sa femme, et Gillonne Honnoré, leur fille, pour « empoisonnement par l'arsenic », commis sur la personne de Pierre Pinchepré, pâtissier à Roye 1, mari de ladite Gillonne, qu'il maltraitait depuis longtemps (Fol 107 v°, n° 187) Février 1501.

1 Roye, Somme, arr de Montdidier.

188 Loches Confirmation des privilèges du prieuré de Ferrière 1, de l'ordre de Saint-Augustin, au bailliage de de Touraine, à la requête de Jean Ryant, prieur ou recteur dudit prieuré (Fol 108, n° 188) Février 1501.

1 Ferrière-sur-Beaulieue, Indre-et-Loire, arr et cant de Loches.

189 Blois Lettres de rémission accordées à Claude du Monceau, garde royal en la forêt d'Amboise, pour meurtre commis sur la personne d'un nommé Braslon, braconnier (Fol 108 v°, n° 189) Mars 1501.

190 Paris Lettres de rémission accordées à Pierre Gillot, charpentier, demeurant à Pontpoint 1, au bailliage de Senlis, pour meurtre commis sur la personne de Jean Ragot, à la suite d'une dispute survenue le jour des noces d'un de leurs voisins (Fol 109, n° 190) Décembre 1500.

1 Pontpoint, Oise, arr de Senlis, cant de Pont-Sainte-Maxence.

191 Paris Lettres de rémission accordées à Jean Frisonnet, « homme de labour », demeurant à Rivière-le-Bois 1, au bailliage de Sens, pour meurtre commis à Heuilley-le-Grand 2, sur la personne d'Etienne d'Etienne Esperit, à la suite d'une querelle survenue le soir de la fête (Fol 109 v°, n° 191) Décembre 1500.

1 Rivière-le-Bois, Haute-Marne, arr de Langres, cant de Longeau 2 Heuilly-le-Grand, Haute-Marne, arr de Langres, cant de Longeau.

192 Paris Lettres de rémission accordées à Antoine Couppe, Mahieu Couppe, Pierre Darras, habitant Croisilles 1, comté d'Artois, gouvernement de Péronne, Raye, Montdidier, pour meurtre commis sur sur la personne d'un nommé Gustin, dit Ave-Maria, à la suite d'une querelle, le jour de la fête du village (Fol 110 v°, n° 193) Décembre 1500.

1 Croissilles, Pas-de-Calais, arr d'Arras.

194 Paris Lettres de rémission accordées à Fleurot Salentin, demeurant à Jaulzy 1, au bailliage de ?, pour meurtre commis à Couloisy 2 sur la personne de Adrien Baudequin, qui les avait traités sans raison de braconnier et proférait contre aux des menaces de mort (Fol 111, n° 194) Décembre 1500.

1 Jaulzy, Oise, arr de Compiègne, cant d'Attichy 2 Couloisy, Oise, arr de Compiègne, cant d'Attichy.

195 Paris Lettres de rémission accordées à Jean du Rocher, habitant aux Rogets 1, pays de Sologne, au bailliage de Blois, pour meurtre commis sur son frère, Pierre du Rocher, qui lui refusait sa part de l'héritage de leur père, Thomas du Rocher (Fol 111 v°, n° 195) Décembre 1500.

1 Rogets (Les), Loiret, arr de Montargis, cant de Courtenay, com de Courteneaux.

196 Paris Lettres de rémission accordées à Louise, femme de Thomas Vallée, demeurant à Souday 1, sénéchaussée du pays du Maine, pour meurtre commis sur la personne de Claude Maslon, prêtre, en défendant son mari contre l'attaque dudit Maslon (Fol 112, n° 196) Décembre 1500.

1 Souday, Loir-et-Cher, arr de Vendôme, cant de Mondoubleau.

197 Paris Lettres de rémission accordées à Guillaume Roullon, demeurant à Boisemont 1 en Valois, au bailliage de Senlis, pour meurtre commis sur la personne de Charlot Fretelle, au retour de la fête de Corcy 2 (Fol 113, n° 197) Janvier 1500.

1 Boisemont, Aisne, arr de Soissons, cant de Villers-Cotterêts, com d'Oigny 2 Corcy, Aisne, arr de Soissons, cant de Villiers-Cotterets.

198 Paris Lettres de rémission accordées à Jehan Lucquet, demeurant à Vaux-en-Arrouaise 1, au bailliage de Vermandois, prévôté de Ribemont, pour meurtre commis sur la personne de Aubert Macaigne, en défendant contre ce dernier son frère Jean Lucquet l'ainé (Cf JJ 233, n° 164, fol 76) (Fol 113 v°, n° 198) Janvier 1500.

1 Vaux-Andigny, Aisne, arr de Vervins, cant de Wassigny.

199 Paris Lettres de rémission accordées à Jean Calez, habitant Halloy-les-Grandvilliers 1, au bailliage de Senlis, pour meurtre commis sur la personne d'un nommé Hutin de Paris, à la suite d'une querelle survenue la veille des noces d'un fils de Pierre Chocquart, aussi habitant Halloy (Fol 114, n° 199) Janvier 1501.

1 Halloy, Oise, arr de Beauvais, cant de Grandvilliers.

200 Paris Lettres de rémission accordées à Nyvet Faboullet, demeurant à Longueval 1, en la prévôté de Péronne, pour meurtre commis sur la personne de Valentin Courcol, qui avait conçu contre lui haine mortelle, depuis que le suppliant l'avait dépossédé de la ferme qu'il tenait au Translay, au comté d'Artois (Fol 114 v°, n° 200) Janvier 1501.

