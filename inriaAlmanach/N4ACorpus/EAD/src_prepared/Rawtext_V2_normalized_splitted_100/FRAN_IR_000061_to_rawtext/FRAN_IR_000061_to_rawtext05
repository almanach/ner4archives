107 Paris Lettres de rémission accordées à Pierre et Philippe Seychaulx, écuyers, hommes d'armes des ordonnances sous la charge de Louis II de la Trémoille, prisonnier à la Conciergerie pour homicide commis l'année précédente sur la route du Dorat au Droux 1 et en état de légitime défense sur la personne de Jean Aigrespéequi s'était précédemment rendu coupable de violences sur Louis Seychaulx frère des suppléants et de meurtre sur la personne de Louis Le Berton, frère de Jean Le Berton « qui avait épousé la soeur desdits suppliants » (Fol 56, n° 107) Juillet 1498.

1 Dorat (Le), Haute-Vienne, arr de Bellac, et le Droux, Haute-Vienne, arr de Bellac, cant de Magnac-Laval.

108 Paris Lettres de rémission pour Jehannin Cardon, laboureur, demeurant à Essuiles 1, bailliage de Senlis, pour coups et blessures ayant entraîné (mai 1498) la mort de Jeanne Torillon, qui avait gravement insulté le père du suppliant (Fol 56, n° 108) Juillet 1498.

1 Essuiles, Oise, arr de Clermont, cant de Saint-Just-en-Chaussée.

109 Paris Lettres de rémission à Étienne Le Mareschal, demeurant à Vovelles 1, paroisse de Dammarie, bailliage de Chartres, qui en voulant, le 25 mars 1498, s'interposer dans une rixe entre Hervé Thomas, couturier à Dammarie 2, et Jean Beaurepaire, a tué ledit Thomas (cf supra, n° 42, JJ 226 A, n° 469) (Fol 56 v°, n° 109) Juillet 1498.

1 Vovelles, Eure-et-Loir, arr et cant Chartres, com de Dammarie 2 Dammarie, Eure-et-Loir, arr et cant de Chartres.

110 Paris Lettres de rémission accordées à Jean de la Porte, mercier forain, demeurant à Saint-Vaast-d'Equiqueville 1 prévenu de mise en circulation de fausse monnaie à Sainte-Foy 2 et ailleurs (Fol 57, n° 110) Juillet 1498.

1 Saint-Vaast-d'Equiqueville, Seine-Maritime, arr de Dieppe, cant Envermeu 2 Sainte-Foy, Seine-Maritime, arr Dieppe, cant de Longueville-sur-Scie.

111 Paris Lettres de rémission accordées à Antoine Laisné, boucher, demeurant à Ceton 1, au pays du Perche, pour meurtre commis, au cours d'une partie de chasse, le jour de Noël 1497, sur la personne de Perrin Baudot en défendant contre ce dernier un nommé Petit-Jean, serviteur de Jacques de Boisguyon, seigneur de Ceton (Fol 57 v°, n° 111) Juillet 1498.

1 Ceton, Orne, arr de Mortagne-au-Perche, cant Le Theil.

112 Paris Lettres de rémission accordées à Henry de Hartaage, laboureur demeurant à Urzy 1, au pays de Nivernais, « retiré en franchise au bourg Saint-Étienne à Nevers » pour meurtre par lui commis le 7 octobre 1496 sur la personne de Jean Calot qui avait envahi de feu une pièce de terre à lui appartenant (Fol 58, n° 112) Juillet 1498.

1 Uzy, Nièvre, arr de Nevers, cant de Guérigny.

113 Paris Lettres de rémission accordées à Robert Paulinier, marchand bourgeois de Paris, et à sa femme Claude Marceau, coupables d'avoir vendu à Jean Raunel, bonnetier, à Pierre de Vaudetar, avocat au Parlement, et à d'autres, divers biens à eux appartenant à Paris et à Vanves 1, sans avertir les acheteurs des rentes constituées par eux, suppliants, sur ces immeubles (Fol 59, n° 113) Juillet 1498.

1 Vanves, Hauts-de-Seine, arr et cant d'Antony.

114 Paris Lettres de rémission accordées à Alleaume Le Fèvre, manouvrier, demeurant à Monthières 1, près près Bouillancourt-en-Série 2, au bailliage d'Amiens, pour meurtre commis le 23 avril 1498 sur la personne de Johannet Fleustre, garçon meunier du moulin de Soreng 3, qui le trompait avec sa femme Tassine (Fol 59 v°, n° 114) Juillet 1498.

1 Monthières, Somme, arr d'Abbeville, cant de Gamaches, com de Bouttencourt 2 Bouillancourt-en-Séry, Somme, arr d'Abbeville, cant de Gamaches 3 Soreng, com de Monchaux-Soreng, Seine-Maritime, arr de Dieppe, cant de Blangy-sur-Bresle.

115 Paris Lettres de rémission accordées à Nicole Sablier, licencié en médecine, et Jean de La Borne, apothicaire à Montluçon 1, prisonniers à la Conciergerie à Paris, comme complices de la fausse déclaration de décès, de l'enterrement simulé et des autres manoeuvres criminelles commises par le seigneur Martial de la Ville lors de la mort en août 1497 au château de Chérotte près Montluçon, de son oncle Jean de la Ville, curé de Lescat 2 au diocèse de Poitiers et de Dissay 3 au diocèse de Limoges (Fol 60, n° 115) Juillet 1498.

1 Montluçon, Allier 2 Lescat, com de Beaupouyet, Dordogne, arr de Périgueux, cant de Mussidan 3 Dissay, Vienne, arr de Poitiers, cant de Saint-Georges-lès-Baillargeaux.

116 Paris Lettres de rémission accordées à Vincent Haire, manouvrier, originaire de Berneuil 1, ou Berneuil-sur-Aisne 2, actuellement prisonnier aux prisons du Châtelet à Paris, pour meurtre commis entre Merville 3 et Calonne-sur-la Lys 4 sur un nommé Robinet Le Fèvre, son débiteur (Fol 61, n° 116) Juillet 1498.

1 Berneuil, Oise, arr de Beauvais, cant d'Auneuil 2 Berneuil-sur-Aisne, Oise, arr de Compiègne, cant d'Attichy 3 Merville, Nord, arr de Dunkerque 4 Calonne-sur-la-Lys, Pas-de-Calais, arr de Béthune, cant de Lillers.

117 Paris Lettres de rémission accordées à Huguet Le Mastin, écolier étudiant en l'université de Poitiers, pour meurtre commis le 29 mars 1498 sur la personne d'un pâtissier dans une rixe, en compagnie de Jacques Goullart, René de Chasteigner, et Jean Jacques, aussi étudiants (Fol 117, n° 61) Juillet 1498.

118 Paris Lettres de rémission pour Guillaume Roland, seigneur du Plessis-Roland 1, au pays d'Anjou, intendant de René Bourré, seigneur de Jarzé, pour meurtre commis sur la personne de Estienne Lyart, ou Le Hédart, palefrenier, qui avait gravement injurié et frappé dans l'exercice de ses fonctions (Fol 62, n° 118) Juillet 1498.

1 Plessis-Roland (Le), com de Précigné, Sarthe, arr de la Flèche, cant de Sablé-sur-Sarthe.

119 Paris Lettres de rémission accordées à Hubert de Fay pour meurtre commis à Crépy-en-Laonnois 1, le 1 er juillet dernier passé, sur la personne de Jehannin Carlier dit Nocure, qui après avoir frappé Colart de Villers, beau-père du suppliant, s'était vanté de lui faire subir le même traitement (Fol 62, n° 119) Juillet 1498.

1 Crépy-en-Laonnois, Aisne, arr et cant Laon.

120 Paris Lettres de rémission accordées à Gaillard de Latapi, homme de labour, pour meurtre commis le 10 juin 1498 dans la maison d'un nommé Pey du Brava, prévôté de la Réole 1, sur la personne d'un certain Sarrausson, à la suite d'une querelle d'auberge (Fol 63, n° 120) Juillet 1498.

1 La Réole, Gironde, arr de Langon.

121 Paris Lettres de rémission accordées à Jean Roger, demeurant à Aubenton 1, au bailliage de Vermandois, pour meurtre commis en légitime défense sur la personne de Jehan Gouge, receveur du lieu, dont il avait dévoilé la conduite scandaleuse avec la femme d'un nommé Jehannet (Fol 64, n° 121) Juillet 1498.

1 Aubenton, Aisne, arr de Vervins.

122 Paris Lettres de rémission accordées à Robin Tabour, demeurant à Bucy-les-Cerny 1, au bailliage de Vermandois, pour homicide commis le soir de la fête de Molinchart 2, à la suite d'une rixe, sur la personne de Pierre Le Camus (Fol 64 et 64 v°, n° 122) Juillet 1498.

1 Bucy-les-Cerny, Aisne, arr et cant de Laon 2 Molinchart, Aisne, arr et cant de Laon.

123 Paris Lettres de rémission accordées à Jehan Le Potier, marchand natif de Verneuil-sur-Avre 1, au pays du Perche, prisonnier à la Conciergerie de Paris, prévenu d'avoir mis en circulation (en Picardie et en Flandre, de la monnaie de mauvais aloi (Fol 64 v°, n° 123) Juillet 1498.

1 Verneuil-sur-Avre, Eure, arr d'Évreux.

124 Paris Lettres de rémission accordées à André Ferret, messager du Poitou à Paris, demeurant aux Moutiers-sur-le-Lay 1, en Poitou, prisonnier au Châtelet de Paris, pour homicide commis sur la personne d'un certain Dominique, domestique de Jean Clémenceau, libraire, qu'il soupçonnait de le tromper avec sa femme (Fol 65, n° 124) Juillet 1498.

1 Moutiers-sur-le-Lay, Vendée, arr de la Roche-sur-Yon, cant de Mareui-sur-Lay-Dissais.

125 Paris Lettres de rémission accordées à Jean de Cassaignet, seigneur de Busca 1, pour avoir blessé mortellement l'un des serviteurs de Odet de Lasseran, seigneur de Mansencôme 2 son voisin, lequel ne cessait d'entreprendre sur ses droits et ses propriétés (Fol 65 v°, n° 125) Juillet 1498.

1 Busca, com de Mansencôme, Gers, arr et cant de Condom 2 Mansencôme, Gers, arr et cant de Condom.

126 Paris Lettres de rémission pour Héliot Augier, homme de labour, demeurant à Castillon-sur-Dordogne 1, serviteur de Barthélémy de Dieusaide, seigneur d'Agulle, près Saint-George à Montagne 2, pour meurtre commis sur la personne de Jean du Boys dit le Rousseau, malfaiteur et surborneur de filles, sur la route de Castillon à la Mothe-Montravel 3 (Fol 66, n° 126) Juillet 1498.

1 Castillon-La-Bataille, Gironde, arr de Libourne 2 Saint-George, com de Montagne, Gironde, arr de Libourne, cant de Lussac 3 Lamothe-Montravel, Dordogne, arr de Bergerac, cant de Vélines.

127 Paris Lettres de rémission pour Guillaume Le Pelletier, de Mésangueville 1, au bailliage de Gournay,prévenu de meurtre commis le 8 juillet 1498 à Aubermesnil, paroisse d'Argueil 2, à la suite d'une noce de village, sur la personne d'un certain David Crosnier (Fol 66 v°, n° 127) Juillet 1498.

1 Mésangueville, Seine-Maritime, arr de Dieppe, cant d'Argueil 2 Argueil, Seine-Maritime, arr de Dieppe.

128 Paris Lettres de rémission accordées à Guillaume de Rauville, sergent à verge au Châtelet de Paris, prisonnier audit Châtelet, qui à plusieurs reprises a extorqué de l'argent à des prisonniers ou à des gens qu'il était chargé d' « exécuter » (Fol 67, n° 128) Juillet 1498.

129 Paris Lettres de rémission accordées à Alexandre Texier, homme de labour, demeurant à la Sablière, près du bourg Saint-Loup, en Poitou, qui au cours d'une querelle où il était intervenu, avait tué un certain Jean Parent, le 4 mars 1498 (Fol 67 v°, n° 129) Juillet 1498.

130 Paris Lettres de rémission accordées à Domingeon de Madirant, homme de labour, demeurant à Leguevin 1, au comté de l'Isle-en-Jourdain-Gers 2, pour meurtre commis le 25 juin 1496 sur la personne de Jean de Laubar, dit de France, tavernier (Fol 68, n° 130) Juillet 1498.

1 Leguevin, Haute-Garonne, arr  de Toulouse 2 Isle-en-Jourdain-Gers (L'), Gers, arr de Lombez.

131 Paris Lettres de rémission accordées à Jean de Lande, dit Mommel, natif de Troussures 1, en Beauvaisis, pour meurtre commis le 4 juillet 1498, le jour de la fête de Villiers-Saint-Barthélemy 2, sur la personne d'un nommé Jean Noël (Fol 68, n° 131) Juillet 1498.

1 Troussures, Oise, arr de Beauvais, cant d'Auneuil 2 Villiers-Saint-Barthélemy, Oise, arr de Beauvais.

132 Paris Lettres de rémission accordées à Jean Moreau, habitant Pressiat, paroisse de Villevêque 1, en la sénéchaussée d'Anjou, pour meurtre commis dans un accès de folie sur la personne de la femme de Jannet Deschamps, son « closier » à la closerie des Fresches-Blans, en la même paroisse (Fol 68 bis, n° 132) Juillet 1498.

1 Villevêque, Maine-et-Loire, arr et cant d'Angers.

133 Paris Lettres de rémission accordées à Robert Le Despencier, maréchal, Jean Le Despencier, son frère,Gervais Valdary, et Jean Maillyn pour meurtre commis le 15 juillet 1498, à Tillières 1, au bailliage d'Évreux, sur la personne de Jean Trevaux, dit Grosse-Teste, originaire de Bretagne, « homme mal vivant » qui depuis longtemps les suppliants poursuivait de ses injures et de ses menaces (Fol 69, n° 133) Juillet 1498.

