Quatre coupe et une élévation sur une feuille.

CP/F/14/17526/6 PHARE DE DIEPPE 1835-1911.

20 pièces dont 3 sans date.

CP/F/14/17526/6, pièce 76 "Département de la Seine-Inférieure [Seine-Maritime] Arrondissement de Dieppe Ponts et chaussées Service des ports maritimes Port de Dieppe Etablissement d'un petit feu sur la jetée de l'ouest Réclamation formulée par le Service de la Marine Plan général du port et de la ville de Dieppe 1856" Dressé par Adolphe DAINEZ, ingénieur ordinaire Vu par Pierre BOUNICEAU, ingénieur en chef Imprimé par FOUCHER, lithographe à Paris 19 juin 1860.

Plan avec légendes de la ville et des ouvrages du port figurant les villes de Dieppe et du Pollet.

CP/F/14/17526/6, pièce 77 "Plan général du Port de Dieppe sur lequel sont indiqués les réverbères qui sont pavés par l'Etat" Dressé par Adrien ARIBAUT, ingénieur 4 septembre 1844.

CP/F/14/17526/6, pièce 78 "Port de Dieppe Année 1835 Plan général de l'entrée du port de Dieppe, où on a indiqué en rouge l'emplacement du pharillon projeté" Dressé par GAYANT, ingénieur de l'armement Vu par MALLET, ingénieur en chef 15 novembre 1835 - 8 décembre 1835.

CP/F/14/17526/6, pièce 79 "Département de la Seine-Inférieure [Seine-Maritime] Arrondissement de Dieppe N°3 Ponts et chaussées Service des ports maritimes Port de Dieppe Établissement d'un petit feu sur la jetée de l'ouest Réclamation formulée par le Service de la Marine Plan du musoir" Dressé par Adolphe DAINEZ, ingénieur ordinaire Vu par Pierre BOUNICEAU, ingénieur en chef 19 juin 1860 - 20 juin 1860.

Plan orienté du musoir de la jetée de l'ouest figurant le pharillon, le cabestan, la cloche, le pieu d'amarre et l'emplacement proposé pour les signaux et le petit feu fixe.

CP/F/14/17526/6, pièce 80 "Port de Dieppe Pharillon à établir à l'extrémité de la jetée de l'ouest" Dressé par GAYANT, ingénieur de l'armement Vu par MALLET, ingénieur en chef 15 novembre 1835 - 24 novembre 1835.

Quatre figures sur une feuille : "Elévation", "Coupe suivant l'axe vertical", "Plan au niveau de la 7e marche", "Plan au niveau du socle de la lanterne".

CP/F/14/17526/6, pièces 81 à 83 et 95 "Phares de Calais, Boulogne et Dieppe Feux permanents de 0,300 m éclairant 360° Plan n° 756-2" Dressé par la société des établissements Henry-Lepaute Sans date.

Quatre figures sur une feuille : Coupe verticale de la chambre de la lanterne, "Elévation d'un montant", "Coupe par le plan focal", "Coupe AB".

CP/F/14/17526/6, pièce 84 "Port de Dieppe Restauration du musoir de la jetée ouest Installation d'un feu provisoire" Février 1899.

Deux figures sur une feuille : "Coupe suivant ab", "Plan".

CP/F/14/17526/6, pièce 85 "Port de Dieppe Installation d'une tourelle métallique pour feu de port sur le musoir de la jetée ouest" Sans date.

Six figures sur une feuille : "Elévation de la tourelle", échelle 1/50e, "Coupe par le plan focal", échelle 1/20e, "Coupe verticale de la lanterne", échelle 1/10e, "Coupe suivant ab", échelle 1/20e, "Détails d'un montant", échelle "vraie grandeur", "Coupe suivant cd vue par dessous".

CP/F/14/17526/6, pièce 86 "Feu de la jetée ouest de Dieppe Coupes horizontale et verticale de la lanterne et de l'appareil" Dressé par Charles RIBIÈRE, ingénieur en chef 7 mai 1902.

CP/F/14/17526/6, pièce 87 "Phare de la jetée de Dieppe Feu clignotant de 4e ordre 1 tour en 16 secondes N° 736-2" Dressé par la Société des établissements Henry-Lepaute Sans date.

Deux figures avec calculs sur une feuille : Coupe verticale de la lanterne, Coupe horizontale.

CP/F/14/17526/6, pièce 88 "Feu de la jetée ouest de Dieppe Seine-Maritime Dessin de détail du bec" Dressé par Charles RIBIÈRE, ingénieur en chef du Service central des phares 7 mai 1902.

Une coupe verticale de l'appareil sur une feuille.

CP/F/14/17526/6, pièce 89 "Feu de la jetée ouest de Dieppe" Dressé par Charles RIBIÈRE, ingénieur en chef du Service central des phares 7 mai 1902.

Une coupe verticale de l'appareil sur une feuille.

CP/F/14/17526/6, pièce 90 "Port de Dieppe Installation d'un signal sonore sur le musoir de la nouvelle jetée ouest 1ère étude sans suite" Dressé par Georges DE JOLY, ingénieur ordinaire attaché au Service central des phares et balises 9 décembre 1903.

Trois figures sur une feuille : "Coupe suivant ef", "Coupe suivant abcd", "Plan d'ensemble", orienté.

CP/F/14/17526/6, pièce 91 "Port de Dieppe Signal sonore de la Jetée Ouest 2e étude sans suite" Dressé par Georges DE JOLY, ingénieur en chef 11 octobre 1909.

Trois figures sur une feuille : "Coupe longitudinale", échelle 1/50e, "Plan", "Plan du musoir", échelle 1/200e.

CP/F/14/17526/6, pièce 92 "Port de Dieppe Installation d'un signal sonore sur le musoir de la nouvelle Jetée Ouest" Septembre 1909.

Trois figures sur une feuille :  "Coupe longitudinale suivant abcd", "Plan de l'abri du signal sonore", échelle 1/50e, "Plan du musoir", échelle 1/200e.

CP/F/14/17526/6, pièce 93 "Port de Dieppe Installation d'un signal sonore sur le musoir de la jetée ouest" 7 février 1911.

Deux figures sur une feuille : "Plan", "Coupe ACBCD".

CP/F/14/17526/6, pièce 94 "Signal sonore de la jetée ouest du port de Dieppe Groupe moto-compresseur à commande électrique" Sans date.

Deux schémas électriques sur une feuille, avec nota : "Schéma n°1 Connexions entre le moteur et le tableau de distribution", "Schéma n°2 Société "L'Eclairage électrique" Rhéostat 104624".

CP/F/14/17526/7 PHARE DE FÉCAMP 1830-1897.

10 pièces dont 7 sans date.

CP/F/14/17526/7, pièce 96 "Plan, coupe et élévation du phare de Fécamp" Sans date.

Cinq figures, avec calculs, sur une feuille : "Coupe sur la ligne AB", Elévation, "Elévation de l'un des bâtiments d'angle de l'enceinte du phare", Plan du rez-de-chaussée, "Plan de l'enceinte du phare".

CP/F/14/17526/7, pièce 97 "Phare de Fécamp Seine Inférieure [Seine-Maritime]" Sans date.

Quatre figures légendées sur une feuille : "Elévation sur MN", "Coupe sur AB", "Plan à hauteur de CD", "Plan à hauteur de EF".

CP/F/14/17526/7, pièce 98 "Phare de Fécamp Seine-Inférieure [Seine-Maritime]" Sans date.

Deux figures orientées sur une feuille : "Plan de la chambre de la lanterne" "Plan de la partie supérieure de l'édifice à hauteur du plan focal".

CP/F/14/17526/7, pièce 99 "Appareil sidéral de Bordier-Marcet" Juin 1830.

Deux figures avec calculs sur une feuille : Coupe verticale, "Plan du chariot et de la lampe à niveau constant".

CP/F/14/17526/7, pièce 100 "Service Central des Phares et Balises Seine-Inférieure [Seine-Maritime] Port de Fécamp Construction d'une tourelle en maçonnerie sur le musoir de la nouvelle jetée" Dressé par Georges DE JOLY, ingénieur ordinaire attaché au Service central des phares et balises Vu et adopté par RIBIERE, ingénieur en chef du Service central des phares et balises 26 mars 1897.

Quatre figures sur une feuille : "Elévation est", "Coupe verticale ab", "Plan du musoir et coupe horizontale de la tour par cdef", "Coupe horizontale par le plan focal".

CP/F/14/17526/7, pièce 101 "Port de Fécamp Construction d'une tourelle en maçonnerie sur le musoir de la jetée sud" Dressé par Georges DE JOLY, ingénieur ordinaire attaché au Service central des phares Vu et adopté par Charles RIBIÈRE, ingénieur en chef du Service central des phares 23 mars 1900.

Quatre figures avec nota et calculs sur une feuille : "Plan du musoir et coupe horizontale de la tourelle suivant abcd", échelle 1/50e, "Coupe verticale suivant ef", échelle 1/50e, "Elévation", échelle 1/50e, "Coupe par le plan focal (conforme au projet des ingénieurs de la Seine Inférieure)", échelle 1/25e.

CP/F/14/17526/7, pièce 102 "Port de Fécamp Construction d'une tourelle en maçonnerie sur le musoir de la jetée sud Etude sans suite A été faite pour Fécamp" Sans date.

Quatre figures sur une feuille : "Plan du musoir et coupe horizontale de la tourelle suivant ab", échelle 1/50e, "Coupe verticale suivant cd", échelle 1/50e, "Elévation", échelle 1/50e, "Coupe par le plan focal", échelle 1/25e.

