Z/1j/258 Estimation des travaux de réfection, en maçonnerie, faits par Gaspard Hubert en une maison, rue Zacharie, appartenant à la fabrique de l'église Saint-Séverin  Voir aussi : Saint-Séverin, rue 26 et 28 septembre 1639.

RÉGION PARISIENNE.

Z/1j/258 Arcueil (Val-de-Marne) Estimation d'une maison à Arcueil, située au croisement des deux rues conduisant à la fontaine et au carrefour proche du cimetière de l'église, à partager entre les trois héritiers de Daniel Coquart : Antoine Dagnières et deux enfants mineurs  2-5 avril 1640.

Argenteuil (Val-d'Oise).

Z/1j/256 Accord donné au s/r/ de Forestel pour la construction d'un pont de bois à Argenteuil 12 juin 1621.

Z/1j/258 Estimation des ouvrages de couverture faits au couvent des Augustins déchaussés, à Argenteuil, par Jacques Michelarme 19 septembre 1639.

Z/1j/259 Estimation d'une maison, rue Calet, à Argenteuil, et de différentes vignes et terres, à partager entre les huit héritiers de Nicolas Berger 29 juillet 1641.

Z/1j/256 Athis-Mons (Essonne) Estimation, faite pour régler les conflits entre les maçons et Guyon Pinson maître maçon, des travaux de maçonnerie réalisés par Pierre Duboué et Jean Arnoul en une maison, à Athis-Mons, appartenant au s/r/ Baudoin 1/er/ août 1636.

Aubervilliers (Seine-Saint-Denis).

Z/1j/256 Estimation des travaux de maçonnerie faits de neuf en une ferme, à Aubervilliers, par Louis Noblet 21 juillet 1637.

Z/1j/260 Estimation des travaux de maçonnerie faits par Charles Verberye en une maison, à Aubervilliers, appartenant à Jean Quetier 31 mars 1642.

Z/1j/260 Estimation des travaux de couverture de tuiles faits par Pierre Besnard pour et sous Charles Verberye, en une maison, à Aubervilliers, appartenant à Jean Quetier [deux expertises complémentaires] 31 mars 1642.

Z/1j/260 Estimation des travaux de maçonnerie faits de neuf par Guillaume Pocquet en une maison, à Aubervilliers, appartenant au s/r/ Le Vacher 14 novembre 1642.

Avron  Voir : Rosny-sous-Bois.

Bagneux (Hauts-de-Seine).

Z/1j/259 Estimation des ouvrages de maçonnerie, charpenterie et couverture faits par et sous David Reubonville, en une maison, à Bagneux  21-22 janvier 1641.

Z/1j/259 Estimation des travaux de couverture de tuiles vieilles faits en une maison, à Bagneux, appartenant à Louis Poupart, par Nicolas de Laistre, sous la direction de David Reubonville [croquis] 22 janvier 1641.

Bagnolet (Seine-Saint-Denis).

Z/1j/258 Estimation d'une maison, sise à Bagnolet, "grande rue  et vis-à-vis l'église", à partager entre les trois héritiers de Martine Gastelignière, femme de Pierre Lemaistre, dont Françoise Le Maistre, femme d'Olivier Leroux 2 avril 1640.

Z/1j/259 Estimation des parties mitoyennes entre deux maisons, sises au village de Bagnolet, appartenant à Pierre Versongne, d'une part, et à Marie Vitry, veuve de Martin Bouillon, d'autre part 28 août 1641.

Bailly-Romainvilliers (Seine-et-Marne).

Z/1j/260 Estimation des ouvrages de couverture faits de neuf par Roland Leduc en une maison, à Bailly, appartenant au s/r/ de La Martillière Janvier 1642.

Z/1j/260 Estimation des travaux faits par Antoine Séjourné et Jacques Hureau pour la conduite des eaux en la maison de Joseph Gallicany, seigneur de Saint-Germain, à Bailly 18 novembre 1642.

Barre (La)  Voir : Deuil-la-Barre.

Z/1j/256 Beaumont-sur-Oise (Val-d'Oise) Estimation de deux moulins situés sur le pont de l'Oise, à Beaumont-sur-Oise, appartenant d'une part à Élisabeth Grégoire, d'autre part à Jean Framerie et Rollin Duboc  21-23 juin 1637.

Besigny  Voir : Chevry-Cossigny.

Z/1j/256 Bezons (Val-d'Oise) Estimation des travaux faits pour la construction d'un pont de bois, au port de Bezons, accordé au s/r/ de Forestel 12 juin 1621.

Z/1j/258 Boississe, [ probablement Boississe-le-Roi] (Seine-et-Marne) Estimation par Michel Villedo des ouvrages de maçonnerie faits pour un quai, sur la Seine, à Boississe, par Charles Benoist, à la suite d'un bail daté du 20 janvier 1638 [croquis] 6 octobre 1639.

Z/1j/259 Boissy-Saint-Léger (Val-de-Marne) Estimation des travaux de maçonnerie et de couverture faits par Adrien Noël et Fiacre Pruneau en une maison, sise à Boissy-Saint-Léger, appartenant à Pierre Des Voyes 11 septembre 1641.

Z/1j/260 Bondy (Seine-Saint-Denis) Visite de l'état d'une maison, rue Médéric, à Bondy, récemment acquise par Pierre Dubuha sur Antoine Despré 22 novembre 1642.

"Bonier" (Le)  Voir : Chilly-Mazarin.

Z/1j/257 Bray-sur-Seine (Seine-et-Marne) Inspection par Michel Villedo et le s/r/ Lefebvre des travaux faits, au pont de Bray-sur-Seine, par Léon Collin  12, 13, 14 juin 1638.

Z/1j/259 Châlons-sur-Marne (Marne) Estimation des réparations à faire pour les bâtiments de l'évêché de Châlons-sur-Marne ainsi que pour les dépendances ; moulins et châteaux de Sarry, Saint-Germain-la-Ville, la Sense, Tenance, Villeneuve-aux-Riches-Hommes, étang de Lampée, fermes d'Audigny, de la Prieuré, de l'Escolle et de Saint-Laurens, réparations qui n'ont pas été faites par Henri Clausse, évêque et comte de Châlons, prédécesseur du nouvel évêque, Félix Vialar 16 mai 1641.

Z/1j/259 Chambry (Seine-et-Marne) Estimation d'une ferme et de terres à Chambry, à diviser entre Henri de Saveuze, seigneur Du Cardonnoy, Henri de Bricqueville, marquis de La Luzerne, et ses héritiers  2, 3, 4 et 5 avril 1641.

Z/1j/256 Champigny-sur-Marne (Val-de-Marne) Estimation des travaux de maçonnerie, charpenterie et couverture faits par Henri Compoinct, pour Guillaume Pathio, dans le presbytère de Champigny 14 mai 1637.

Z/1j/259 Champs-sur-Marne (Seine-et-Marne) Estimation des ouvrages de couverture de tuiles faits par Robert Anglart et Claude Bailly, dans les bâtiments des deux basses-cours du château de Champs, appartenant à Jules-César Favre 2 novembre 1641.

Chantemesle  Voir : Corbeil-Essonnes.

Charenton (Val-de-Marne).

Z/1j/256 Alignement du mur mitoyen entre deux maisons, situées dans la grande rue du village de Charenton, appartenant l'une à Ancelot Fanyn, saisie par Pierre Maugard, l'autre à Alexandre de La Nohe 26 août 1637.

Z/1j/257 Estimation d'une maison, sise au village de Charenton-Saint-Maurice, en face de la grande place, à partager entre Étienne Le Gaigneur et ses enfants 30 juin 1638.

Z/1j/257 Estimation de deux arpents de vignes, nommées "les Bannières" et de deux arpents de terre, "les Bezançonnes", situés au village de Charenton, à partager entre Étienne Le Gaigneur et ses enfants 30 juin 1638.

Z/1j/257 Estimation des réparations à faire en une maison, ayant pour enseigne "le Mouton", avec jardin et île, sis à Charenton, adjugés à Michel Noblet sur Charles Panthonnier 30 septembre 1638.

Z/1j/257 Expertise concluant sur l'avis de ne pas partager une maison, sise à Charenton, appartenant à Étienne Le Gaigneur, à ses enfants et à Haly Poirier 16 octobre 1638.

Z/1j/258 Estimation des réparations à faire en une maison, au village de Charenton, appartenant à Eustache Carron et louée à Claude Racquin 28 juin 1640.

Z/1j/259 Visite des lieux mitoyens entre une maison et un jardin, situés grande rue de Charenton, la maison appartenant à Isaac Du Fresne, le jardin à Jean Bedouyn 2 août 1640.

Z/1j/259 Estimation des améliorations apportées par Cosme de Rainnevillier et Jeanne Foucault, sa veuve, en une maison sise aux carrières de Charenton, appartenant à Charles Duchesne 7 août 1640.

Z/1j/260 Estimation des ouvrages faits au pont de Charenton, réalisée par Michel Villedo d'une part, et par Sébastien Messier et Denis de Mezerets, d'autre part 22 novembre 1642.

Z/1j/261 Château-Thierry (Aisne) Estimation des ouvrages de maçonnerie, charpenterie, menuiserie et serrurerie faits au château de Château-Thierry, par Denis Parent 19 mars 1643.

Châtillon (Hauts-de-Seine).

Z/1j/259 Estimation des ouvrages de maçonnerie faits par le s/r/ Duret, suivant le marché du 20 mars 1639 passé par-devant les s/rs/ Martoux et Lormer, notaires au Châtelet, en une maison à Châtillon, appartenant au général des Mathurins 8 novembre 1639.

