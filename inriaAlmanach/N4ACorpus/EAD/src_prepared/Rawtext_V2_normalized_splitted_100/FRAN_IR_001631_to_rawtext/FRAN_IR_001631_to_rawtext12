LORENZINI (Aristide), de Potto-Ferraio (île d'Elbe) Lettre relative à son invention destinée à "arrêter instantanément un train de chemin de fer en pleine course", 1857-1858.

LORIER (Jean-Louis), cocher à Paris Réponse à une demande de restituton de permis de conduire 1852.

LOUBIES-BARREL, de Bessan (Hérault) Demande de secours (transmission) 1853.

LOUVET, de Paris (18 rue Albony) Mémoire proposant diverses réformes administratives, sociales, etc en faveur des populations ouvrières (notamment de Paris) 1852.

LOVICONI (Noël), de Bastelica (Corse) Pétition relative à l'exécution du legs fait par Napoléon Ier à son père, l'officier Joseph-Marie Loviconi 1862.

LUCINSIANI (Joseph), de l'Agha, près d'Alger Demande de place de surveillant au lazaret d'Alger (transmission) 1852.

LURINE (Louis), de Paris Projet de société pour la publication des oeuvres complètes de Napoléon Ier 1852.

LUSIGNAN (Vienne) Pétition des autorités du canton pour le passage à - du chemin de fer de Poitiers à la Rochelle (transmission) 1852.

LYON Réclamation de la chambre de commerce contre le classement de la ville comme place de guerre (transmission avec analyse détaillée) 1853 Demande de concession d'ouvrages historiques et archéologiques (récemment publiés par l'Imprimerie impériale) pour la bibliothèque municipale 1867.

LYON (Catherine), ménagère à Licques (Pas-de-Calais) Demande de secours (transmission) 1855 ( ?).

F/70/65 MADAGASCAR à MUSÉES DE PROVINCE.

MADAGASCAR Note du capitaine de vaisseau Guillain proposant l'établissement du protectorat français sur l'île à l'occasion de la mort de la reine Ranavalo 1852.

MADELEINE (marché de la), à Paris Demandes de médailles à l'effigie de l'Empereur par des commerçants de ce marché 1852-1853.

MADRAGUES Demande de rétablissement des - par des habitants de Solliès-Farlède [auj la Farlède] (Var) 1852.

MAIGUE (Francisque), ex-représentant du peuple expulsé de France Réponse du ministre de l'Intérieur (avec pièces jointes) à une demande d'autorisation définitive de résidence en Haute-Loire 1853-1854.

MAISONS DE CAMPAGNE Dossier comprenant trente-neuf feuilles de croquis (plans et élévations), un devis et un tableau comparatif de prix de revient sur les maisons à édifier dans les domaines de la Couronne à Vincennes 1858.

MALIBRAN (Alexandre) Projet d'ensemble récréatif nommé "Villa Fiorita" dans le bois de Vincennes (vue cavalière et plan colorié, par l'architecte Ernest Le Brun, de Paris) 1857.

MALLET Réponse à un envoi de projet d'uniforme pour les employés au balayage de la voie publique [à Paris] 1853.

MANGIN, de Belleville (Seine), ancien notaire Réclamations relatives à une demande d'emploi de gardien d'un monument public 1855.

MANUFACTURE DES TAPISSERIES DE NEUILLY Mémoire historique et justificatif autographié de son fondateur, F Planchon 1864.

MANUFACTURES DES GOBELINS, DE SEVRES [et de Beauvais] 1 Note sur le mode de concession des produits de ces manufactures sd 2 Réglement d'attributions du personnel administratif de la manufacture des Gobelins (avec copie d'une lettre d'envoi de 1851) Sd.

MARCESCHEAU, de Paris, consul général en retraite Envoi de trois imprimés de 1838 dont il est l'auteur 1862 1 Opinion d'un pauvre diable sur le remboursement des rentes 2 Proposition de loi pour la conversion des rentes 3 L'Auteur de l'Opinion d'un pauvre diable à M de Lamartine (avec copie manuscrite de la réponse de Lamartine).

MARCHAND (L), de Paris, épicier ( ?) Pétition signée de très nombreux épiciers de Paris, contre la concurrence jugée déloyale des grands magasins 1852.

MARCHANDS AMBULANTS Pétition de plusieurs - de Paris qui souhaitent vendre limonade et liqueurs sur le Champs-de-Mars pendant les revues et exercices militaires (transmission) 1852.

MARCOU, instituteur public à Villiers-en-Bois (Deux-Sèvres) Réponse à une demande de secours pour son école (transmission) 1852.

MARCOU (Mme), veuve d'un garde national mort des suites des évènements de juin 1848 Demande de secours et d'emploi (transmission) 1852.

MARENNES (Charente-Inférieure) Pétition de plusieurs propriétaires de marais salants de l'arrondissement (transmission) 1853.

MARET-LERICHE, de Paris Projet de réorganisation financière des expositions nationales des Beaux-Arts (mise en loterie de toutes les oeuvres) et demande de titularisation au Ministère d'État 1861.

MAROLLE (Marie-Virginie Manon, épouse Jean-Louis), de Grenoble Demande de réversion de la pension de sa mère, veuve d'un fusilier vétéran du Premier Empire 1852.

MAROT (Charles-Auguste), inspecteur des travaux des Tuileries Lettre de l'architecte H Lefuel en sa faveur 1864.

MARQUESSAC (baron de), de Saint-Jean-de-Blaignac (Gironde) Correspondance touchant un mémoire sur un nouveau système de classement des archives (recommandation du cardinal Donnet, archevêque de Bordeaux) 1865.

MARTIN (George), avocat à Lyon Demande de justice de paix à Lyon 1861.

MARTIN (abbé), directeur de l'institution des sourds-muets de Besançon Demande de prise en charge par l'Etat des sourds-muets enfants d'indigents 1852.

MARULLO (Felice), de Naples Pétition présentée à titre de survivant de la campagne de Russie de 1812 - 1813 (certificats napolitains joints) 1857.

MASSON-d'ESPAGNAC Réclamation par Z Crouzet, de Paris, mandataire d'un nommé Driollet, de la liquidation de la créance de l'entreprise de convois et charrois militaires formée en 1792-1793 par Masson et Sahuguet d'Espagnac 1854.

MAUSCHERNING (veuve), de Münster (Allemagne) Proposition de vente de divers objets pris dans la voiture de Napoléon Ier, après la bataille de Waterloo, par son beau-père, alors officier de l'armée prussienne 1853.

MAYET (L), propriétaire à Paris Envoi d'une brochure de 1851 avec plan colorié sur un projet d'ouverture de voie nouvelle dans le Quartier Latin, à Paris (contre-projet de la rue des Ecoles) 1852.

MAZIER (famille), de Nantes Demande de réparation, par Durand-Mazier, négociant à Nantes, du tort subi en 1816 par son beau-père Mazier-Verrier, ancien maître de postes à Nantes (y est jointe la demande faite en 1830 par ce dernier et appuyée par La Fayette et le général Lamarque) 1852.

MÉJANEL DE LA ROQUE, propriétaire d'un haras à Villers-les-Pots (Côte-d'Or) Pétition relative à son haras (transmission) 1857.

MELLIER (Mme) de Paris Demande d'admission de sa fille dans un orphelinat 1853.

MÉNIL-ERREUX (Orne) Demande de subvention pour travaux à l'église (transmission) 1853.

MÉRYON (Charles), graveur Demande d'indemnité pour les modifications apportées par la Chalcographie à la planche qu'il avait gravée, sur commande ministérielle, d'après un tableau de RZeeman 1867.

MEXIQUE Envoi au ministre des Finances, à l'intention du gouvernement mexicain, d'une collection de documents administratifs sur le ministère de la Maison de l'Empereur et des Beaux-Arts 1865.

MICHEL (Francisque), érudit Renseignements sur les papiers et reliques de Napoléon Ier conservés au musée de Kelso (Grande-Bretagne) et chez un fermier des environs nommé Darling- Note relative au congrès des architectes du Royaume-Uni à Oxford 1858.

MICHELOT (Jules), de Paris, ancien chef de bureau au ministère de la Guerre Demande de réintégration appuyée par son frère, A Michelot, directeur du Chemin de fer de la Loire 1852-1853.

MIELLIN, de Laferté-sur-Aube (Haute-Marne) Pétition relative à un remède de son invention contre les maladies épizootiques de la race bovine (transmission avec analyse détaillée 1852.

MIGEON (abbé), vicaire à Saint-Germain-en-Laye Demande de canonicat à Saint-Denis (transmission) 1852.

MILITAIRES DES PROVINCES RHÉNANES (anciens) Pétitions de onze anciens militaires résidant à Trèves, qui souhaitent bénéficier du testament de Napoléon Ier (transmission, avec note) 1854.

MILTENBERGER, de Paris Procédé (s) contre la maladie des pommes de terre et pour débarasser les blés des charençons, insectes, poussières, etc (transmission, avec note) 1852.

MINISTERE DES AFFAIRES ÉTRANGERES Dossier sur la revendication de compétence exclusive de ce ministère en matière d'affaires internationales et notamment pour ce qui concerne les décorations étrangères ou décernées aux étrangers 1852 Présentation de la signature de Ch Génant, commis principal de la Chancellerie du ministère 1865.

