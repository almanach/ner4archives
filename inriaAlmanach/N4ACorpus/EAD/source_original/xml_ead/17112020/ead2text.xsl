<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   
   <!-- Output Methods initialize -->
    <xsl:output omit-xml-declaration="yes" indent="no"/>
    <xsl:output method="text" encoding="UTF-8"/>
   
   <!-- Configuration initialize -->
   <xsl:strip-space elements="*"/>
   
   <!-- Variables collection -->
   <xsl:variable name='newline'>&#xa;</xsl:variable>
   <xsl:variable name='carriage_return'>&#xd;</xsl:variable>
   
   
   
    
   <xsl:template match="/">
    <xsl:apply-templates select="//archdesc"/>
 </xsl:template>
   
   <xsl:template match="did">
      <xsl:for-each select=".">
         <xsl:value-of select=""/><xsl:value-of select="$newline"/>
      </xsl:for-each>
   </xsl:template>
   
   <xsl:template match="scopecontent">
      <xsl:value-of select="."/>
   </xsl:template>
    
    
</xsl:stylesheet>