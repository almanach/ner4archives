- `ISTEX/` (Voir Wiki pour citation/infos corpus)
	- `source` articles scientifiques en format XML TEI et annotations déportées dans JSON avec les offsets correspondants.
	- `ISTEX_TEI_V1` Annotations réinsérées dans une structure XML TEI minimale via un script Python à partir des fichiers `source/`. Prétraitements** : aucun **pré-annoté (correction et enrichissement nécéssaire)** [Docs : 35, Taille : 2 Mo ]

- `LEM17_PRESTO/` (Voir Wiki pour citation/infos corpus)
	- `presto_max_v.5.3` : fichier texte annotés en entités nommées format Conll **pré-annoté (enrichissement nécéssaire)** [Docs : 1, Taille : 16 Mo ]