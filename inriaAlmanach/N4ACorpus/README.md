## Corpus NER4Archives

Dernière actualisation : 22/10/2021

`Corpus NER4Archives/` regroupe l'ensemble des données et des outils pour les traitements, l'intégration et la récupération de ces dernières (dans la plateforme [INCEpTION](https://inception-project.github.io/)) dans le cadre du projet NER4Archives.

Pour comprendre l'interaction/articulation général entre les dossiers, scripts et fichiers Cf. ci-dessous schéma **Chaîne de traitement d'INCEpTION au NER**

-----

- [`EAD/`](EAD/)
	- `source_original/` : corpus de référence en XML EAD fourni par les Archives nationales le 17/11/2020. Pour plus de détail consulter le [CSV](../../ArchivesNationales/20201117_NER4Archives_corpusTest_1/20201117_projetNER4Archives_liste-corpus-de-test_1.xlsx) associé ;
	- `source_preannot/` (obsolète) : corpus préparé dans le cadre d'un essai de pré-annotation des XML EAD via l'outil [*Entity-fishing*](https://nerd.readthedocs.io/en/latest/) et l'API d'interaction avec le format XMI [DKPro Cassis](https://cassis.readthedocs.io/en/latest/) pour une post-correction dans l'outil d'annotation INCEpTION ;
	- `src_prepared/` : Diverses versions de corpus préparées (non-annotés) pour tester leur intégration dans l'outil INCEpTION. Pour plus de détail sur les sous-dossiers consulter les métadonnées dans le [CSV](./documentation/src_prepared_metadata.ods). **Le corpus `EAD_TEI_V2/` est celui qui est en cours d'annotation dans INCEpTION.** 

- [`EAD_IAA/`](EAD_IAA/) : Corpus versionnés spécifiques pour les évaluations inter-annotateurs et récupérés via la [CLI](./InceptionCLI/); pour consulter les résultats voir le [répertoire dédié](../Evaluation_annotation).

- [`EAD_release/`](EAD_release/) : Corpus / exemples versionés, annotés et corrigés dans INCEpTION (avec et sans post-traitements) dans différents formats pour l'entraînement NER, voir les [détails](./EAD_release/README.md).

- [`data_on_hold/`](data_on_hold/) : Autres corpus indépendants du projet NER4Archives pour des tests, voir les [détails](./on_hold/README.md).

- [`InceptionCLI/`](./InceptionCLI/) : CLI pour la récupération des données sur INCEpTION.

- [`scripts_utils/`](./scripts_utils/) : Divers scripts Bash et feuilles XSLT de transformation EAD vers TEI. Voir le Readme pour plus de détail.

- [`scripts_preprocessing/`](./scripts_preprocessing/) : script de post-traitements (merge, retokenisation, normalisation, sérialisation) des fichiers XMI corrigés et/ou des fichiers CONLL corrigés. Voir le Readme pour plus de détail.

- `Statistiques_dataset.ipynb` : Notebook pour réaliser des statistiques basiques sur les annotations du *golden corpus* 

-----

### Chaîne de traitement d'INCEpTION au NER  


<!--
#### Méthode n°1 : **"Fast & Light"**

<img src="./documentation/N4ACorpus_ner4Archives_fast_light.drawio.png" alt="schéma N4ACorpus"/>
-->

* Passage par le format XMI intermédiaire 

<img src="./documentation/N4ACorpus_ner4Archives_slow_control.drawio.png" alt="schéma N4ACorpus"/>


<!--- [`notebooks/`]() : Notebooks pour préparer et constituer des corpus avant leur intégration dans INCEpTION. La plupart utilisés pour `src_prepared/`.-->